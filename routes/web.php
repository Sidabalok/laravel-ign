<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('welcome', function () {
    return view('welcome');
});

Route::get('secretariat', function () {
	return view('secretariat');
});

Route::get('committee', function () {
	return view('committee');
});

Route::get('ayimun-2019', function () {
	return view('ayimun-2019');
});

