<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#22B6F1">
    <script type="0f1a2a0a0b0018dab24341f6-text/javascript">
	 window.__lc = window.__lc || {};
	 window.__lc.license = 10350237;
	 (function() {
	   var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
	   lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
	   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
	 })();
    </script>
    <noscript>
      <a href="https://www.livechatinc.com/chat-with/10350237/" rel="nofollow">Chat with us
      </a>,
      powered by 
      <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat
      </a>
    </noscript>
    <script type="0f1a2a0a0b0018dab24341f6-text/javascript">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MXB4C94');</script>
    <link rel="shortcut icon" href="https://modelunitednation.org/wp-content/uploads/2019/08/AYIMUN_Logo_trspnt-50-x-49.png" />
    <title>Asia Youth International Model United Nations
    </title>
    <meta name="description" content="AYIMUN (Asia Youth International Model United Nations) by International Global Network is a platform where youth mentality in leadership, negotiation and diplomacy will be developed in a Model United Nations. AYIMUN aims to engage youth leaders from all over the world and to provide a platform to share perspectives in opinions with the theme “GLOBAL DIPLOMACY AMONGST THE SOVEREIGN NATIONS“ We wish, by joining AYIMUN (Asia Youth International Model United Nations) by International Global Network, You could enhance capabilities and encourage yourself to develop networks, also improve your communication and diplomatic skills." />
    <meta name="keywords" content="Mun, model united nation, ayimun, asia youth, asia, young, international model un" />
    <meta name="author" content="International Global Network, AYIMUN" />
    @push('styles')
    <link rel="stylesheet" href="{{asset('/css/liquid-icon.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/css/theme-vendors.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/css/theme.min.css')}}" />
    <link rel="stylesheet" href="https://modelunitednation.org/ayimun5/assets/css/ayimun5.css" />
  </head>
  <body data-mobile-nav-trigger-alignment="right" data-mobile-nav-align="left" data-mobile-nav-style="classic" data-mobile-nav-shceme="gray" data-mobile-header-scheme="gray" data-mobile-nav-breakpoint="1199">
    <div id="wrap">
      <header class="main-header main-header-overlay" data-sticky-header="true" data-sticky-options='{ "stickyTrigger": "first-section" }'>
        <div class="mainbar-wrap">
          <div class="megamenu-hover-bg">
          </div>
          <div class="container-fluid mainbar-container">
            <div class="mainbar">
              <div class="row mainbar-row align-items-lg-stretch px-4">
                <div class="col pr-5">
                  <div class="navbar-header">
                    <a class="navbar-brand" href="welcome" rel="home">
                      <span class="navbar-brand-inner">
                        <img class="mobile-logo-default" src="{{('img/logo/AYIMUNLOGO2(150x150) color.png')}}" alt="Asia Youth International MUN Logo">
                        <img class="logo-default" src="{{('img/logo/AYIMUNLOGO2(150x150) white.png')}}" alt="AYIMUN logo">
                      </span>
                    </a>
                    <button type="button" class="navbar-toggle collapsed nav-trigger style-mobile" data-toggle="collapse" data-target="#main-header-collapse" aria-expanded="false" data-changeclassnames='{ "html": "mobile-nav-activated overflow-hidden" }'>
                      <span class="sr-only">Toggle navigation
                      </span>
                      <span class="bars">
                        <span class="bar">
                        </span>
                        <span class="bar">
                        </span>
                        <span class="bar">
                        </span>
                      </span>
                    </button>
                  </div>
                </div>
                <div class="col">
                  <div class="collapse navbar-collapse" id="main-header-collapse">
                    <ul id="primary-nav" class="main-nav main-nav-hover-underline-1 nav align-items-lg-stretch justify-content-lg-center" data-submenu-options='{ "toggleType":"fade", "handler":"mouse-in-out" }' data-localscroll="true">
                      <li class="">
                        <a href="welcome">
                          <span class="link-icon">
                          </span>
                          <span class="link-txt">
                            <span class="link-ext">
                            </span>
                            <span class="txt">
                              Home
                              <span class="submenu-expander">
                                <i class="fa fa-angle-down">
                                </i>
                              </span>
                            </span>
                          </span>
                        </a>
                      </li>
                      <li class="menu-item-has-children megamenu megamenu-fullwidth">
                        <a href="#">
                          <span class="link-icon">
                          </span>
                          <span class="link-txt">
                            <span class="link-ext">
                            </span>
                            <span class="txt">
                              Information
                              <span class="submenu-expander">
                                <i class="fa fa-angle-down">
                                </i>
                              </span>
                            </span>
                          </span>
                        </a>
                        <ul class="nav-item-children">
                          <li>
                            <div class="container megamenu-container">
                              <div class="vc_row row megamenu-inner-row bg-white">
                                <div class="container ld-container">
                                  <div class="row ld-row">
                                    <div class="megamenu-column col-md-4">
                                      <h3 class="megamenu-heading">Secretariat
                                      </h3>
                                      <ul class="lqd-custom-menu reset-ul">
                                        <li>
                                          <a href="secretariat">The Secretariat
                                          </a>
                                        </li>
                                      </ul>
                                    </div>
                                    <div class="megamenu-column col-md-4">
                                      <h3 class="megamenu-heading">General Information
                                      </h3>
                                      <ul class="lqd-custom-menu reset-ul">
                                        <li>
                                          <a href="#">Application Overview
                                          </a>
                                        </li>
                                        <li>
                                          <a href="#">Venue
                                          </a>
                                        </li>
                                        <li>
                                          <a href="#">Guide to Fundarising
                                          </a>
                                        </li>
                                        <li>
                                          <a href="#">Payments
                                          </a>
                                        </li>
                                        <li>
                                          <a href="#">Delegates Preparation
                                          </a>
                                        </li>
                                        <li>
                                          <a href="#">Travel Arrangements
                                          </a>
                                        </li>
                                      </ul>
                                    </div>
                                    <div class="megamenu-column col-md-4">
                                      <h3 class="megamenu-heading">MUN
                                      </h3>
                                      <ul class="lqd-custom-menu reset-ul">
                                        <li>
                                          <a href="#">Fixed Delegates
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://modelunitednation.org/committee-topics/">Committee Topics
                                          </a>
                                        </li>
                                      </ul>
                                    </div>
                                    <div class="megamenu-column col-md-4">
                                      <h3 class="megamenu-heading">Previous AYIMUN
                                      </h3>
                                      <ul class="lqd-custom-menu reset-ul">
                                        <li>
                                          <a href="https://modelunitednation.org/ayimun-2019/">2019
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://modelunitednation.org/ayimun-2018/">2018
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://modelunitednation.org/ayimun-2017/">2017
                                          </a>
                                        </li>
                                      </ul>
                                    </div>
                                    <div class="megamenu-column col-md-4">
                                      <h3 class="megamenu-heading">Others
                                      </h3>
                                      <ul class="lqd-custom-menu reset-ul">
                                        <li>
                                          <a href="https://events.oneworld.com/travel/web/oneworldhome">Booking Flight
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://modelunitednation.org/tnc/">Terms & Condition
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://modelunitednation.org/faq/">FAQ
                                          </a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <a href="#">
                          <span class="link-icon">
                          </span>
                          <span class="link-txt">
                            <span class="link-ext">
                            </span>
                            <span class="txt">
                              MUN Academy
                              <span class="submenu-expander">
                                <i class="fa fa-angle-down">
                                </i>
                              </span>
                            </span>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="https://modelunitednation.org/blog/">
                          <span class="link-icon">
                          </span>
                          <span class="link-txt">
                            <span class="link-ext">
                            </span>
                            <span class="txt">
                              News
                              <span class="submenu-expander">
                                <i class="fa fa-angle-down">
                                </i>
                              </span>
                            </span>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="https://modelunitednation.org/about">
                          <span class="link-icon">
                          </span>
                          <span class="link-txt">
                            <span class="link-ext">
                            </span>
                            <span class="txt">
                              About Us
                              <span class="submenu-expander">
                                <i class="fa fa-angle-down">
                                </i>
                              </span>
                            </span>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="https://modelunitednation.org/socialproject">
                          <span class="link-icon">
                          </span>
                          <span class="link-txt">
                            <span class="link-ext">
                            </span>
                            <span class="txt">
                              Social Project
                              <span class="submenu-expander">
                                <i class="fa fa-angle-down">
                                </i>
                              </span>
                            </span>
                          </span>
                        </a>
                      </li>
                      <li>
                        <a href="https://modelunitednation.org/partnership/">
                          <span class="link-icon">
                          </span>
                          <span class="link-txt">
                            <span class="link-ext">
                            </span>
                            <span class="txt">
                              Partnership
                              <span class="submenu-expander">
                                <i class="fa fa-angle-down">
                                </i>
                              </span>
                            </span>
                          </span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col text-right">
                  <div class="header-module py-2">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <main id="content" class="content">
        <section class="vc_row lqd-css-sticky bg-cover bg-center d-flex align-items-center py-5 bg-header">
          <div class="container">
            <div class="row">
              <div class="lqd-space" style="height: 150px;">
              </div>
              <div class="lqd-column col-md-10 col-md-offset-1 text-center px-lg-3" data-custom-animations="true" data-ca-options='{"triggerHandler":"inview", "animationTarget":"all-childs", "duration":1600, "delay":250, "easing":"easeOutQuint", "direction":"forward", "initValues":{"opacity":0, "translateY":100}, "animations":{"opacity":1, "translateY":0}}' data-parallax="true" data-parallax-from='{"translateY":1}' data-parallax-to='{"translateY":-220}' data-parallax-options='{"easing":"linear","reverse":true,"triggerHook":"onEnter", "overflowHidden": false}'>
                <h2 class="mt-0 mb-3 text-white lh-115 " data-fittext="true" data-fittext-options='{"compressor":0.75,"maxFontSize":"64","minFontSize":"48"}' data-split-text="true" data-split-options='{"type":"lines"}'>
                  Board of Director and Secretariat
                </h2>
                <h6 class="text-uppercase font-weight-normal text-white ltr-sp-075">ASIA YOUTH INTERNATIONAL MODEL UNITED NATIONS
                </h6>
              </div>
            </div>
          </div>
        </section>
        <section class="pt-25 pb-25">
          <div class="container">
            <div class="row">
              <header class="fancy-heading text-center mb-5">
                <h3 class="font-weight-semibold">SECRETARY GENERAL
                </h3>
              </header>
            </div>
            <div class="row d-flex flex-wrap align-items-center">
              <div class="lqd-column col-md-6">
                <div class="lqd-frickin-img" data-inview="true" data-inview-options='{ "delayTime": 250, "threshold": 0.75 }'>
                  <div class="lqd-frickin-img-inner">
                    <span class="lqd-frickin-img-bg bg-gradient-primary-tl">
                    </span>
                    <div class="lqd-frickin-img-holder">
                      <figure data-responsive-bg="true" class="loaded">
                        <img class="" src="https://modelunitednation.org/master_asset/img/bodbos/Jihan-indo.jpg" alt="bos">
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-6 mt-0">
                <header class="fancy-title">
                  <h3 class="title mb-4 font-weight-bold">Cut Jihan Shavira&nbsp;
                    <sup>
                      <img class="bendera" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                    </sup>
                  </h3>
                  <p class="font-size-16 lh-16" style="text-indent: 50px">
                    Cut Jihan Shavira is a fresh graduate from Universitas Indonesia majoring in International Relations. Her MUN journey has made her traveling across the world, such as to Sydney, Madrid, and Boston to name a few. One of her most remarkable winning moment was when snatching Best Delegation (open award involving every council) and Outstanding Delegate (highest award for each council) at Asia-Pacific Model United Nations Conference 2018 in Sydney, Australia. Aside from MUN, she is also active in many organizations, such as being the President of Foreign Policy Community Indonesia Chapter Universitas Indonesia 2019. Being a fresh graduate, she is currently starting her professional career as CEO Office Analyst at a prop-tech company.
                    <span style="padding-left: 50px">
                      </p>
                </header>
              </div>
            </div>
          </div>
        </section>
        <section class="vc_row pt-25 pb-30">
          <div class="container">
            <div class="row mt-0 mb-5">
              <header class="fancy-heading text-center">
                <h3 class="font-weight-semibold">BOARD OF DIRECTORS
                </h3>
              </header>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/UNICEF.png" alt="UNHCR">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/charvlduggal.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Charvi Duggal
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">CHAIR
                </h6>
                <a href="#Charvi" data-lity="#Charvi">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Charvi" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/charvlduggal.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/charvlduggal.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Charvi Duggal has been a part of both International and Nations Model United Nations conferences and aims to advocate her platform for advocating world issues. She is a 4th-year law student who has been an intern at Amnesty International in Handling the Human rights of Women and has been working as an intern at J Sagar Associates (India’s Top Law Firm). She has her publication in Thomson Reuters on the Topic “Access to Justice and United Nations”. She is the founder and Secretary-General of Indépendante International which works to make the international legal system reach the people across the globe and that they have easy access to justice. She is very proud to announce that AYI MUN is going to be her 100th MUN and she feels happy to celebrate it with all the young and intellectual minds. With her passion for debate, she is filled with enthusiasm and is looking forward to a fruitful debate and is ready to render some of her learning experiences to the Delegates.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/vianca.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Vianca Caitlin
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/philippines-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/philippines-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - Chair
                </h6>
                <a href="#vaincacatlin" data-lity="#vaincacatlin">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="vaincacatlin" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/vianca.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/vianca.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>17 year old, Vianca Caitlin Marie Go from Cebu, Philippines believes that anyone can make a difference. By the ripe age of six, public speaking played an enormous role in her life. She has written and performed Ted Talks and competed in various public speaking competitions. Armed with her talent in public speaking and leadership, she ventured into Model United Nations and was fortunate enough to bring pride for her country. Ever since, she has actively participated winning various Best Delegate Awards and charing for multiple conferences. She also believes that this is a platform to be the voice for the voiceless and the protectors of the weak. Vianca is more than delighted and thrilled to meet all the delegates participating in this conference. She is proud of you for making this decision because change is only possible if we speak up and fight for it. She's reminding everyone to BE THE CHANGE, because it all starts with your voice and what you want to do with it.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">COMING SOON&nbsp;
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/logo/AYIMUNLOGO3(85x85)%20color.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/logo/AYIMUNLOGO3(85x85)%20color.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="ngelink" data-lity="ngelink">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="ngelink" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/UNHCR.png" alt="UNHCR">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/daria.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Daria Kisseleva
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/france-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/france-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">CHAIR
                </h6>
                <a href="#daria" data-lity="#daria">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="daria" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/daria.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/daria.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Daria is pursuing a dual master’s degree in International Development and International Security. She has discovered MUN back in her first year of university in Paris and has been involved for a while now, in every capacity from delegate to Secretariat, and everything in between! So far, her MUN journey has made her travel accross Europe, in Southeast Asia, in the Middle East and in North America. Growing up between two countries with fairly different cultures, Daria’s favourite part about MUN is meeting people from different cultural backgrounds and exploring the diversity of points of view. In her spare time, she enjoys learning new languages, binge watching TED talks and meeting with her MUN friends from all over the world. She is deeply convinced that Model UN can be a great tool to create positive change, and being the Chair of UNHCR at this edition of AYIMUN is a true honour for her!
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/sachin.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Sachin Shekhawat 
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - Chair
                </h6>
                <a href="#Sachin" data-lity="#Sachin">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Sachin" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/sachin.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/sachin.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Sachin Shekhawat is pursuing his bachelor's in english and political science from shekhawati university.his mun journey started quite early since he turned 16 and he has been a part of 6 different national and international muns .His most prominent achievement is to be invited in jaipur mun as an invitational speaker for being the youngest delegate to attend an international mun ,when he was 16 yrs old . Apart from muns Sachin has been a part of indian public schools conference debate ,iconic poet of india 2019, national boxers meet 2018,Rashtriya military schools conference and the list goes on.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">COMING SOON&nbsp;
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/logo/AYIMUNLOGO3(85x85)%20color.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/logo/AYIMUNLOGO3(85x85)%20color.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="ngelink" data-lity="ngelink">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="ngelink" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/DISEC.png" alt="DISEC">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/jountyyin.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Jonty Yin
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/england-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/england-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">CHAIR
                </h6>
                <a href="#jountyyin" data-lity="#jountyyin">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="jountyyin" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/jountyyin.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/jountyyin.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Jonty Morrough Yin is a first year Law student at the University of Cambridge in the UK. His MUN journey has been mainly centralised around the UK, but he has engaged in over 10 online conferences around the world, including head chairing International MUN (IMUN). In July, he set up his own conference - the London Independent MUN, an online conference with over 500 signups from 60 countries on six continents and with guest speakers including an international judge. Mr Yin has now served as the Secretary General of two conferences and acted last month as the Under Secretary General of Logistics at Cambridge University International Model United Nations High School Conference online as one of the youngest Secretariat member in the conference history. He has also won best native english speaking delegate at the Prague Erasmian European Youth Parliament session in 2018 and was selected as a UK delegate for the (sadly cancelled) Milan International Session of the European Youth Parliament.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">COMING SOON
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/logo/AYIMUNLOGO3(85x85) color.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/logo/AYIMUNLOGO3(85x85) color.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - Chair
                </h6>
                <a href="#ngelink" data-lity="#ngelink">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="ngelink" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">COMING SOON&nbsp;
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/logo/AYIMUNLOGO3(85x85)%20color.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/logo/AYIMUNLOGO3(85x85)%20color.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="ngelink" data-lity="ngelink">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="ngelink" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/FAO.png" alt="FAO">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/irenashaleva.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Irena Shaleva
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/italy-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/italy-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">CHAIR
                </h6>
                <a href="#Irena" data-lity="#Irena">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Irena" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/irenashaleva.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/irenashaleva.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>My name is Irena Shaleva, I’m a cosmopolitan studying International Sciences at the University of Turin. I was born in Bulgaria but raised in Italy, perfect combo between love for the Mediterranean cousin and pathos from the Balkans. My passion for MUN started in 2015 in high school and introduced me to a beautiful and international environment, up to now I had the opportunity to attend MUNs both in the United States and in Europe. I’m part of M.S.O.I. Torino at my university in Italy, and in my free time I write articles about geopolitics for M.S.O.I. thePost. Social issues and human rights are fundamental to me, thus I believe in the importance of awareness through culture and the power of education for freedom. It’s an honour for me to chair at this prestigious Conference.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/madhavrathi.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Madhav Rathi
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - Chair
                </h6>
                <a href="#Madhav" data-lity="#Madhav">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Madhav" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/madhavrathi.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/madhavrathi.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Madhav is currently doing his graduation in Medicine. Being a Future Doctor his main goal is to " Develop Health across the Border". He knows how to be calm and keep patience in the most tactful situations. His MUN journey had given him a platform where he thinks that his goal and vision can be fulfilled. Also serving as a Ambassador for International Global Network, he tends to influence the youth in his country. He had started his writing and MUN, from very young age, that gave him a experience of more than 9 MUN's till now. He has been awarded by the PCRA, Ministry of India for showing his excellent skills in Essay Writing and Debate/ Allocation in Pan India competitions.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/Rousseau.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Rousseau Angelo
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="#Rousseau" data-lity="#Rousseau">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Rousseau" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/Rousseau.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/Rousseau.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/oic.png" alt="oic">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/hinosamueljose.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Hino Samuel Jose
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">CHAIR
                </h6>
                <a href="#Samuel" data-lity="#Samuel">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Samuel" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/hinosamueljose.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/hinosamueljose.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Hailing from Jakarta, Jose is a sophomore of IR undergraduate started MUNing since 2018 and he has been to both online and offline conferences either International or national MUNs. This will be his 3rd time in AYIMUN after he started delegating in AYIMUN back in 2019 ! He has been doing MUNs and being passionate to it as he dreams to serving his country as a diplomat. He strived for the value that the best delegate should not only be able to excel in both substance and diplomatic manner, but also could empower the council to reach beyond the targeted measures. Aside from MUN-ing, he loves to watch movies and eat beef yakiniku; he loves to engage with others too! Jose is looking forward to welcoming all delegates in AYIMUN Online 2020!
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/adolfo.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Adolfo Alvarez
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/united-states-of-america-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/united-states-of-america-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - Chair
                </h6>
                <a href="#Adolfo" data-lity="#Adolfo">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Adolfo" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/adolfo.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/adolfo.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Adolfo Alvarez is a high school student at Universidad Espanol in Acapulco, Mexico. He started joining MUN in 2016, since then, he has enrolled in several conferences as delegate, chair and organizer, coordinated by committees from 11 different countries. He has gotten notable awards, such as being the Best Delegate of Universidad Espanol MUN twice, the Most Outstanding Delegate of IGN Online MUN and other awards at various national and international conferences. Aside from MUN, he is also involved in various organizations, such as MUN Impact, Rotary International, and Teleton Mexico through volunteering and fundraising. In addition, Adolfo has participated in public speaking, debating, science and writing contests with the support of his school and advisors.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">COMING SOON
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="#ngelink" data-lity="#ngelink">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="ngelink" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/ufm.png" alt="UFM">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/jeremysee.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Jeremy S. J. Heng
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/singapore-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/singapore-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">CHAIR
                </h6>
                <a href="#Jeremy" data-lity="#Jeremy">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Jeremy" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/jeremysee.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/jeremysee.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Jeremy See is a 21 year old student from the Singapore Institute Of Management currently pursuing a degree in Economics and Management. Hailing from the sunny island of Singapore, his very first MUN experience was Harvard National Model United Nations in Boston where he was the head delegate for Singapore's delegation. To him MUN is one of the most enriching experiences a student can partake in, giving one the opportunity to meet amazing people from all over the world! Outside of MUN, Jeremy also is the founder of a profitable cleaning company focusing on providing jobs to the ageing population of Singapore for the past 3 years. He believes in providing an exciting and friendly environment for all delegates to practice diplomacy but more importantly to also learn from others! He looks forward to hearing all the feasible and creative solutions and debates from all delegates!
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/michelleedin.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Michelle Eldyn Yauw
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - Chair
                </h6>
                <a href="#Michelle" data-lity="#Michelle">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Michelle" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/michelleedin.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/michelleedin.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Michelle Eldyn Yauw is a second year student in Singapore Institute of Management (SIM) majoring in International Relations under the University of London. She started her MUN journey one year ago in AYIMUN 2019 as a delegate in UNEP where she was awarded Honourable Mention for her performance. It was a very eye-opening experience for her and it sparked her interest in the MUN community. Hence after entering university, she joined the delegation there and was chosen as one of the delegates to represent Singapore in Harvard National MUN 2020 in Boston. She enjoys MUN a lot because it gives her the chance to practice her diplomacy skills and to connect with new people from all over the world. A humbling experience during her MUN journey was when she received the Diplomatic Commendation Award in Harvard National MUN 2020 for the ECOFIN committee. She has since came back to Singapore to be one of the trainers for SIM’s delegation. In her free time, she loves studying languages, painting and drawing portraits, and watching vlogs in Youtube! She’s looking forward to hearing the debates in the UfM committee and can’t wait to see everyone soon!
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">COMING SOON
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="#ngelink" data-lity="#ngelink">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="ngelink" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/imf.png" alt="IMF">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/Faris.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Faris Abdurrachman
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">CHAIR
                </h6>
                <a href="#Faris" data-lity="#Faris">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Faris" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/Faris.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/Faris.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/albekdoskahanov.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Alibek Dosakhanov
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/uzbekistan-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/uzbekistan-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - Chair
                </h6>
                <a href="#albekdoskahanov" data-lity="#albekdoskahanov">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="albekdoskahanov" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/albekdoskahanov.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/albekdoskahanov.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>My name is Alibek Dosakhanov and I will be performing as the co-chairperson on the upcoming AYIMUN 2020 conference. Currently, I am studying at Westminster International University in Tashkent with faculty of Business Management with Marketing pathway. Moreover, I am social activist and young diplomat in various aspects. Having participated in 35+ MUNs across the world both locally and internationally, starting back in 2014 in school MUN, I felt in love with MUN sphere. Overall winning 8 awardees on international MUNs and performing as a Chairperson on remained ones, I gained tremendous experience from a pure diplomacy. What I’m telling this for is to claim that I have valuable experience to share with all of you with respect to diplomacy. By and large, from my side I promise to use my knowledge of being a co-chairperson wisely to both providing diplomatic atmosphere and having a great fun. It is highly recommended to join the voices of youth and learn more how to be competent in the diplomatic field we try to be engaged in.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">COMING SOON
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="#ngelink" data-lity="#ngelink">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="ngelink" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/undp.png" alt="UNDP">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/hibanasrulloh.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Hiba Nasrollah
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/italy-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/italy-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">CHAIR
                </h6>
                <a href="#hibanasrulloh" data-lity="#hibanasrulloh">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="hibanasrulloh" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/hibanasrulloh.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/hibanasrulloh.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Hiba Nasrollah is pursuing a master in International Relations in the eternal city of Rome. She has been introduced to MUN when she first started university, and since then attended several conferences in the Middle East, North America, and Europe. Given her Moroccan origins, she can speak French and the Arabic as well as Spanish, Italian and of course English! It is a true honour for Hiba to take part in the conference and to chair the UNDP committee.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/pratham.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Pratham P. Golcha
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - Chair
                </h6>
                <a href="#pratham" data-lity="#pratham">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="pratham" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/pratham.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/pratham.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Pratham Golcha is a 19 year old Indian national, currently pursuing his third year of graduation, majoring in the subjects of psychology, sociology and political science from Nagpur University, India. A former delegate and award winner of AYIMUN 2020 in Malaysia, Pratham began his chairing journey with Da Nang International MUN in Vietnam shortly after. Other MUNs to his credit as a part of the Executive Board include JLMUN(Israel), GYLMUN(India), International MUN Online Conferences, GYIMUN(India), to name a few. Apart from MUNs, Pratham is an avid traveller and also a resource speaker worldwide. He is a writer, International Human Rights volunteer, International research paper presenter and facilitates United Nations’ SDG courses globally. Additionally, Pratham is a podcaster and a blogger and has a keen interest about new cultures and languages.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/shubam.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Shubham Bhatia
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="#shubam" data-lity="#shubam">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="shubam" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/shubam.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/shubam.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Shubham Bhatia is a second year finance student in New Delhi Institute of Management. His MUN journey began at a college level in 2020 when winning the Honourable Mention at IMI College MUN in Delhi. He has won the Best Delegate award in the International MUN as well and a Special Mention at International Global Summit.He has done 6 MUNs as a delegate converting 4 of them to awards and 3 MUNs as a chair.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/interpol.png" alt="interpol">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/naufalrafiff.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Naufal Rafif Siahaan
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">CHAIR
                </h6>
                <a href="#naufalrafiff" data-lity="#naufalrafiff">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="naufalrafiff" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/naufalrafiff.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/naufalrafiff.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Naufal Rafif Siahaan, nicknamed Raf, is a bachelor of Political Science from Universitas Brawijaya, Malang, Indonesia. Adventurous is his nature; originally from Jakarta, he likes to travel and always up for challenges and something new. He is currently a DKI Jakarta Tourism Ambassador, where he is entrusted to promote the tourism hot spots in Jakarta, particularly in South Jakarta. As for his MUN career, in total he has participated in 25 MUN Conferences; 15 delegating and 10 chairing, domestically and internationally, with numerous and various awards, achievements, and experiences. Lastly, he is the Founding Father of his university Model United Nations organisation; the Universitas Brawijaya Model United Nations Club.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/fikria.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Fikri A. Rizki Selo
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - Chair
                </h6>
                <a href="#fikria" data-lity="#fikria">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="fikria" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/fikria.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/fikria.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Fikri Abirawa Rizki is an individual with versatile knowledge on public and private law and tremendous ability to perform legal research which required to excel in the field of corporate law. Moreover, he has participated into numerous Model United Nations conferences to enhance his public speaking and negotiation skills and basketball as his main sport to maintain his physicality.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Coming Soon
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="#ngelink" data-lity="#ngelink">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="ngelink" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/who.png" alt="who">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/carolineagustine.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Caroline Augustine A
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Chair
                </h6>
                <a href="#carolineagustine" data-lity="#carolineagustine">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="carolineagustine" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/carolineagustine.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/carolineagustine.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Caroline Augustine Atmojo is a final year student majoring at Public Health Nutrition, Universitas Indonesia. She has been exploring health-related topics at WHO since 2018 where she won Best Delegate award at WHO Singapore MUN. She is currently engaged in MUN circuit as Secretary General of UI MUN Club 2020. She believes that MUN is only a tool that the impact will depend on how a person utilize it. She is now into social project by becoming a part of Tiny Hopes Indonesia, a contextualization of solutions beyond draft-resolutions aiming to stop stunting in Indonesia through health education and promotion.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/Ibrahimkhan.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">M Ibrahim Khan
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/pakistan-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/pakistan-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - CHAIR
                </h6>
                <a href="#Ibrahimkhan" data-lity="#Ibrahimkhan">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Ibrahimkhan" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/Ibrahimkhan.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/Ibrahimkhan.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Muhammad Ibrahim Khan is a 3rd year medical student enrolled in Services Institute of Medical Sciences, Lahore, Pakistan. If there's one thing Ibrahim is dedicated about in his life other than helping people, it's participating in every single debating event that's going on whether it be a parliamentary event or a Model UN. His dedication and determination towards debates is what has refined him into an extremely sublime debater. Ibrahim has managed to bag a number of accolades throughout the course of his debating career which simply indicate upon his debating skills and prowess. Besides debates, Ibrahim is a supporter of Universal Health Coverage and advocates for Meaningful Youth Participation in decision making processes at all levels. He prides himself in his sense of humour, and his explorative taste in music. His aspirations are also inclusive of travelling around the world in 80 days. His message to all the delegates preparing would be: "Over the years I've learned that there are winners, but there are no losers at MUNs, because those who do not win awards, still get a chance to learn new things and make new friends. If you are new to MUNs, donot let the notion affect your participation in intense debate, impactful rhetorics and fine diplomacy. I want all delegates to be diplomatic, put in the effort and think outside the box, display eloquence in expression,and work together to come up with immaculate draft resolutions containing sustainable frameworks and optimum solutions. Research good! Dress better! Turn on the camera! And you are ready to go!"
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Coming Soon
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="#ngelink" data-lity="#ngelink">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="ngelink" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/Sochum.png" alt="Sochum">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/namanshar.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Naman Sharma
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Chair
                </h6>
                <a href="#namanshar" data-lity="#namanshar">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="namanshar" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/namanshar.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/namanshar.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Naman Sharma is a first-year undergraduate student, majoring in economics. His life revolves around MUNs, food, movies, friends and football. He has been to myriad national and international conferences as delegate, chairs and academics director. According to him, his best MUN experience was AYIMUN 2019. When it comes to committee preference, his love for the Security Council is unrivaled but he also enjoys the GAs, especially GA1 & GA3. He prioritizes learning over winning and wishes to improvise him with every conference attends. He believes every conference is unique and one must enjoy it. He sends his best regards to the delegates.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/claudi.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Claudia Patricia
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/venezuela-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/venezuela-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - CHAIR
                </h6>
                <a href="#Claudiapatricia" data-lity="#Claudiapatricia">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Claudiapatricia" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/claudi.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/claudi.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Claudia Matjushin is a Venezuelan student of Advertising and Public Relations at Universidad Complutense de Madrid. She started MUN at high-school level, attending multiple models in her country. In 2019, she assisted her first international-level conference in Harvard World Model United Nations in Madrid with the delegation from her then university, UCAB. Aside from MUN, she partcipated in various student-led organizations and was part of the organizing committee for a youth art festival at her home city. Currently, she works at the communications department at a Venezuelan NGO who serves children and teenagers with developmental disorders, for which she also is a volunteer.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Coming Soon
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/india-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="#Muflihdwi" data-lity="#Muflihdwi">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Muflihdwi" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/ayimunbodnyasiapaya.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>...
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/councils/UNEP.png" alt="UNEP">
                </figure>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/pamudhika.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Pumudika H. Amarasekara
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/sri-lanka-flag-wave-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/sri-lanka-flag-wave-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Chair
                </h6>
                <a href="#Pumudika" data-lity="#Pumudika">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="Pumudika" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/pamudhika.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/pamudhika.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Pumudika Amarasekara is a member of the Chartered Global Management Accountants London, UK and Associate of the Chartered Institute of Management Accountants London, UK. She has a Distinction for MSc in Business Analytics from Robert Gordon University, UK and a 1st Class in BSc. (Hons) in Business Management from The Federal University of Wales, UK. She always believes that one should not love what you do but, rather should do what you love, and in this style, there is no need for extra motivation since you already are. Her hobby is to travel the world and thus MUNs gives her the prefect opportunity to see the beauty of mother nature. She currently works as the Deputy Group Management Accountant at a leading private Hospital in Sri Lanka. Her quote for life is, “Magic happens when you do not give up, even though you want to, the Universe will always fall in love with a Stubborn heart”, which have proven to be true over the years with her life journey. To the amazing delegates of AYIMUN VC 2020 she wishers the best of luck in all your endeavors and bless you all to find that spirit which will drive you.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/markpoalo.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Markpoalo Canada
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/philippines-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/philippines-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Co - CHAIR
                </h6>
                <a href="#markpoalo" data-lity="#markpoalo">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="markpoalo" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/markpoalo.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/markpoalo.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>An ambitious 24-year old Science Professor, Markpoalo Canada, who teaches in one of the most prestigious schools in Cebu City Philippines has gotten the inspiration from the active youth who wants to be the change for the world. Education is his greatest weapon towards youth development through knowledge acquisition, research-driven approach, and discoveries of today for Scientific Development. He focuses on the general concepts of Chemistry, Biology, Physics, and Astronomy. He also discusses environmental awareness, health, and many more. One thing he could not forget from his favorite saying by Mr. Albert Einstein that Education is what remains after one has forgotten what one has learned in school. He started his interest in joining and chairing local and international model united nations conferences when he was at the tertiary level. He has joined and has lead student organizations as an active youth for empowerment towards a better community. Asia Youth International Model United Nations is his first international MUN conference in Bangkok, Thailand, and has inspired him to do more. He chaired local and international MUNs as well and he could not say no to AYIMUN because his heart is so close to it. He became a coach to his students to join MUN and is so proud to have seen them bagged awards and now they are flying with their own wings. Many people say that a great teacher inspires but for him it is not. He wants to emphasize that he became a great teacher because he was and will always be inspired by his students.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
              <div class="lqd-column col-md-3 col-xs-6 mb-30">
                <figure>
                  <img class="round" src="https://modelunitednation.org/master_asset/img/bodbos/aryadika.jpg" alt="BOD">
                </figure>
                <h3 class="mb-1 h4">Arya Dika
                </h3>
                <img class="w-10 visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <img class="w-25 visible-sm visible-xs" src="https://modelunitednation.org/master_asset/img/flag/indonesia-flag-waving-icon-128.png">
                <h6 class="mt-0 mb-15 font-size-18 text-uppercase opacity-05">Rapoteur
                </h6>
                <a href="#aryadika" data-lity="#aryadika">
                  <p class="text-uppercase" style="color: #ff7719">Detail Profile&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-long-arrow-right">
                    </i>
                  </p>
                </a>
                <div id="aryadika" class="lqd-modal lity-hide">
                  <div class="lqd-modal-inner">
                    <div class="lqd-modal-head">
                    </div>
                    <div class="lqd-modal-content">
                      <div class="row">
                        <div class="col-md-2 text-center">
                          <figure class="mb-3">
                            <img class="visible-lg visible-md" src="https://modelunitednation.org/master_asset/img/bodbos/aryadika.jpg" alt="Modal Image">
                            <img class="visible-xs visible-sm w-20" src="https://modelunitednation.org/master_asset/img/bodbos/aryadika.jpg" alt="Modal Image">
                          </figure>
                        </div>
                        <div class="lqd-column col-md-8">
                          <p>Arya Dika is an undergraduate English Literature student in one of the universities in Indonesia with passion in community services, volunteering, and discussing diplomacy. He has been involved in several model united nations conferences both national and international since 2018, 10+ as a delegate, 5 as an EB member, and 2 as member of Secretariat. Back in 2018, he got an exchange program scholarship to USA for a school year and since from there, he learned about different perspectives between countries and always want to implement his knowledge through MUN conferences. He is more than excited to welcome all of the delegates of UNEP Committee as he believes MUN is a platform to learn and have fun, therefore you shall not be worried to say whatever you have in mind. He has a very magnetic personalities that wants to bring smiles to the delegates. That being said, he cannot wait to have fun with all of you and bring smiles in this edition of AYIMUN Virtual Conference!
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="lqd-modal-foot">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <section class="vc_row visible-md visible-lg" style="display: none !important">
        <div class="container">
          <div class="lqd-custom-menu lqd-sticky-menu" style="position: fixed; padding: 8px 12px">
            <div class="row d-flex flex-wrap align-items-center px-2">
              <div class="lqd-column col-md-4">
                <p class="font-weight-bold font-size-25 mt-0 pb-0 mb-0 text-black">Get Your Chance
                </p>
              </div>
              <div class="lqd-column col-md-3 text-center">
              </div>
              <div class="lqd-column col-md-2 text-center pt-0">
                <a href="https://modelunitednation.org/join" class="btn btn-solid text-uppercase btn-md circle border-thin text-uppercase font-size-12">
                  <span>
                    <span class="btn-txt text-uppercase">Join Now
                    </span>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="vc_row visible-xs visible-sm" style="display: none !important">
        <div class="lqd-custom-menu lqd-sticky-menu" style="position: fixed; padding: 8px 12px 15px">
          <div class="row d-flex flex-wrap align-items-center px-2 py-1">
            <div class="lqd-column col-xs-5 col-md-offset-6">
              <p class="font-weight-bold font-size-16 text-black mb-0 pb-0">Get Your Chance
              </p>
            </div>
            <div class="lqd-column col-xs-5 col-md-offset-6">
              <a href="https://modelunitednation.org/join" class="btn btn-solid text-uppercase btn-md circle border-thin font-size-10 text-uppercase ltr-sp-15 lh-17">
                <span>
                  <span class="btn-txt text-uppercase">Join Now
                  </span>
                </span>
              </a>
            </div>
          </div>
        </div>
      </section>
      <footer class="main-footer bg-primary">
        <section class="pt-25 pb-10">
          <div class="container">
            <div class="row d-flex flex-wrap">
              <div class="lqd-column col-md-3 col-sm-3 align-items-center text-center">
                <figure>
                  <img src="https://modelunitednation.org/online/assets/img/logo/AYIMUNLOGO3(150x150) white.png" alt="Logo">
                </figure>
              </div>
              <div class="lqd-column col-md-6 col-sm-6">
                <h3 class="widget_title h5 text-white font-weight-bold">Contact us
                </h3>
                <p class="text-white">
                <p>
                  <strong>
                    <i class="fa fa-envelope">&nbsp;
                    </i>
                  </strong> 
                  <a class="text-white" href="/cdn-cgi/l/email-protection#c2aaa7aeaead82afada6a7aeb7acabb6a7a6aca3b6abadacecadb0a5">
                    <span class="__cf_email__" data-cfemail="026a676e6e6d426f6d66676e776c6b7667666c63766b6d6c2c6d7065">[email&#160;protected]
                    </span>
                  </a>
                </p>
                <p>
                  <i class="fa fa-phone">&nbsp;
                  </i>
                </strong> 
              <a class="text-white" href="phone:+622127611598">+62 21 2761 1598
              </a>
              </p>
          </div>
          <div class="lqd-column col-md-3 col-sm-3">
            <h3 class="widget_title text-white h5 font-weight-bold">Follow Us
            </h3>
            <ul class="social-icon circle social-icon-md">
              <li>
                <a href="https://www.facebook.com/internationalmodelun/" target="_blank" style="background-color: #595959">
                  <i class="fa fa-facebook">
                  </i>
                </a>
              </li>
              <li>
                <a href="https://www.instagram.com/internationalmun/" target="_blank" rel="noopener noreferrer" target="_blank" style="background-color: #595959">
                  <i class="fa fa-instagram">
                  </i>
                </a>
              </li>
              <li>
                <a href="https://www.youtube.com/channel/UCji9RDaSd46lsxpwx7lSPqw" title="youtube" target="_blank" style="background-color: #595959">
                  <i class="fa fa-youtube-play">
                  </i>
                </a>
              </li>
            </ul>
          </div>
          </div>
        </div>
      </section>
    <section class="bt-fade-white-015 pt-3 pb-15">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center text-white lh-15">
            <p> Organized by 
              <a style="color: yellow" href="https://internationalglobalnetwork.com/" target="_blank">
                <strong>International Global Network
                </strong>
              </a> | © Copyright
              <a style="color: yellow" href="https://modelunitednation.org/">
                <strong> AYIMUN
                </strong>
              </a> Terms and Condition
              <a style="color: yellow" href="https://modelunitednation.org/terms-and-condition/" target="_blank">
                <strong>
                </strong>
              </a>
            </p>
          </div>
        </div>
      </div>
    </section>
    </footer>
  </div>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js" type="0f1a2a0a0b0018dab24341f6-text/javascript">
</script>
<script src="https://modelunitednation.org/master_asset/ave/js/theme-vendors.js" type="0f1a2a0a0b0018dab24341f6-text/javascript">
</script>
<script src="https://modelunitednation.org/master_asset/ave/js/theme.min.js" type="0f1a2a0a0b0018dab24341f6-text/javascript">
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="0f1a2a0a0b0018dab24341f6-|49" defer="">
</script>
</body>
</html>
