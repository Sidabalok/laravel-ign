<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#22B6F1">
    <script type="225a7fec3d77f8612aa0e549-text/javascript">
	 window.__lc = window.__lc || {};
	 window.__lc.license = 10350237;
	 (function() {
	   var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
	   lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
	   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
	 })();
    </script>
    <noscript>
      <a href="https://www.livechatinc.com/chat-with/10350237/" rel="nofollow">Chat with us
      </a>,
      powered by 
      <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat
      </a>
    </noscript>
    <script type="225a7fec3d77f8612aa0e549-text/javascript">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MXB4C94');</script>
    <link rel="shortcut icon" href="https://modelunitednation.org/wp-content/uploads/2019/08/AYIMUN_Logo_trspnt-50-x-49.png" />
    <title>Asia Youth International Model United Nations
    </title>
    <meta name="description" content="AYIMUN (Asia Youth International Model United Nations) by International Global Network is a platform where youth mentality in leadership, negotiation and diplomacy will be developed in a Model United Nations. AYIMUN aims to engage youth leaders from all over the world and to provide a platform to share perspectives in opinions with the theme “GLOBAL DIPLOMACY AMONGST THE SOVEREIGN NATIONS“ We wish, by joining AYIMUN (Asia Youth International Model United Nations) by International Global Network, You could enhance capabilities and encourage yourself to develop networks, also improve your communication and diplomatic skills." />
    <meta name="keywords" content="Mun, model united nation, ayimun, asia youth, asia, young, international model un" />
    <meta name="author" content="International Global Network, AYIMUN" />
    <link rel="stylesheet" href="https://modelunitednation.org/master_asset/ave/vendors/liquid-icon/liquid-icon.min.css" />
    <link rel="stylesheet" href="https://modelunitednation.org/master_asset/ave/vendors/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://modelunitednation.org/master_asset/ave/css/theme-vendors.min.css" />
    <link rel="stylesheet" href="https://modelunitednation.org/master_asset/ave/css/theme.min.css" />
    <link rel="stylesheet" href="https://modelunitednation.org/ayimun5/assets/css/ayimun5.css" />
  </head>
  <body data-mobile-nav-trigger-alignment="right" data-mobile-nav-align="left" data-mobile-nav-style="classic" data-mobile-nav-shceme="gray" data-mobile-header-scheme="gray" data-mobile-nav-breakpoint="1199">
    <div id="wrap">
      <main id="content" class="content">
        <section class="vc_row fullheight d-flex flex-wrap align-items-center py-5 bg-cover bg-header-kolase">
          <div class="liquid-row-overlay" style="background:rgba(0, 0, 0, 0.15)">
          </div>
          <div class="ld-particles-container pos-abs" style="height: 50vh; top: 25vh;">
            <div class="ld-particles-inner pos-abs" id="particles-1" data-particles="true" data-particles-options='{"particles":{"number":{"value":150},"color":{"value":"#ffffff"},"shape":{"type":["circle"]},"opacity":{"value":0.5,"random":true,"anim":{"enable":true,"opacity_min":0.25,"speed":0,"sync":true}},"size":{"value":2,"random":true,"anim":{"enable":true,"size_min":35}},"move":{"enable":true,"direction":"right","speed":6,"random":true,"out_mode":"out","attract":{"enable":true}}},"interactivity":{"modes":{"bubble":{"distance":1,"size":1,"duration":1}}},"retina_detect":true}'>
            </div>
          </div>
          <div class="ld-particles-container pos-abs" style="height: 50vh; top: 25vh;">
            <div class="ld-particles-inner" id="particles-2" data-particles="true" data-particles-options='{"particles":{"number":{"value":100},"color":{"value":"#ffffff"},"shape":{"type":["circle"]},"opacity":{"value":0.65,"random":true,"anim":{"enable":true,"opacity_min":0.2,"speed":1,"sync":true}},"size":{"value":2,"random":true,"anim":{"enable":true,"size_min":44}},"move":{"enable":true,"direction":"right","speed":4,"random":true,"out_mode":"out","attract":{"enable":true}}},"interactivity":{"modes":{"bubble":{"distance":1,"size":1,"duration":1}}},"retina_detect":true}'>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="lqd-column col-md-12 text-center">
                <h5 class="text-white text-uppercase ltr-sp-15">Asia Youth International Model United Nations
                </h5>
                <h1 class="my-0 text-white text-uppercase" data-fittext="true" data-fittext-options='{"compressor":1,"maxFontSize": 100,"minFontSize": 68}'>
                  Coming Soon
                </h1>
                <div class="mb-100">
                </div>
                <div class="row">
                  <div class="lqd-column col-md-8 col-md-offset-2">
                    <div class="mb-100">
                    </div>
                    <p class="font-size-14 font-weight-bold text-uppercase ltr-sp-15 text-white">Stay tuned for updates
                    </p>
                    <ul class="social-icon branded circle social-icon-sm mb-3 mt-3">
                      <li>
                        <a href="https://www.facebook.com/internationalmodelun/" target="_blank">
                          <i class="fa fa-facebook text-white">
                          </i>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.instagram.com/internationalmun/" target="_blank">
                          <i class="fa fa-instagram text-white">
                          </i>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.youtube.com/channel/UCji9RDaSd46lsxpwx7lSPqw" target="_blank">
                          <i class="fa fa-youtube text-white">
                          </i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="lqd-column col-md-12">
                    <p class="my-0 font-size-15 text-fade-white-08">Organized by 
                      <a class="text-white" href="https://internationalglobalnetwork.com/">International Global Network
                      </a> © Copyright 2020. (AYIMUN) Asia Youth International MUN. 
                      <a class="text-white" href="https://modelunitednation.org/tnc/">Terms and Conditions
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js" type="225a7fec3d77f8612aa0e549-text/javascript">
    </script>
    <script src="https://modelunitednation.org/master_asset/ave/js/theme-vendors.js" type="225a7fec3d77f8612aa0e549-text/javascript">
    </script>
    <script src="https://modelunitednation.org/master_asset/ave/js/theme.min.js" type="225a7fec3d77f8612aa0e549-text/javascript">
    </script>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="225a7fec3d77f8612aa0e549-|49" defer="">
    </script>
  </body>
</html>