<!DOCTYPE html>
<html lang=en-US>
  <head>
    <meta charset=UTF-8>
    <link rel="stylesheet" id="ao_optimized_gfonts" href="https://fonts.googleapis.com/css?family=Rubik%3A500%2C700%7CRoboto%3A600%2C500%2C300%2C400%7CRubik%3A500%2C700%7CRoboto%3A600%2C500%2C300%2C400%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRubik%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7COpen+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRubik%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7COpen+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;display=swap" />
    <link rel=profile href=https://gmpg.org/xfn/11>
    <link rel=pingback href=https://modelunitednation.org/xmlrpc.php>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <link media=all href=https://modelunitednation.org/wp-content/cache/autoptimize/css/autoptimize_010fbfd26b90df0b57e5a547411c003e.css rel=stylesheet>
    <title>AYIMUN | Asia Youth International Model United Nations
    </title>
    <meta name=description content="AYIMUN is an international United Nation simulation conference where youth can share thoughts/ideas about social issues and learn diplomacy, leadership, networking &amp; negotiation skills.">
    <meta name=robots content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">
    <link rel=canonical href=https://modelunitednation.org/ayimun-2019/>
    <meta property=og:locale content=en_US>
    <meta property=og:type content=article>
    <meta property=og:title content="AYIMUN | Asia Youth International Model United Nations">
    <meta property=og:description content="AYIMUN is an international United Nation simulation conference where youth can share thoughts/ideas about social issues and learn diplomacy, leadership, networking &amp; negotiation skills.">
    <meta property=og:url content=https://modelunitednation.org/ayimun-2019/>
    <meta property=og:site_name content="Asia Youth International Model United Nations">
    <meta property=article:publisher content=https://www.facebook.com/internationalmodelun/>
    <meta property=article:modified_time content=2019-09-27T06:47:20+00:00>
    <meta property=og:image content=https://modelunitednation.org/wp-content/uploads/2019/02/logo-header.png>
    <meta name=twitter:card content=summary_large_image> 
    <script type=application/ld+json class=yoast-schema-graph>{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://modelunitednation.org/#organization","name":"Asia Youth International Model United Nation","url":"https://modelunitednation.org/","sameAs":["https://www.facebook.com/internationalmodelun/","https://www.instagram.com/internationalmun/","https://www.youtube.com/channel/UCji9RDaSd46lsxpwx7lSPqw"],"logo":{"@type":"ImageObject","@id":"https://modelunitednation.org/#logo","inLanguage":"en-US","url":"https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN_Logo_trspnt.png","width":601,"height":589,"caption":"Asia Youth International Model United Nation"},"image":{"@id":"https://modelunitednation.org/#logo"}},{"@type":"WebSite","@id":"https://modelunitednation.org/#website","url":"https://modelunitednation.org/","name":"Asia Youth International Model United Nations","description":"Asia Youth International Model United Nations","publisher":{"@id":"https://modelunitednation.org/#organization"},"potentialAction":[{"@type":"SearchAction","target":"https://modelunitednation.org/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"ImageObject","@id":"https://modelunitednation.org/ayimun-2019/#primaryimage","inLanguage":"en-US","url":"https://modelunitednation.org/wp-content/uploads/2019/02/logo-header.png","width":580,"height":156},{"@type":"WebPage","@id":"https://modelunitednation.org/ayimun-2019/#webpage","url":"https://modelunitednation.org/ayimun-2019/","name":"AYIMUN | Asia Youth International Model United Nations","isPartOf":{"@id":"https://modelunitednation.org/#website"},"primaryImageOfPage":{"@id":"https://modelunitednation.org/ayimun-2019/#primaryimage"},"datePublished":"2019-01-23T07:55:39+00:00","dateModified":"2019-09-27T06:47:20+00:00","description":"AYIMUN is an international United Nation simulation conference where youth can share thoughts/ideas about social issues and learn diplomacy, leadership, networking & negotiation skills.","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://modelunitednation.org/ayimun-2019/"]}]}]}</script> 
    <link rel=dns-prefetch href=//js.hs-scripts.com>
    <link rel=dns-prefetch href=//connect.livechatinc.com>
    <link href=https://fonts.gstatic.com crossorigin=anonymous rel=preconnect>
    <link rel=alternate type=application/rss+xml title="Asia Youth International Model United Nations &raquo; Feed" href=https://modelunitednation.org/feed/>
    <link rel=alternate type=application/rss+xml title="Asia Youth International Model United Nations &raquo; Comments Feed" href=https://modelunitednation.org/comments/feed/>
    <noscript>
      <link id=wp-block-library rel=stylesheet href=https://modelunitednation.org/wp-includes/css/dist/block-library/style.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link id=wc-block-vendors-style rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css media=all type=text/css>
    </noscript>
    <noscript>
      <link id=wc-block-style rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=eae-css rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/css/eae.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=font-awesome-4-shim rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=font-awesome-5-all rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=vegas-css rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/vegas.min.css media=all type=text/css>
    </noscript>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="8cfcd8d6780e2ddc6bdb6570-|49">
    </script>
    <link id=bc9b8f2a3 rel=preload href=https://modelunitednation.org/wp-content/uploads/essential-addons-elementor/bc9b8f2a3.min.css as=style media=all onload="this.onload=null;this.rel='stylesheet'" type=text/css>
    <noscript>
      <link
            id=bc9b8f2a3 rel=stylesheet href=https://modelunitednation.org/wp-content/uploads/essential-addons-elementor/bc9b8f2a3.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=font-awesome rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=kava-theme-style rel=stylesheet href=https://modelunitednation.org/wp-content/themes/kava/style.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=blog-layouts-module rel=stylesheet href=https://modelunitednation.org/wp-content/themes/kava/inc/modules/blog-layouts/assets/css/blog-layouts-module.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=kava-woocommerce-style rel=stylesheet href=https://modelunitednation.org/wp-content/themes/kava/inc/modules/woo/assets/css/woo-module.css media=all type=text/css>
    </noscript>
    <noscript>
    </noscript>
    <noscript>
      <link
            id=jet-elements rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/jet-elements/assets/css/jet-elements.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=jet-elements-skin rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=elementor-icons rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=elementor-animations rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/lib/animations/animations.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=elementor-frontend-legacy rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/css/frontend-legacy.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=elementor-frontend rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/css/frontend.min.css media=all type=text/css>
    </noscript>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="8cfcd8d6780e2ddc6bdb6570-|49">
    </script>
    <link id=elementor-post-444782 rel=preload href=https://modelunitednation.org/wp-content/cache/autoptimize/css/autoptimize_single_f012b69251877dbfacc96da0d2794bef.css as=style media=all onload="this.onload=null;this.rel='stylesheet'" type=text/css>
    <noscript>
      <link
            id=elementor-post-444782 rel=stylesheet href=https://modelunitednation.org/wp-content/uploads/elementor/css/post-444782.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=lae-animate-styles rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/addons-for-elementor/assets/css/animate.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=lae-sliders-styles rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/addons-for-elementor/assets/css/sliders.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=lae-icomoon-styles rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/addons-for-elementor/assets/css/icomoon.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=lae-frontend-styles rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/addons-for-elementor/assets/css/lae-frontend.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=lae-widgets-styles rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/addons-for-elementor/assets/css/lae-widgets.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=jet-tricks-frontend rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/jet-tricks/assets/css/jet-tricks-frontend.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=jet-sticky-frontend rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/jetsticky-for-elementor/assets/css/jet-sticky-frontend.css media=all type=text/css>
    </noscript>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="8cfcd8d6780e2ddc6bdb6570-|49">
    </script>
    <link id=elementor-post-39363 rel=preload href=https://modelunitednation.org/wp-content/cache/autoptimize/css/autoptimize_single_9e8175999e768fe42ed3c8c6afb30b0e.css as=style media=all onload="this.onload=null;this.rel='stylesheet'" type=text/css>
    <noscript>
      <link
            id=elementor-post-39363 rel=stylesheet href=https://modelunitednation.org/wp-content/uploads/elementor/css/post-39363.css media=all type=text/css>
    </noscript>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="8cfcd8d6780e2ddc6bdb6570-|49">
    </script>
    <link id=elementor-post-316 rel=preload href=https://modelunitednation.org/wp-content/cache/autoptimize/css/autoptimize_single_4aa19ee98315aba6278e133bfb9dbaab.css as=style media=all onload="this.onload=null;this.rel='stylesheet'" type=text/css>
    <noscript>
      <link
            id=elementor-post-316 rel=stylesheet href=https://modelunitednation.org/wp-content/uploads/elementor/css/post-316.css media=all type=text/css>
    </noscript>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="8cfcd8d6780e2ddc6bdb6570-|49">
    </script>
    <link id=elementor-post-364 rel=preload href=https://modelunitednation.org/wp-content/cache/autoptimize/css/autoptimize_single_37a4d2f9b833e6e0db31bfe53702fd3d.css as=style media=all onload="this.onload=null;this.rel='stylesheet'" type=text/css>
    <noscript>
      <link
            id=elementor-post-364 rel=stylesheet href=https://modelunitednation.org/wp-content/uploads/elementor/css/post-364.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=kava-extra-nucleo-outline rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/kava-extra/assets/fonts/nucleo-outline-icon-font/nucleo-outline.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=rating_style rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/all-in-one-schemaorg-rich-snippets/css/jquery.rating.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=bsf_style rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/all-in-one-schemaorg-rich-snippets/css/style.css media=all type=text/css>
    </noscript>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="8cfcd8d6780e2ddc6bdb6570-|49">
    </script>
    <link id=popup-maker-site rel=preload href="https://modelunitednation.org/wp-content/cache/autoptimize/css/autoptimize_single_08bb8eabe9a58273c75749023705e5a7.css?generated=1597736261&#038;ver=1.11.2" as=style media=all onload="this.onload=null;this.rel='stylesheet'" type=text/css>
    <noscript>
      <link
            id=popup-maker-site rel=stylesheet href="//modelunitednation.org/wp-content/uploads/pum/pum-site-styles.css?generated=1597736261&#038;ver=1.11.2" media=all type=text/css>
    </noscript>
    <noscript>
    </noscript>
    <noscript>
      <link
            id=elementor-icons-shared-0 rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=elementor-icons-fa-regular rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=elementor-icons-fa-brands rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css media=all type=text/css>
    </noscript>
    <noscript>
      <link
            id=elementor-icons-fa-solid rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css media=all type=text/css>
    </noscript> 
    <script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script>
    <script src=https://modelunitednation.org/wp-includes/js/jquery/jquery.js id=jquery-core-js type="8cfcd8d6780e2ddc6bdb6570-text/javascript">
    </script> 
    <link rel=https://api.w.org/ href=https://modelunitednation.org/wp-json/>
    <link rel=alternate type=application/json href=https://modelunitednation.org/wp-json/wp/v2/pages/39363>
    <link rel=EditURI type=application/rsd+xml title=RSD href=https://modelunitednation.org/xmlrpc.php?rsd>
    <link rel=wlwmanifest type=application/wlwmanifest+xml href=https://modelunitednation.org/wp-includes/wlwmanifest.xml>
    <meta name=generator content="WordPress 5.5.1">
    <meta name=generator content="WooCommerce 4.5.2">
    <link rel=shortlink href='https://modelunitednation.org/?p=39363'>
    <link rel=alternate type=application/json+oembed href="https://modelunitednation.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fmodelunitednation.org%2Fayimun-2019%2F">
    <link rel=alternate type=text/xml+oembed href="https://modelunitednation.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fmodelunitednation.org%2Fayimun-2019%2F&#038;format=xml"> 
    <script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">(function () {
                        window.lae_fs = {can_use_premium_code: false};
                    })();</script> 
    <script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var ajaxurl = "https://modelunitednation.org/wp-admin/admin-ajax.php";</script>
    <meta name=google-site-verification content=TJ9t3MXux1JOaM4s6zjmzT0a2XFN39gwzpdyWjLtapo> 
    <script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
        h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
        (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,
        {'GTM-MXB4C94':true});</script> 
    <script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MXB4C94');</script> 
    <script type=application/ld+json>{
                "@context": "http://www.schema.org",
                "@type": "EducationEvent",
                "name": "AYIMUN - Asia Youth International Model United Nations 2019",
                "alternateName": "AYIMUN 2019",
                "image": {
                    "@type": "ImageObject",
                    "url": "https://modelunitednation.org/wp-content/uploads/2018/07/logo-ayimun-transparent.png",
                    "width": "472",
                    "height": "109"
                },
                "url": "https://modelunitednation.org/",
                "description": "Asia Youth International Model United Nations (AYIMUN) 2019 is international United Nation simulation conference where youth can share thoughts/ideas about social issues and learn diplomacy, leadership, networking & negotiation skills.",
                "startDate": "2019-08-25",
                "endDate": "2019-08-28",
                "eventStatus": "EventScheduled",
                "typicalAgeRange": "15-27",
                "location": {
                    "@type": "Place",
                    "name": "Putrajaya",
                    "address": {
                        "addressLocality": "Putrajaya",
                        "addressCountry": "Malaysia"
                    }
                },
                "organizer": {
                    "name": "Asia Youth International Model United Nations"
                }
                ,"sameAs": [
                "https://www.modelunitednation.org"
                ,"https://www.facebook.com/internationalmodelun/"
                ]
            }</script> 
    <script type=application/ld+json>{
                "@context": "http://www.schema.org",
                "@type": "Organization",
                "name": "Asia Youth International Model United Nations",
                "url": "https://www.modelunitednation.org",
                "location": {
                    "@type": "Place",
                    "name": "Putrajaya",
                    "address": {
                        "addressLocality": "Putrajaya",
                        "addressCountry": "Malaysia"
                    }
                }
                }</script> 
    <script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">(function() {
                            var formObjects = [];
                            window.hbsptReady = function(formObject) {
                                if (!formObject) {
                                    for (var i in formObjects) {
                                        hbspt.forms.create(formObjects[i]);
                                    };
                                    formObjects = [];
                                } else if (window.hbspt && window.hbspt.forms) {
                                    hbspt.forms.create(formObject);
                                } else {
                                    formObjects.push(formObject);
                                }
                            };
                        })();</script> 
    <script id=speed-up-optimize-css-delivery type="8cfcd8d6780e2ddc6bdb6570-text/javascript">/* loadCSS. [c]2018 Filament Group, Inc. MIT License */ (function(w){"use strict";if(!w.loadCSS){w.loadCSS=function(){}}
        var rp=loadCSS.relpreload={};rp.support=(function(){var ret;try{ret=w.document.createElement("link").relList.supports("preload")}catch(e){ret=!1}
        return function(){return ret}})();rp.bindMediaToggle=function(link){var finalMedia=link.media||"all";function enableStylesheet(){link.media=finalMedia}
        if(link.addEventListener){link.addEventListener("load",enableStylesheet)}else if(link.attachEvent){link.attachEvent("onload",enableStylesheet)}
        setTimeout(function(){link.rel="stylesheet";link.media="only x"});setTimeout(enableStylesheet,3000)};rp.poly=function(){if(rp.support()){return}
        var links=w.document.getElementsByTagName("link");for(var i=0;i<links.length;i++){var link=links[i];if(link.rel==="preload"&&link.getAttribute("as")==="style"&&!link.getAttribute("data-loadcss")){link.setAttribute("data-loadcss",!0);rp.bindMediaToggle(link)}}};if(!rp.support()){rp.poly();var run=w.setInterval(rp.poly,500);if(w.addEventListener){w.addEventListener("load",function(){rp.poly();w.clearInterval(run)})}else if(w.attachEvent){w.attachEvent("onload",function(){rp.poly();w.clearInterval(run)})}}
        if(typeof exports!=="undefined"){exports.loadCSS=loadCSS}
        else{w.loadCSS=loadCSS}}(typeof global!=="undefined"?global:this))</script> 
    <noscript>
      <style>.woocommerce-product-gallery{
        opacity: 1 !important;
        }
      </style>
    </noscript>
    <link rel=icon href=https://modelunitednation.org/wp-content/uploads/2019/01/cropped-AYIMUN_Logo_trspnt-32x32.png sizes=32x32>
    <link rel=icon href=https://modelunitednation.org/wp-content/uploads/2019/01/cropped-AYIMUN_Logo_trspnt-192x192.png sizes=192x192>
    <link rel=apple-touch-icon href=https://modelunitednation.org/wp-content/uploads/2019/01/cropped-AYIMUN_Logo_trspnt-180x180.png>
    <meta name=msapplication-TileImage content=https://modelunitednation.org/wp-content/uploads/2019/01/cropped-AYIMUN_Logo_trspnt-270x270.png>
    <noscript>
      <style id=rocket-lazyload-nojs-css>.rll-youtube-player, [data-lazy-src]{
        display:none !important;
        }
      </style>
    </noscript>
  </head>
  <body data-rsssl=1 class="page-template page-template-elementor_header_footer page page-id-39363 wp-custom-logo theme-kava woocommerce-no-js group-blog layout-fullwidth blog-creative woocommerce-active elementor-default elementor-template-full-width elementor-kit-444782 elementor-page elementor-page-39363 currency-usd">
    <div id=page class=site>
      <a class="skip-link screen-reader-text" href=#content>Skip to content
      </a>
      <header id=masthead class="site-header ">
        <div data-elementor-type=jet_header data-elementor-id=316 class="elementor elementor-316" data-elementor-settings=[]>
          <div class=elementor-inner>
            <div class=elementor-section-wrap>
              <header class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-20cd994 elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=20cd994 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;,&quot;jet_sticky_section_sticky&quot;:&quot;yes&quot;,&quot;jet_sticky_section_sticky_visibility&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}>
                <div class="elementor-container elementor-column-gap-no">
                  <div class=elementor-row>
                    <div class="has_eae_slider elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-793736b" data-id=793736b data-element_type=column>
                      <div class="elementor-column-wrap elementor-element-populated">
                        <div class=elementor-widget-wrap>
                          <div class="elementor-element elementor-element-4e37765 elementor-widget elementor-widget-image" data-id=4e37765 data-element_type=widget data-widget_type=image.default>
                            <div class=elementor-widget-container>
                              <div class=elementor-image>
                                <a href=https://modelunitednation.org>
                                  <img width=472 height=109 src=https://modelunitednation.org/wp-content/uploads/2018/07/logo-ayimun-transparent.png class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2018/07/logo-ayimun-transparent.png 472w, https://modelunitednation.org/wp-content/uploads/2018/07/logo-ayimun-transparent-300x69.png 300w" sizes="(max-width: 472px) 100vw, 472px"> 
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="has_eae_slider elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-c9c7c76" data-id=c9c7c76 data-element_type=column>
                      <div class=elementor-column-wrap>
                        <div class=elementor-widget-wrap>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </header>
            </div>
          </div>
        </div>
      </header>
      <div id=content class="site-content ">
        <div data-elementor-type=wp-post data-elementor-id=39363 class="elementor elementor-39363" data-elementor-settings=[]>
          <div class=elementor-inner>
            <div class=elementor-section-wrap>
              <div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-cba6cd1 elementor-section-full_width elementor-section-height-min-height elementor-section-content-middle elementor-section-items-top elementor-section-height-default" data-id=cba6cd1 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                <div class=elementor-background-overlay>
                </div>
                <div class="elementor-container elementor-column-gap-no">
                  <div class=elementor-row>
                    <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4dfa4ba" data-id=4dfa4ba data-element_type=column>
                      <div class="elementor-column-wrap elementor-element-populated">
                        <div class=elementor-widget-wrap>
                          <div class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-6e344e6 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=6e344e6 data-element_type=section>
                            <div class="elementor-container elementor-column-gap-default">
                              <div class=elementor-row>
                                <div class="has_eae_slider elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-03e5b34" data-id=03e5b34 data-element_type=column>
                                  <div class=elementor-column-wrap>
                                    <div class=elementor-widget-wrap>
                                    </div>
                                  </div>
                                </div>
                                <div class="has_eae_slider elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-39213fe" data-id=39213fe data-element_type=column>
                                  <div class="elementor-column-wrap elementor-element-populated">
                                    <div class=elementor-widget-wrap>
                                      <div class="elementor-element elementor-element-8ccdea2 elementor-widget elementor-widget-image" data-id=8ccdea2 data-element_type=widget data-widget_type=image.default>
                                        <div class=elementor-widget-container>
                                          <div class=elementor-image>
                                            <img width=580 height=156 src=https://modelunitednation.org/wp-content/uploads/2019/02/logo-header.png class="attachment-full size-full" alt loading=lazy>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-342b905 elementor-widget elementor-widget-heading" data-id=342b905 data-element_type=widget data-widget_type=heading.default>
                                        <div class=elementor-widget-container>
                                          <h2 class="elementor-heading-title elementor-size-default">"Human Security Agenda in The Globalized World"
                                          </h2>
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-af50ad5 elementor-widget elementor-widget-heading" data-id=af50ad5 data-element_type=widget data-widget_type=heading.default>
                                        <div class=elementor-widget-container>
                                          <h2 class="elementor-heading-title elementor-size-default">25th August - 28th August 2019
                                            <br>
                                            Putrajaya International Convention Centre, Malaysia
                                          </h2>
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-25618b2 elementor-align-right elementor-mobile-align-right elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button" data-id=25618b2 data-element_type=widget data-widget_type=button.default>
                                        <div class=elementor-widget-container>
                                          <div class=elementor-button-wrapper>
                                            <a href=https://modelunitednation.org/waiting-list/ class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                              <span class=elementor-button-content-wrapper>
                                                <span class=elementor-button-text>I Want To Be a Delegate
                                                </span>
                                              </span>
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-2b9e14f elementor-align-right elementor-mobile-align-right elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button" data-id=2b9e14f data-element_type=widget data-widget_type=button.default>
                                        <div class=elementor-widget-container>
                                          <div class=elementor-button-wrapper>
                                            <a href=# class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                              <span class=elementor-button-content-wrapper>
                                                <span class=elementor-button-text>Registration has CLOSED
                                                </span>
                                              </span>
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3f3f822 elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-heading" data-id=3f3f822 data-element_type=widget data-widget_type=heading.default>
                                        <div class=elementor-widget-container>
                                          <h2 class="elementor-heading-title elementor-size-default">Registration Close in:
                                          </h2>
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-d66c525 elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-premium-countdown-timer" data-id=d66c525 data-element_type=widget data-widget_type=premium-countdown-timer.default>
                                        <div class=elementor-widget-container>
                                          <div id=countDownContiner-d66c525 class="premium-countdown premium-countdown-separator-" data-settings='{"label1":"Year,Month,Week,Day,Hour,Minute,Second","label2":"Years,Months,Weeks,Days,Hours,Minutes,Seconds","until":"2019\/05\/15 23:59","format":"DHMS","event":"onExpiry","text":"<p>Registration Has Closed<\/p>","serverSync":"2020\/10\/09 22:54:14","separator":""}'>
                                            <div id=countdown-d66c525 class="premium-countdown-init countdown down">
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-21c777f elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=21c777f data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                <div class="elementor-container elementor-column-gap-no">
                  <div class=elementor-row>
                    <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-b59427b" data-id=b59427b data-element_type=column>
                      <div class="elementor-column-wrap elementor-element-populated">
                        <div class=elementor-widget-wrap>
                          <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-f91542d elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=f91542d data-element_type=section>
                            <div class="elementor-container elementor-column-gap-no">
                              <div class=elementor-row>
                                <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-2f6791b" data-id=2f6791b data-element_type=column>
                                  <div class="elementor-column-wrap elementor-element-populated">
                                    <div class=elementor-widget-wrap>
                                      <div class="elementor-element elementor-element-438da12 elementor-widget elementor-widget-premium-addon-dual-header" data-id=438da12 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
                                        <div class=elementor-widget-container>
                                          <div class=premium-dual-header-container>
                                            <div class=premium-dual-header-first-container>
                                              <h2 class="premium-dual-header-first-header ">
                                                <span class=premium-dual-header-first-span>News 
                                                </span>
                                                <span class="premium-dual-header-second-header ">
                                                </span>
                                              </h2>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-737dd03 elementor-widget elementor-widget-divider" data-id=737dd03 data-element_type=widget data-widget_type=divider.default>
                                        <div class=elementor-widget-container>
                                          <div class=elementor-divider>
                                            <span class=elementor-divider-separator>
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-529200c elementor-widget elementor-widget-lae-posts-carousel" data-id=529200c data-element_type=widget data-widget_type=lae-posts-carousel.default>
                                        <div class=elementor-widget-container>
                                          <div id=lae-posts-carousel-5f8087a673148 class="lae-posts-carousel lae-container lae-classic-skin" data-settings='{"arrows":true,"dots":true,"autoplay":true,"autoplay_speed":3000,"animation_speed":300,"pause_on_hover":true,"adaptive_height":false,"display_columns":3,"scroll_columns":3,"gutter":10,"tablet_width":800,"tablet_display_columns":2,"tablet_scroll_columns":2,"tablet_gutter":10,"mobile_width":480,"mobile_display_columns":1,"mobile_scroll_columns":1,"mobile_gutter":10}'>
                                            <div data-id=id-436339 class=lae-posts-carousel-item>
                                              <article id=post-436339 class="post-436339 post type-post status-publish format-standard has-post-thumbnail hentry category-pressrelease">
                                                <div class=lae-project-image>
                                                  <a href=https://modelunitednation.org/news-from-grand-symposium-asia-youth-model-united-nations/>
                                                  <img width=300 height=200 src=https://modelunitednation.org/wp-content/uploads/2019/09/news_grandsymposium-min-300x200.jpg class="lae-image attachment-medium size-medium" alt=news_grandsymposium-min loading=lazy title=news_grandsymposium-min srcset="https://modelunitednation.org/wp-content/uploads/2019/09/news_grandsymposium-min-300x200.jpg 300w, https://modelunitednation.org/wp-content/uploads/2019/09/news_grandsymposium-min-768x512.jpg 768w, https://modelunitednation.org/wp-content/uploads/2019/09/news_grandsymposium-min-600x400.jpg 600w, https://modelunitednation.org/wp-content/uploads/2019/09/news_grandsymposium-min-510x340.jpg 510w, https://modelunitednation.org/wp-content/uploads/2019/09/news_grandsymposium-min.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px">
                                                  </a>
                                                <div class=lae-image-info>
                                                  <div class=lae-entry-info>
                                                    <h3 class="lae-post-title">
                                                      <a href=https://modelunitednation.org/news-from-grand-symposium-asia-youth-model-united-nations/ title="News from Grand Symposium Asia Youth Model United Nations" rel=bookmark>News from Grand Symposium Asia Youth Model United Nations
                                                      </a>
                                                    </h3>
                                                    <span class=lae-terms>
                                                      <a href=https://modelunitednation.org/category/pressrelease/>PressRelease
                                                      </a>
                                                    </span>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="lae-entry-text-wrap ">
                                              <div class=lae-entry-meta>
                                                <span class=published>
                                                  <abbr title="Tuesday, September, 2019, 3:10 pm">10 September 2019
                                                  </abbr>
                                                </span>
                                              </div>
                                              <div class=entry-summary>PUTRAJAYA, Malaysia — The Asia Youth International Model United Nations (AYIMUN) was successfully held in the Putrajaya International Convention Centre (PICC) on 25 to 28 August, witnessed participation of 1518 delegates worldwide. The event was also graced by eminent speakers including Charles Mohan, the CEO and founder of Institut Onn Jaafar (IOJ), Frederika Alexis Cull [&hellip;]
                                              </div>
                                            </div>
                                            </article>
                                        </div>
                                        <div data-id=id-436341 class=lae-posts-carousel-item>
                                          <article id=post-436341 class="post-436341 post type-post status-publish format-standard has-post-thumbnail hentry category-pressrelease">
                                            <div class=lae-project-image>
                                              <a href=https://modelunitednation.org/committee-session-day2/>
                                              <img width=300 height=200 src=https://modelunitednation.org/wp-content/uploads/2019/09/news_commiittee_session_day2-300x200.jpg class="lae-image attachment-medium size-medium" alt=news_commiittee_session_day2 loading=lazy title=news_commiittee_session_day2 srcset="https://modelunitednation.org/wp-content/uploads/2019/09/news_commiittee_session_day2-300x200.jpg 300w, https://modelunitednation.org/wp-content/uploads/2019/09/news_commiittee_session_day2-510x340.jpg 510w, https://modelunitednation.org/wp-content/uploads/2019/09/news_commiittee_session_day2.jpg 512w" sizes="(max-width: 300px) 100vw, 300px">
                                              </a>
                                            <div class=lae-image-info>
                                              <div class=lae-entry-info>
                                                <h3 class="lae-post-title">
                                                  <a href=https://modelunitednation.org/committee-session-day2/ title="News from Asia Youth Model United Nations Committee Session Day 2" rel=bookmark>News from Asia Youth Model United Nations Committee Session Day 2
                                                  </a>
                                                </h3>
                                                <span class=lae-terms>
                                                  <a href=https://modelunitednation.org/category/pressrelease/>PressRelease
                                                  </a>
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="lae-entry-text-wrap ">
                                          <div class=lae-entry-meta>
                                            <span class=published>
                                              <abbr title="Tuesday, September, 2019, 3:16 pm">10 September 2019
                                              </abbr>
                                            </span>
                                          </div>
                                          <div class=entry-summary>During the second day of Committee Session, the WTO (World Trade Organization) council had a special guest, the High Commissioner Kenya for Malaysia, Mr. Francis N. Muhoro. In front of the impressed Delegates, he delivered a speech regarding human security, particularly related to economic stability, by giving an example in Cambodia. Besides listening to his [&hellip;]
                                          </div>
                                        </div>
                                        </article>
                                    </div>
                                    <div data-id=id-436335 class=lae-posts-carousel-item>
                                      <article id=post-436335 class="post-436335 post type-post status-publish format-standard has-post-thumbnail hentry category-pressrelease">
                                        <div class=lae-project-image>
                                          <a href=https://modelunitednation.org/committee-session-day-1/>
                                          <img width=300 height=200 src=https://modelunitednation.org/wp-content/uploads/2019/09/news_commiittee_session_day1-300x200.jpg class="lae-image attachment-medium size-medium" alt=news_commiittee_session_day1 loading=lazy title=news_commiittee_session_day1 srcset="https://modelunitednation.org/wp-content/uploads/2019/09/news_commiittee_session_day1-300x200.jpg 300w, https://modelunitednation.org/wp-content/uploads/2019/09/news_commiittee_session_day1-510x340.jpg 510w, https://modelunitednation.org/wp-content/uploads/2019/09/news_commiittee_session_day1.jpg 512w" sizes="(max-width: 300px) 100vw, 300px">
                                          </a>
                                        <div class=lae-image-info>
                                          <div class=lae-entry-info>
                                            <h3 class="lae-post-title">
                                              <a href=https://modelunitednation.org/committee-session-day-1/ title="News from Asia Youth Model United Nations Committee Session Day 1" rel=bookmark>News from Asia Youth Model United Nations Committee Session Day 1
                                              </a>
                                            </h3>
                                            <span class=lae-terms>
                                              <a href=https://modelunitednation.org/category/pressrelease/>PressRelease
                                              </a>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="lae-entry-text-wrap ">
                                      <div class=lae-entry-meta>
                                        <span class=published>
                                          <abbr title="Tuesday, September, 2019, 2:34 pm">10 September 2019
                                          </abbr>
                                        </span>
                                      </div>
                                      <div class=entry-summary>The first day of AYIMUN committee session was also the first time for most Delegates to participate in Model UN. They had prepared the important substances regarding the topic and the country they would be representing, as well as the materials and motions they wished to discuss with the other delegates. Although the Delegates were [&hellip;]
                                      </div>
                                    </div>
                                    </article>
                                </div>
                                <div data-id=id-436342 class=lae-posts-carousel-item>
                                  <article id=post-436342 class="post-436342 post type-post status-publish format-standard has-post-thumbnail hentry category-pressrelease">
                                    <div class=lae-project-image>
                                      <a href=https://modelunitednation.org/belgium-ambassador-speech/>
                                      <img width=300 height=200 src=https://modelunitednation.org/wp-content/uploads/2019/09/beligia_ambassadors_meeting-300x200.jpg class="lae-image attachment-medium size-medium" alt=beligia_ambassadors_meeting loading=lazy title=beligia_ambassadors_meeting srcset="https://modelunitednation.org/wp-content/uploads/2019/09/beligia_ambassadors_meeting-300x200.jpg 300w, https://modelunitednation.org/wp-content/uploads/2019/09/beligia_ambassadors_meeting-768x512.jpg 768w, https://modelunitednation.org/wp-content/uploads/2019/09/beligia_ambassadors_meeting-600x400.jpg 600w, https://modelunitednation.org/wp-content/uploads/2019/09/beligia_ambassadors_meeting-510x340.jpg 510w, https://modelunitednation.org/wp-content/uploads/2019/09/beligia_ambassadors_meeting.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px">
                                      </a>
                                    <div class=lae-image-info>
                                      <div class=lae-entry-info>
                                        <h3 class="lae-post-title">
                                          <a href=https://modelunitednation.org/belgium-ambassador-speech/ title="Belgium Ambassador’s Speech in AYIMUN 2019" rel=bookmark>Belgium Ambassador’s Speech in AYIMUN 2019
                                          </a>
                                        </h3>
                                        <span class=lae-terms>
                                          <a href=https://modelunitednation.org/category/pressrelease/>PressRelease
                                          </a>
                                        </span>
                                    </div>
                                    </div>
                                </div>
                                <div class="lae-entry-text-wrap ">
                                  <div class=lae-entry-meta>
                                    <span class=published>
                                      <abbr title="Tuesday, September, 2019, 3:13 pm">10 September 2019
                                      </abbr>
                                    </span>
                                  </div>
                                  <div class=entry-summary>“We think we are in peace, when the whole world is preparing for war” The quote was stated by the Belgium Ambassador for Malaysia, H.E Pascal H. Grégoire in his Opening Remarks for AYIMUN 2019 under the theme Human Security In the Globalized World. Given the reality that many people still believe that they are [&hellip;]
                                  </div>
                                </div>
                                </article>
                            </div>
                            <div data-id=id-436340 class=lae-posts-carousel-item>
                              <article id=post-436340 class="post-436340 post type-post status-publish format-standard has-post-thumbnail hentry category-pressrelease">
                                <div class=lae-project-image>
                                  <a href=https://modelunitednation.org/attended-dinner-reception-in-belgium-residence/>
                                  <img width=300 height=200 src=https://modelunitednation.org/wp-content/uploads/2019/09/Attended-Dinner-Reception-in-Belgium-Residence-300x200.jpg class="lae-image attachment-medium size-medium" alt="Attended Dinner Reception in Belgium Residence" loading=lazy title="Attended Dinner Reception in Belgium Residence" srcset="https://modelunitednation.org/wp-content/uploads/2019/09/Attended-Dinner-Reception-in-Belgium-Residence-300x200.jpg 300w, https://modelunitednation.org/wp-content/uploads/2019/09/Attended-Dinner-Reception-in-Belgium-Residence-768x512.jpg 768w, https://modelunitednation.org/wp-content/uploads/2019/09/Attended-Dinner-Reception-in-Belgium-Residence-1024x683.jpg 1024w, https://modelunitednation.org/wp-content/uploads/2019/09/Attended-Dinner-Reception-in-Belgium-Residence-600x400.jpg 600w, https://modelunitednation.org/wp-content/uploads/2019/09/Attended-Dinner-Reception-in-Belgium-Residence-510x340.jpg 510w, https://modelunitednation.org/wp-content/uploads/2019/09/Attended-Dinner-Reception-in-Belgium-Residence.jpg 1600w" sizes="(max-width: 300px) 100vw, 300px">
                                  </a>
                                <div class=lae-image-info>
                                  <div class=lae-entry-info>
                                    <h3 class="lae-post-title">
                                      <a href=https://modelunitednation.org/attended-dinner-reception-in-belgium-residence/ title="AYIMUN (Asia Youth International Model United Nations) Attended Dinner Reception in Belgium Residence" rel=bookmark>AYIMUN (Asia Youth International Model United Nations) Attended Dinner Reception in Belgium Residence
                                      </a>
                                    </h3>
                                    <span class=lae-terms>
                                      <a href=https://modelunitednation.org/category/pressrelease/>PressRelease
                                      </a>
                                    </span>
                                </div>
                                </div>
                            </div>
                            <div class="lae-entry-text-wrap ">
                              <div class=lae-entry-meta>
                                <span class=published>
                                  <abbr title="Tuesday, September, 2019, 3:29 pm">10 September 2019
                                  </abbr>
                                </span>
                              </div>
                              <div class=entry-summary>As the recognition of the success of AYIMUN 2019, the organisers and Board of Directors of AYIMUN obtained an honour to receive a personal invitation by the Belgium Ambassador for Malaysia, H.E Pascal H. Grégoire as his exclusive guests in the Belgium House on August 28, 2019. In this special opportunity, the Embassy of Belgium [&hellip;]
                              </div>
                            </div>
                            </article>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </section>
    </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-a96bdb3 elementor-section-content-middle elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=a96bdb3 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-wider">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-f2307ee" data-id=f2307ee data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-3a40708 elementor-aspect-ratio-169 elementor-widget elementor-widget-video" data-id=3a40708 data-element_type=widget data-settings={&quot;aspect_ratio&quot;:&quot;169&quot;} data-widget_type=video.default>
              <div class=elementor-widget-container>
                <div class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
                  <iframe class=elementor-video-iframe allowfullscreen title="youtube Video Player" src="https://www.youtube.com/embed/ru3haZe2oXM?feature=oembed&amp;start&amp;end&amp;wmode=opaque&amp;loop=0&amp;controls=1&amp;mute=0&amp;rel=0&amp;modestbranding=0">
                  </iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-a7d3630" data-id=a7d3630 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-3e3c93c elementor-widget elementor-widget-premium-addon-dual-header" data-id=3e3c93c data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>WHAT AYIMUN IS 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-ee8b79c elementor-widget elementor-widget-divider" data-id=ee8b79c data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-e371e2b elementor-widget elementor-widget-text-editor" data-id=e371e2b data-element_type=widget data-widget_type=text-editor.default>
              <div class=elementor-widget-container>
                <div class="elementor-text-editor elementor-clearfix">
                  <h3>
                    <b>We Believe That The World Needs Youth&#8217;s Voice!
                    </b>
                  </h3>
                  <p>
                    <b>Asia Youth International Model United Nations (AYIMUN)
                    </b> is a platform where youth mentality in leadership, negotiation and diplomacy will be developed in a Model United Nations.
                  </p>
                  <p>
                    <b>AYIMUN
                    </b> aims to engage youth leaders from all over the world and to provide a platform to share perspectives in opinions with the theme “
                    <b>Human security agenda in the globalized world
                    </b>”
                  </p>
                  <p>We wish, by joining 
                    <b>AYIMUN 2019
                    </b>, You could enhance capabilities and encourage yourself to develop networks, also improve your communication and diplomatic skills.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-4a88e62 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=4a88e62 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-wider">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-2885cd1" data-id=2885cd1 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-dd92fa6 elementor-widget elementor-widget-premium-addon-dual-header" data-id=dd92fa6 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>WHAT YOU WILL GET 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-00cd860 elementor-widget elementor-widget-divider" data-id=00cd860 data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-6d36061 elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id=6d36061 data-element_type=widget data-widget_type=image-box.default>
              <div class=elementor-widget-container>
                <div class=elementor-image-box-wrapper>
                  <figure class=elementor-image-box-img>
                    <img width=54 height=54 src=https://modelunitednation.org/wp-content/uploads/2019/02/ayimun-logo-1.png class="attachment-full size-full" alt loading=lazy>
                  </figure>
                  <div class=elementor-image-box-content>
                    <h3 class="elementor-image-box-title">Chance to implement your idea of problem solving
                    </h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-b119c59 elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id=b119c59 data-element_type=widget data-widget_type=image-box.default>
              <div class=elementor-widget-container>
                <div class=elementor-image-box-wrapper>
                  <figure class=elementor-image-box-img>
                    <img width=54 height=54 src=https://modelunitednation.org/wp-content/uploads/2019/02/ayimun-logo-2.png class="attachment-full size-full" alt loading=lazy>
                  </figure>
                  <div class=elementor-image-box-content>
                    <h3 class="elementor-image-box-title">World level recognition
                    </h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-1e08558 elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id=1e08558 data-element_type=widget data-widget_type=image-box.default>
              <div class=elementor-widget-container>
                <div class=elementor-image-box-wrapper>
                  <figure class=elementor-image-box-img>
                    <img width=54 height=54 src=https://modelunitednation.org/wp-content/uploads/2019/02/ayimun-logo-3.png class="attachment-full size-full" alt loading=lazy>
                  </figure>
                  <div class=elementor-image-box-content>
                    <h3 class="elementor-image-box-title">Experimental learning to be future diplomat
                    </h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-1a3b38d elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id=1a3b38d data-element_type=widget data-widget_type=image-box.default>
              <div class=elementor-widget-container>
                <div class=elementor-image-box-wrapper>
                  <figure class=elementor-image-box-img>
                    <img width=54 height=54 src=https://modelunitednation.org/wp-content/uploads/2019/02/ayimun-logo-4.png class="attachment-full size-full" alt loading=lazy>
                  </figure>
                  <div class=elementor-image-box-content>
                    <h3 class="elementor-image-box-title">Meeting and networking with the best youth leaders of the world
                    </h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-5aa15a7 elementor-widget elementor-widget-text-editor" data-id=5aa15a7 data-element_type=widget data-widget_type=text-editor.default>
              <div class=elementor-widget-container>
                <div class="elementor-text-editor elementor-clearfix">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-33d6157" data-id=33d6157 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-fbe400f elementor-widget elementor-widget-image" data-id=fbe400f data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=493 height=567 src=https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN3.jpg class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN3.jpg 493w, https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN3-261x300.jpg 261w" sizes="(max-width: 493px) 100vw, 493px">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-d10e3e8 elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=d10e3e8 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-02ff8b1" data-id=02ff8b1 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-f328bc4 elementor-widget elementor-widget-spacer" data-id=f328bc4 data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
<script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">jQuery(document).ready(function () {
                        jQuery(".elementor-element-5c4d913").prepend('<div class=eae-section-bs><div class=eae-section-bs-inner></div></div>');
                        var bgimage = '';
                        if ('yes' == 'yes') {
                            //if(bgimage == ''){
                            //    var bgoverlay = '';
                            //}else{
                            var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder//assets/lib/vegas/overlays/00.png';
                            // }
                        } else {
                            if ('') {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/.png';
                            } else {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/00.png';
                            }
                        }
                        jQuery(".elementor-element-5c4d913").children('.eae-section-bs').children('.eae-section-bs-inner').vegas({
                            slides: [{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto5.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN2.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN3.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN4.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/Diplomatic-Dinner.jpg"}],
                            transition: 'fade2',
                            animation: 'kenburnsLeft',
                            overlay: bgoverlay,
                            cover: true,
                            delay: 4000,
                            timer: false                });
                        if ('yes' == 'yes') {
                            jQuery(".elementor-element-5c4d913").children('.eae-section-bs').children('.eae-section-bs-inner').children('.vegas-overlay').css('background-image', '');
                        }
                    });</script> 
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-5c4d913 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=5c4d913 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;pyramids&quot;}>
  <div class=elementor-background-overlay>
  </div>
  <div class="elementor-shape elementor-shape-top" data-negative=false>
    <svg xmlns=http://www.w3.org/2000/svg viewBox="0 0 1000 100" preserveAspectRatio=none>
      <path class=elementor-shape-fill d=M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9 />
    </svg>
  </div>
  <div class="elementor-container elementor-column-gap-default">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-77c7b58" data-id=77c7b58 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-8e8a944 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=8e8a944 data-element_type=section>
              <div class="elementor-container elementor-column-gap-default">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d3a5505" data-id=d3a5505 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-6c93526 elementor-widget elementor-widget-heading" data-id=6c93526 data-element_type=widget data-widget_type=heading.default>
                          <div class=elementor-widget-container>
                            <h2 class="elementor-heading-title elementor-size-default">Its YOUR time
                            </h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-76cbaf9 elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=76cbaf9 data-element_type=section>
              <div class="elementor-container elementor-column-gap-default">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d3ffff9" data-id=d3ffff9 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-79e06bb elementor-align-center elementor-mobile-align-center elementor-widget elementor-widget-button" data-id=79e06bb data-element_type=widget data-widget_type=button.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-button-wrapper>
                              <a href=https://modelunitednation.org/waiting-list/ class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                <span class=elementor-button-content-wrapper>
                                  <span class=elementor-button-text>I Want To Be a Delegate
                                  </span>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-0560bff elementor-align-right elementor-mobile-align-right elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button" data-id=0560bff data-element_type=widget data-widget_type=button.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-button-wrapper>
                              <a href=# class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                <span class=elementor-button-content-wrapper>
                                  <span class=elementor-button-text>Registration has CLOSED
                                  </span>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="has_eae_slider elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-c101e4d" data-id=c101e4d data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-a55c524 elementor-align-center elementor-mobile-align-center elementor-widget elementor-widget-button" data-id=a55c524 data-element_type=widget data-widget_type=button.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-button-wrapper>
                              <a href=https://modelunitednation.org/result class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                <span class=elementor-button-content-wrapper>
                                  <span class=elementor-button-text>Check Your Result
                                  </span>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div class="elementor-element elementor-element-e941bda elementor-widget elementor-widget-spacer" data-id=e941bda data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-ef3c9ee elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=ef3c9ee data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3261352" data-id=3261352 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-42a4733 elementor-widget elementor-widget-spacer" data-id=42a4733 data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-f763982 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=f763982 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1f1b5d3" data-id=1f1b5d3 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-0d85d4c elementor-widget elementor-widget-premium-addon-dual-header" data-id=0d85d4c data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>DELEGATES REQUIREMENTS 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-23f4a19 elementor-widget elementor-widget-divider" data-id=23f4a19 data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-3d61651 elementor-widget elementor-widget-text-editor" data-id=3d61651 data-element_type=widget data-widget_type=text-editor.default>
              <div class=elementor-widget-container>
                <div class="elementor-text-editor elementor-clearfix">
                  <h3>As we mentioned before, this  event is exclusively for those who passed our strict screening process.
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-f9c5e25 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=f9c5e25 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-c74035a" data-id=c74035a data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-c08764e eael-infobox-content-align-center eael-infobox-shape-square eael-infobox-hover-img-shape-square elementor-widget elementor-widget-eael-info-box" data-id=c08764e data-element_type=widget data-widget_type=eael-info-box.default>
              <div class=elementor-widget-container>
                <div class=eael-infobox>
                  <div class=infobox-icon>
                    <img src=https://modelunitednation.org/wp-content/uploads/2019/04/Single.jpg alt>
                  </div>
                  <div class=infobox-content>
                    <h4 class="title">Single
                    </h4>
                    <p>
                    <p>You choose to register individually. You’ll become an independent delegate, conquering the floor like a mighty warrior you are. How incredible, right?
                    </p>
                    </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-c21ac1b" data-id=c21ac1b data-element_type=column>
      <div class="elementor-column-wrap elementor-element-populated">
        <div class=elementor-widget-wrap>
          <div class="elementor-element elementor-element-bbfa6ea elementor-widget elementor-widget-text-editor" data-id=bbfa6ea data-element_type=widget data-widget_type=text-editor.default>
            <div class=elementor-widget-container>
              <div class="elementor-text-editor elementor-clearfix">
                <h3>OR
                </h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-ef69be6" data-id=ef69be6 data-element_type=column>
      <div class="elementor-column-wrap elementor-element-populated">
        <div class=elementor-widget-wrap>
          <div class="elementor-element elementor-element-5d721c9 eael-infobox-content-align-center eael-infobox-shape-square eael-infobox-hover-img-shape-square elementor-widget elementor-widget-eael-info-box" data-id=5d721c9 data-element_type=widget data-widget_type=eael-info-box.default>
            <div class=elementor-widget-container>
              <div class=eael-infobox>
                <div class=infobox-icon>
                  <img src=https://modelunitednation.org/wp-content/uploads/2019/04/Group.jpg alt>
                </div>
                <div class=infobox-content>
                  <h4 class="title">Group
                  </h4>
                  <p>
                  <p>You choose to register in group consists of 5– 15 members. You’ll join forces with remarkable people, trying to solve global issues. Follow group terms
                  </p>
                  </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-9ef31a2" data-id=9ef31a2 data-element_type=column>
    <div class="elementor-column-wrap elementor-element-populated">
      <div class=elementor-widget-wrap>
        <div class="elementor-element elementor-element-e3935d8 elementor-widget elementor-widget-text-editor" data-id=e3935d8 data-element_type=widget data-widget_type=text-editor.default>
          <div class=elementor-widget-container>
            <div class="elementor-text-editor elementor-clearfix">
              <h3>OR
              </h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-cb6a080" data-id=cb6a080 data-element_type=column>
    <div class="elementor-column-wrap elementor-element-populated">
      <div class=elementor-widget-wrap>
        <div class="elementor-element elementor-element-78a6bc0 eael-infobox-content-align-center eael-infobox-shape-square eael-infobox-hover-img-shape-square elementor-widget elementor-widget-eael-info-box" data-id=78a6bc0 data-element_type=widget data-widget_type=eael-info-box.default>
          <div class=elementor-widget-container>
            <div class=eael-infobox>
              <div class=infobox-icon>
                <img src=https://modelunitednation.org/wp-content/uploads/2019/04/Observer.jpg alt>
              </div>
              <div class=infobox-content>
                <h4 class="title">Observer
                </h4>
                <p>
                <p>You choose to register as Observer. This applies for anyone over age 25 or below 17 (minimum 14 for non malaysian and 13 for malaysian). You join this conference to survey, not taking part. Follow observer terms.
                </p>
                </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-1c4b4fc elementor-section-content-middle elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=1c4b4fc data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-8305066" data-id=8305066 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-00d08b3 elementor-widget elementor-widget-premium-addon-dual-header" data-id=00d08b3 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>DELEGATES REQUIREMENTS 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-387a1f7 elementor-widget elementor-widget-divider" data-id=387a1f7 data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-c87151e elementor-widget elementor-widget-text-editor" data-id=c87151e data-element_type=widget data-widget_type=text-editor.default>
              <div class=elementor-widget-container>
                <div class="elementor-text-editor elementor-clearfix">
                  <h3>As we mentioned before, this  event is exclusively for those who passed our strict screening process.
                    You are eligible to join this program, if you meet these requirements:
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-5c40759 elementor-section-content-middle elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=5c40759 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-267c958" data-id=267c958 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-b1f4659 elementor-widget elementor-widget-image" data-id=b1f4659 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=258 height=446 src=https://modelunitednation.org/wp-content/uploads/2019/02/Artboard-2.png class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/Artboard-2.png 258w, https://modelunitednation.org/wp-content/uploads/2019/02/Artboard-2-174x300.png 174w" sizes="(max-width: 258px) 100vw, 258px">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-4ccd19b" data-id=4ccd19b data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-885ad0e elementor-widget elementor-widget-text-editor" data-id=885ad0e data-element_type=widget data-widget_type=text-editor.default>
              <div class=elementor-widget-container>
                <div class="elementor-text-editor elementor-clearfix">
                  <h3>OR
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-e46f8c4" data-id=e46f8c4 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-67474c1 elementor-widget elementor-widget-image" data-id=67474c1 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=257 height=447 src=https://modelunitednation.org/wp-content/uploads/2019/02/Artboard-4.png class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/Artboard-4.png 257w, https://modelunitednation.org/wp-content/uploads/2019/02/Artboard-4-172x300.png 172w" sizes="(max-width: 257px) 100vw, 257px">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-0aa29f1" data-id=0aa29f1 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-f8b2332 elementor-widget elementor-widget-text-editor" data-id=f8b2332 data-element_type=widget data-widget_type=text-editor.default>
              <div class=elementor-widget-container>
                <div class="elementor-text-editor elementor-clearfix">
                  <h3>OR
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-b3bdd88" data-id=b3bdd88 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-342daef elementor-widget elementor-widget-image" data-id=342daef data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=258 height=448 src=https://modelunitednation.org/wp-content/uploads/2019/02/Artboard-3.png class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/Artboard-3.png 258w, https://modelunitednation.org/wp-content/uploads/2019/02/Artboard-3-173x300.png 173w" sizes="(max-width: 258px) 100vw, 258px">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-9be3755 elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=9be3755 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1fc6565" data-id=1fc6565 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-ef42aed elementor-widget elementor-widget-spacer" data-id=ef42aed data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
<script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">jQuery(document).ready(function () {
                        jQuery(".elementor-element-570eb80").prepend('<div class=eae-section-bs><div class=eae-section-bs-inner></div></div>');
                        var bgimage = '';
                        if ('yes' == 'yes') {
                            //if(bgimage == ''){
                            //    var bgoverlay = '';
                            //}else{
                            var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder//assets/lib/vegas/overlays/00.png';
                            // }
                        } else {
                            if ('') {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/.png';
                            } else {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/00.png';
                            }
                        }
                        jQuery(".elementor-element-570eb80").children('.eae-section-bs').children('.eae-section-bs-inner').vegas({
                            slides: [{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto5.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN2.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN3.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN4.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/Diplomatic-Dinner.jpg"}],
                            transition: 'fade2',
                            animation: 'kenburnsLeft',
                            overlay: bgoverlay,
                            cover: true,
                            delay: 4000,
                            timer: false                });
                        if ('yes' == 'yes') {
                            jQuery(".elementor-element-570eb80").children('.eae-section-bs').children('.eae-section-bs-inner').children('.vegas-overlay').css('background-image', '');
                        }
                    });</script> 
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-570eb80 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=570eb80 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;pyramids&quot;}>
  <div class=elementor-background-overlay>
  </div>
  <div class="elementor-shape elementor-shape-top" data-negative=false>
    <svg xmlns=http://www.w3.org/2000/svg viewBox="0 0 1000 100" preserveAspectRatio=none>
      <path class=elementor-shape-fill d=M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9 />
    </svg>
  </div>
  <div class="elementor-container elementor-column-gap-default">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5fe5d6c" data-id=5fe5d6c data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-c164393 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=c164393 data-element_type=section>
              <div class="elementor-container elementor-column-gap-default">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ac42586" data-id=ac42586 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-5a8c64a elementor-widget elementor-widget-heading" data-id=5a8c64a data-element_type=widget data-widget_type=heading.default>
                          <div class=elementor-widget-container>
                            <h2 class="elementor-heading-title elementor-size-default">Let YOUR Voice be Heard
                            </h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div class="elementor-element elementor-element-1c39923 elementor-widget elementor-widget-spacer" data-id=1c39923 data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-1e7359d0 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=1e7359d0 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-wider">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-828bf19" data-id=828bf19 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-8bcd770 elementor-widget elementor-widget-premium-addon-dual-header" data-id=8bcd770 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>SERIES OF ACTIVITIES 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-6ca3844 elementor-widget elementor-widget-divider" data-id=6ca3844 data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-22123349 elementor-widget elementor-widget-spacer" data-id=22123349 data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-34ae956f elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=34ae956f data-element_type=section>
              <div class="elementor-container elementor-column-gap-wide">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4cc21d45" data-id=4cc21d45 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-68c8887 elementor-widget elementor-widget-image" data-id=68c8887 data-element_type=widget data-widget_type=image.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-image>
                              <img width=751 height=496 src=https://modelunitednation.org/wp-content/uploads/2017/07/20181128_042422.jpg class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2017/07/20181128_042422.jpg 751w, https://modelunitednation.org/wp-content/uploads/2017/07/20181128_042422-510x337.jpg 510w, https://modelunitednation.org/wp-content/uploads/2017/07/20181128_042422-300x198.jpg 300w" sizes="(max-width: 751px) 100vw, 751px">
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-59c6936b elementor-widget elementor-widget-heading" data-id=59c6936b data-element_type=widget data-widget_type=heading.default>
                          <div class=elementor-widget-container>
                            <h6 class="elementor-heading-title elementor-size-default">Opening Ceremony
                            </h6>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-1831f3b8 elementor-widget elementor-widget-text-editor" data-id=1831f3b8 data-element_type=widget data-widget_type=text-editor.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-text-editor elementor-clearfix">
                              <p>There will be an amazing opening ceremony from AYIMUN 2019. Delegates will be warmly welcomed in Asia Youth International Model United Nations.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="has_eae_slider elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-6a119fd" data-id=6a119fd data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-7e1e655 elementor-widget elementor-widget-image" data-id=7e1e655 data-element_type=widget data-widget_type=image.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-image>
                              <img width=512 height=346 src=https://modelunitednation.org/wp-content/uploads/2019/02/grand-symposium.jpg class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/grand-symposium.jpg 512w, https://modelunitednation.org/wp-content/uploads/2019/02/grand-symposium-300x203.jpg 300w, https://modelunitednation.org/wp-content/uploads/2019/02/grand-symposium-510x345.jpg 510w" sizes="(max-width: 512px) 100vw, 512px">
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-e493464 elementor-widget elementor-widget-heading" data-id=e493464 data-element_type=widget data-widget_type=heading.default>
                          <div class=elementor-widget-container>
                            <h6 class="elementor-heading-title elementor-size-default">Grand Symposium
                            </h6>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-ee712a6 elementor-widget elementor-widget-text-editor" data-id=ee712a6 data-element_type=widget data-widget_type=text-editor.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-text-editor elementor-clearfix">
                              <p>AYIMUN 2019 provides grand symposium with the theme &#8220;Human Security Agenda in The Globalized World&#8221; which will be conducted by outstanding speakers that will discuss recent international issues regarding the topic in each council.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="has_eae_slider elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-675878a" data-id=675878a data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-b2c455f elementor-widget elementor-widget-image" data-id=b2c455f data-element_type=widget data-widget_type=image.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-image>
                              <img width=1102 height=716 src=https://modelunitednation.org/wp-content/uploads/2019/02/Diplomatic-Dinner.jpg class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/Diplomatic-Dinner.jpg 1102w, https://modelunitednation.org/wp-content/uploads/2019/02/Diplomatic-Dinner-300x195.jpg 300w, https://modelunitednation.org/wp-content/uploads/2019/02/Diplomatic-Dinner-768x499.jpg 768w, https://modelunitednation.org/wp-content/uploads/2019/02/Diplomatic-Dinner-1024x665.jpg 1024w, https://modelunitednation.org/wp-content/uploads/2019/02/Diplomatic-Dinner-600x390.jpg 600w, https://modelunitednation.org/wp-content/uploads/2019/02/Diplomatic-Dinner-510x331.jpg 510w" sizes="(max-width: 1102px) 100vw, 1102px">
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-bbb50ad elementor-widget elementor-widget-heading" data-id=bbb50ad data-element_type=widget data-widget_type=heading.default>
                          <div class=elementor-widget-container>
                            <h6 class="elementor-heading-title elementor-size-default">Diplomatic Dinner
                            </h6>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-4967f9b elementor-widget elementor-widget-text-editor" data-id=4967f9b data-element_type=widget data-widget_type=text-editor.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-text-editor elementor-clearfix">
                              <p>AYIMUN 2019 provides MUN 101 and Zero Conference before the committee sessions start, in order for the delegates to learn how MUN works and to understand the Rules of Procedure deeper. This MUN 101 and Zero Conference will be assisted by our Board of Dais. They will explain and teach the delegates step-by-step procedure of the MUN and they will be discussing fun topics in the Zero Conference.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-2f5bce4 elementor-widget elementor-widget-text-editor" data-id=2f5bce4 data-element_type=widget data-widget_type=text-editor.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-text-editor elementor-clearfix">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div class="elementor-element elementor-element-eb92f3d elementor-hidden-phone elementor-widget elementor-widget-spacer" data-id=eb92f3d data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-64ac19d elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=64ac19d data-element_type=section>
              <div class="elementor-container elementor-column-gap-wider">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-c660ef3" data-id=c660ef3 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                    <div class=elementor-column-wrap>
                      <div class=elementor-widget-wrap>
                      </div>
                    </div>
                  </div>
                  <div class="has_eae_slider elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-88a7701" data-id=88a7701 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-fbe33c0 elementor-widget elementor-widget-image" data-id=fbe33c0 data-element_type=widget data-widget_type=image.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-image>
                              <img width=512 height=346 src=https://modelunitednation.org/wp-content/uploads/2019/02/Meeting-session.jpg class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/Meeting-session.jpg 512w, https://modelunitednation.org/wp-content/uploads/2019/02/Meeting-session-300x203.jpg 300w, https://modelunitednation.org/wp-content/uploads/2019/02/Meeting-session-510x345.jpg 510w" sizes="(max-width: 512px) 100vw, 512px">
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-ce7d40a elementor-widget elementor-widget-heading" data-id=ce7d40a data-element_type=widget data-widget_type=heading.default>
                          <div class=elementor-widget-container>
                            <h6 class="elementor-heading-title elementor-size-default">Meeting Session
                            </h6>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-784cd54 elementor-widget elementor-widget-text-editor" data-id=784cd54 data-element_type=widget data-widget_type=text-editor.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-text-editor elementor-clearfix">
                              <p>Delegates will be assigned as representatives of a country on each council that have chosen by delegates to discuss the particular topic in their council. Delegates will be moderated by Board of Directors during the committee session.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="has_eae_slider elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-8fc6bd0" data-id=8fc6bd0 data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-4dbae7d elementor-widget elementor-widget-image" data-id=4dbae7d data-element_type=widget data-widget_type=image.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-image>
                              <img width=512 height=346 src=https://modelunitednation.org/wp-content/uploads/2019/02/Cultural-Performance.jpg class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/Cultural-Performance.jpg 512w, https://modelunitednation.org/wp-content/uploads/2019/02/Cultural-Performance-300x203.jpg 300w, https://modelunitednation.org/wp-content/uploads/2019/02/Cultural-Performance-510x345.jpg 510w" sizes="(max-width: 512px) 100vw, 512px">
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-fd396b8 elementor-widget elementor-widget-heading" data-id=fd396b8 data-element_type=widget data-widget_type=heading.default>
                          <div class=elementor-widget-container>
                            <h6 class="elementor-heading-title elementor-size-default">Cultural Night
                            </h6>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-a7644bd elementor-widget elementor-widget-text-editor" data-id=a7644bd data-element_type=widget data-widget_type=text-editor.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-text-editor elementor-clearfix">
                              <p>AYIMUN 2019 will provide cultural party night as a closing ceremony that will be shown by delegates from their own country of origin.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="has_eae_slider elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-a26a82a" data-id=a26a82a data-element_type=column data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
                    <div class=elementor-column-wrap>
                      <div class=elementor-widget-wrap>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> 
<script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">jQuery(document).ready(function () {
                        jQuery(".elementor-element-7bb15ab").prepend('<div class=eae-section-bs><div class=eae-section-bs-inner></div></div>');
                        var bgimage = '';
                        if ('yes' == 'yes') {
                            //if(bgimage == ''){
                            //    var bgoverlay = '';
                            //}else{
                            var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder//assets/lib/vegas/overlays/00.png';
                            // }
                        } else {
                            if ('') {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/.png';
                            } else {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/00.png';
                            }
                        }
                        jQuery(".elementor-element-7bb15ab").children('.eae-section-bs').children('.eae-section-bs-inner').vegas({
                            slides: [{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto5.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN2.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN3.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN4.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/Diplomatic-Dinner.jpg"}],
                            transition: 'fade2',
                            animation: 'kenburnsLeft',
                            overlay: bgoverlay,
                            cover: true,
                            delay: 4000,
                            timer: false                });
                        if ('yes' == 'yes') {
                            jQuery(".elementor-element-7bb15ab").children('.eae-section-bs').children('.eae-section-bs-inner').children('.vegas-overlay').css('background-image', '');
                        }
                    });</script> 
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-7bb15ab elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=7bb15ab data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;pyramids&quot;}>
  <div class=elementor-background-overlay>
  </div>
  <div class="elementor-shape elementor-shape-top" data-negative=false>
    <svg xmlns=http://www.w3.org/2000/svg viewBox="0 0 1000 100" preserveAspectRatio=none>
      <path class=elementor-shape-fill d=M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9 />
    </svg>
  </div>
  <div class="elementor-container elementor-column-gap-default">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e52a1dd" data-id=e52a1dd data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-e692654 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=e692654 data-element_type=section>
              <div class="elementor-container elementor-column-gap-default">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-aaaf755" data-id=aaaf755 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-f552857 elementor-widget elementor-widget-heading" data-id=f552857 data-element_type=widget data-widget_type=heading.default>
                          <div class=elementor-widget-container>
                            <h2 class="elementor-heading-title elementor-size-default">Let's Make a Better World
                            </h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-c1a6d7f elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=c1a6d7f data-element_type=section>
              <div class="elementor-container elementor-column-gap-default">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-4534105" data-id=4534105 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-f09024e elementor-align-center elementor-mobile-align-center elementor-widget elementor-widget-button" data-id=f09024e data-element_type=widget data-widget_type=button.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-button-wrapper>
                              <a href=https://modelunitednation.org/waiting-list/ class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                <span class=elementor-button-content-wrapper>
                                  <span class=elementor-button-text>I Want To Be a Delegate
                                  </span>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-67cadba elementor-align-right elementor-mobile-align-right elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button" data-id=67cadba data-element_type=widget data-widget_type=button.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-button-wrapper>
                              <a href=# class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                <span class=elementor-button-content-wrapper>
                                  <span class=elementor-button-text>Registration has CLOSED
                                  </span>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="has_eae_slider elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-2b6dc57" data-id=2b6dc57 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-dc48e52 elementor-align-center elementor-mobile-align-center elementor-widget elementor-widget-button" data-id=dc48e52 data-element_type=widget data-widget_type=button.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-button-wrapper>
                              <a href=https://modelunitednation.org/result class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                <span class=elementor-button-content-wrapper>
                                  <span class=elementor-button-text>Check Your Result
                                  </span>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div class="elementor-element elementor-element-964ce30 elementor-widget elementor-widget-spacer" data-id=964ce30 data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-4793e3e elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=4793e3e data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-ed4ed8f" data-id=ed4ed8f data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-f1ed80b elementor-widget elementor-widget-spacer" data-id=f1ed80b data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-ee5c78c elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=ee5c78c data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1845933" data-id=1845933 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-af5b62b elementor-widget elementor-widget-premium-addon-dual-header" data-id=af5b62b data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>COUNCIL PREFERENCES 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-3a65b51 elementor-widget elementor-widget-divider" data-id=3a65b51 data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-68e0e16 elementor-section-content-middle elementor-section-full_width elementor-hidden-phone elementor-section-height-default elementor-section-height-default" data-id=68e0e16 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-075e582" data-id=075e582 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-4fe9fec elementor-widget elementor-widget-image" data-id=4fe9fec data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNSC.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNSC.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNSC-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-c60f9eb elementor-widget elementor-widget-heading" data-id=c60f9eb data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNSC
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-964e35b" data-id=964e35b data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-ce4422b elementor-widget elementor-widget-image" data-id=ce4422b data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNHCR.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNHCR.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNHCR-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-f5485c4 elementor-widget elementor-widget-heading" data-id=f5485c4 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNHCR
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-3f6fa24" data-id=3f6fa24 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-59ce316 elementor-widget elementor-widget-image" data-id=59ce316 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNEP.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNEP.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNEP-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-401afe3 elementor-widget elementor-widget-heading" data-id=401afe3 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNEP
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-985b5f0" data-id=985b5f0 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-8d2cba7 elementor-widget elementor-widget-image" data-id=8d2cba7 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNDP.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNDP.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNDP-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-b729b08 elementor-widget elementor-widget-heading" data-id=b729b08 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNDP
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-a4e3990" data-id=a4e3990 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-7ac49ec elementor-widget elementor-widget-image" data-id=7ac49ec data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/WHO.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/WHO.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/WHO-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-ce6df09 elementor-widget elementor-widget-heading" data-id=ce6df09 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">WHO
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-a200314" data-id=a200314 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-1393409 elementor-widget elementor-widget-image" data-id=1393409 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/FAO.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/FAO.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/FAO-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-ce220e7 elementor-widget elementor-widget-heading" data-id=ce220e7 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">FAO
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-2f297ce" data-id=2f297ce data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-e8093e0 elementor-widget elementor-widget-image" data-id=e8093e0 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/IOM.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/IOM.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/IOM-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-8b1d43e elementor-widget elementor-widget-heading" data-id=8b1d43e data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">IOM
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-b5fa74d elementor-section-full_width elementor-hidden-phone elementor-section-height-default elementor-section-height-default" data-id=b5fa74d data-element_type=section>
  <div class="elementor-container elementor-column-gap-narrow">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-451f575" data-id=451f575 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-cb98e43 elementor-widget elementor-widget-image" data-id=cb98e43 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNICEF.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNICEF.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNICEF-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-c8a4d1f elementor-widget elementor-widget-heading" data-id=c8a4d1f data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNICEF
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-7fda62f" data-id=7fda62f data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-0b76f0b elementor-widget elementor-widget-image" data-id=0b76f0b data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNISDR.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNISDR.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNISDR-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-4296328 elementor-widget elementor-widget-heading" data-id=4296328 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNISDR
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-59a602b" data-id=59a602b data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-c556cfd elementor-widget elementor-widget-image" data-id=c556cfd data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNWOMEN.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNWOMEN.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNWOMEN-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-31bfcf5 elementor-widget elementor-widget-heading" data-id=31bfcf5 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UN WOMEN
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-eb1bf9a" data-id=eb1bf9a data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-a148175 elementor-widget elementor-widget-image" data-id=a148175 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/INTERPOL.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/INTERPOL.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/INTERPOL-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-8107cf6 elementor-widget elementor-widget-heading" data-id=8107cf6 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">INTERPOL
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-325d8f4" data-id=325d8f4 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-06e06a2 elementor-widget elementor-widget-image" data-id=06e06a2 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/ASEAN-EU.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/ASEAN-EU.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/ASEAN-EU-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-0ea8e78 elementor-widget elementor-widget-heading" data-id=0ea8e78 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">ASEAN - EUROPEAN UNION
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-3336c3e elementor-section-full_width elementor-hidden-phone elementor-section-height-default elementor-section-height-default" data-id=3336c3e data-element_type=section>
  <div class="elementor-container elementor-column-gap-narrow">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-c4b67e0" data-id=c4b67e0 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-2c32dbb elementor-widget elementor-widget-image" data-id=2c32dbb data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/AU.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/AU.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/AU-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-877b4ad elementor-widget elementor-widget-heading" data-id=877b4ad data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">AFRICA UNION
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-8692905" data-id=8692905 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-c30b938 elementor-widget elementor-widget-image" data-id=c30b938 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNCTAD.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNCTAD.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNCTAD-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-19b646b elementor-widget elementor-widget-heading" data-id=19b646b data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNCTAD
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-018f47b" data-id=018f47b data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-9565391 elementor-widget elementor-widget-image" data-id=9565391 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNTFHS.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNTFHS.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNTFHS-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-054e272 elementor-widget elementor-widget-heading" data-id=054e272 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNTFHS
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-be59fe5" data-id=be59fe5 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-92afb45 elementor-widget elementor-widget-image" data-id=92afb45 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=121 height=121 src=https://modelunitednation.org/wp-content/uploads/2019/02/WTO.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/WTO.png 121w, https://modelunitednation.org/wp-content/uploads/2019/02/WTO-100x100.png 100w" sizes="(max-width: 121px) 100vw, 121px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-0e4350b elementor-widget elementor-widget-heading" data-id=0e4350b data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">WTO
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-dedd428" data-id=dedd428 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-13f4878 elementor-widget elementor-widget-image" data-id=13f4878 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/OIC.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/OIC.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/OIC-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-c091421 elementor-widget elementor-widget-heading" data-id=c091421 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">OIC
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-6384faa elementor-section-content-middle elementor-section-full_width elementor-hidden-desktop elementor-section-height-default elementor-section-height-default" data-id=6384faa data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-c49e8ff" data-id=c49e8ff data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-d04180e elementor-widget elementor-widget-image" data-id=d04180e data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNSC.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNSC.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNSC-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-9539ef0 elementor-widget elementor-widget-heading" data-id=9539ef0 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNSC
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-af23a1d" data-id=af23a1d data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-9cdf1d4 elementor-widget elementor-widget-image" data-id=9cdf1d4 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNHCR.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNHCR.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNHCR-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-425674e elementor-widget elementor-widget-heading" data-id=425674e data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNHCR
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-3b6d69c" data-id=3b6d69c data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-0bb5dce elementor-widget elementor-widget-image" data-id=0bb5dce data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNEP.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNEP.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNEP-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-648554a elementor-widget elementor-widget-heading" data-id=648554a data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNEP
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-03cf22d" data-id=03cf22d data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-4cdb705 elementor-widget elementor-widget-image" data-id=4cdb705 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNDP.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNDP.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNDP-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-70e6367 elementor-widget elementor-widget-heading" data-id=70e6367 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNDP
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-7145229" data-id=7145229 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-c9c5439 elementor-widget elementor-widget-image" data-id=c9c5439 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/WHO.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/WHO.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/WHO-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-b383e61 elementor-widget elementor-widget-heading" data-id=b383e61 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">WHO
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-184a4ac" data-id=184a4ac data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-1066757 elementor-widget elementor-widget-image" data-id=1066757 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/FAO.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/FAO.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/FAO-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-a2ce035 elementor-widget elementor-widget-heading" data-id=a2ce035 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">FAO
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-b697cc0" data-id=b697cc0 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-345e304 elementor-widget elementor-widget-image" data-id=345e304 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/IOM.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/IOM.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/IOM-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-046a250 elementor-widget elementor-widget-heading" data-id=046a250 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">IOM
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-b4864ae" data-id=b4864ae data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-fbade79 elementor-widget elementor-widget-image" data-id=fbade79 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNICEF.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNICEF.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNICEF-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-8da0b38 elementor-widget elementor-widget-heading" data-id=8da0b38 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNICEF
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-5462352" data-id=5462352 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-333ad0e elementor-widget elementor-widget-image" data-id=333ad0e data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNISDR.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNISDR.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNISDR-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-43c8604 elementor-widget elementor-widget-heading" data-id=43c8604 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNISDR
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-10 elementor-top-column elementor-element elementor-element-14c2914" data-id=14c2914 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-7c6eb3f elementor-widget elementor-widget-image" data-id=7c6eb3f data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNWOMEN.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNWOMEN.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNWOMEN-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-6cd31e0 elementor-widget elementor-widget-heading" data-id=6cd31e0 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UN WOMEN
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-004502b elementor-section-content-middle elementor-section-full_width elementor-hidden-desktop elementor-section-height-default elementor-section-height-default" data-id=004502b data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-0fcde51" data-id=0fcde51 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-1d58034 elementor-widget elementor-widget-image" data-id=1d58034 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/INTERPOL.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/INTERPOL.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/INTERPOL-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-4bb416b elementor-widget elementor-widget-heading" data-id=4bb416b data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">INTERPOL
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-2d8e7fb" data-id=2d8e7fb data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-742b242 elementor-widget elementor-widget-image" data-id=742b242 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/ASEAN-EU.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/ASEAN-EU.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/ASEAN-EU-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-dc9a9d0 elementor-widget elementor-widget-heading" data-id=dc9a9d0 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">ASEAN - EUROPEAN UNION
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-d268fe5" data-id=d268fe5 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-c783cec elementor-widget elementor-widget-image" data-id=c783cec data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/AU.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/AU.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/AU-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-b0bcaf4 elementor-widget elementor-widget-heading" data-id=b0bcaf4 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">AFRICA UNION
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-8b370f4" data-id=8b370f4 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-5740765 elementor-widget elementor-widget-image" data-id=5740765 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNCTAD.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNCTAD.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNCTAD-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-8dee08a elementor-widget elementor-widget-heading" data-id=8dee08a data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNCTAD
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-f029980" data-id=f029980 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-bff83ab elementor-widget elementor-widget-image" data-id=bff83ab data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/UNTFHS.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/UNTFHS.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/UNTFHS-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-33074cf elementor-widget elementor-widget-heading" data-id=33074cf data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">UNTFHS
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-b510115" data-id=b510115 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-78d773a elementor-widget elementor-widget-image" data-id=78d773a data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=121 height=121 src=https://modelunitednation.org/wp-content/uploads/2019/02/WTO.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/WTO.png 121w, https://modelunitednation.org/wp-content/uploads/2019/02/WTO-100x100.png 100w" sizes="(max-width: 121px) 100vw, 121px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-7a48bd6 elementor-widget elementor-widget-heading" data-id=7a48bd6 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">WTO
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-6096602" data-id=6096602 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-5257cae elementor-widget elementor-widget-image" data-id=5257cae data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <img width=120 height=120 src=https://modelunitednation.org/wp-content/uploads/2019/02/OIC.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/OIC.png 120w, https://modelunitednation.org/wp-content/uploads/2019/02/OIC-100x100.png 100w" sizes="(max-width: 120px) 100vw, 120px">
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-51288cc elementor-widget elementor-widget-heading" data-id=51288cc data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">OIC
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-e01d6e7 elementor-section-content-middle elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=e01d6e7 data-element_type=section id=rundown data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-d270715" data-id=d270715 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-0a5f6a7 elementor-widget elementor-widget-premium-addon-dual-header" data-id=0a5f6a7 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>Rundown AYIMUN 2019 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-d9cad60 elementor-widget elementor-widget-divider" data-id=d9cad60 data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-bb0f011 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=bb0f011 data-element_type=section>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-ed55d21" data-id=ed55d21 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-ab7d43f elementor-widget elementor-widget-eael-adv-tabs" data-id=ab7d43f data-element_type=widget data-widget_type=eael-adv-tabs.default>
              <div class=elementor-widget-container>
                <div id=eael-advance-tabs-ab7d43f class="eael-advance-tabs eael-tabs-horizontal active-caret-on" data-tabid=ab7d43f>
                  <div class=eael-tabs-nav>
                    <ul class=eael-tab-inline-icon>
                      <li class=active-default> 
                        <span class=eael-tab-title>DAY 1
                        </span>
                      </li>
                      <li class> 
                        <span class=eael-tab-title>DAY 2
                        </span>
                      </li>
                      <li class> 
                        <span class=eael-tab-title>DAY 3
                        </span>
                      </li>
                      <li class> 
                        <span class=eael-tab-title>DAY 4
                        </span>
                      </li>
                    </ul>
                  </div>
                  <div class=eael-tabs-content>
                    <div class="clearfix active-default">
                      <div style="overflow-x: auto;">
                        <table border=1 width=100% cellspacing=0 cellpadding="2% 0% 2% 0%">
                          <tbody>
                            <tr>
                              <td width=35%>
                                <h3 style="text-align: center;">
                                  <strong>Venue
                                  </strong>
                                </h3>
                              </td>
                              <td width=35%>
                                <h3 style="text-align: center;position: middle">
                                  <strong>Time
                                  </strong>
                                </h3>
                              </td>
                              <td width=65%>
                                <h3 style="text-align: center;">
                                  <strong>Agenda
                                  </strong>
                                </h3>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">KLIA & KLIA 2
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">10:00 am - 03:00 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">AIRPORT ASSISTANCE**
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">Allocated Hotel
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">10:00 am - 04:00 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Registration for FULL-ACCOMMODATION Delegates
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">Putrajaya International Convention Centre (PICC)
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">03:00 pm - 07:00 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Registration for NON-ACCOMMODATION Delegates
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">Allocated Hotel
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">03:00 pm - 09:00 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Check-in Hotel***
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Perdana Hall
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">06:30 pm - 07:00 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Opening Ceremony
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Perdana Hall
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;"> 07.00 pm - 07.45 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Dinner
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Perdana Hall
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">08.27 pm - 09.47 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Opening Grand Symposium
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Perdana Hall
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">09.57 pm - 10.32 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">MUN 101
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Perdana Hall
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">10.32 pm - 11.02 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Closing
                                </p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <p>
                          <strong>*THE COMMITTEE DID NOT PROVIDE LUNCH DURING REGISTRATION ON 25
                            <sup>th
                            </sup> AUGUST
                          </strong>
                        </p>
                        <p>
                          <strong>**AIRPOT ASSISTANCE FOR FULL-ACCOMMODATION DELEGATES ONLY
                          </strong>
                        </p>
                        <p>
                          <strong>***CHECK-IN HOTEL FOR FULL-ACCOMMODATION DELEGATES ONLY
                          </strong>
                        </p>
                      </div>
                    </div>
                    <div class="clearfix ">
                      <div style="overflow-x: auto;">
                        <table border=1 width=100% cellspacing=0 cellpadding="2% 0% 2% 0%">
                          <tbody>
                            <tr>
                              <td width=35%>
                                <h3 style="text-align: center;">
                                  <strong>Venue
                                  </strong>
                                </h3>
                              </td>
                              <td width=35%>
                                <h3 style="text-align: center;">
                                  <strong>Time
                                  </strong>
                                </h3>
                              </td>
                              <td width=65%>
                                <h3 style="text-align: center;">
                                  <strong>Agenda
                                  </strong>
                                </h3>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">Putrajaya International Convention Centre (PICC)
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">08:45 am - 09:00 am
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Registration
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Room Allocated
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;"> 09.00 am - 09.30 am
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">MUN Q&amp;A
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Room Allocated
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">09.30 am - 11.30 am
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Committee Session 1
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Room Allocated
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">11.30 am - 11.45 am
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Coffe Break
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Room Allocated
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">11.45 am - 01.00 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Committee Session 2
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Room Allocated
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">01.15 pm - 01.50 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Lunch &amp; Break
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Room Allocated
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">02.00 pm - 03.00 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Committee Session 3
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Room Allocated
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">03.00 pm - 03.30 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Coffe Break
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td width=35%>
                                <p style="text-align: center;">PICC Room Allocated
                                </p>
                              </td>
                              <td width=35%>
                                <p style="text-align: center;">03.00 pm - 05.00 pm
                                </p>
                              </td>
                              <td width=65%>
                                <p style="text-align: center;">Committee Session 4
                                </p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <P>
                        </P>
                        <p> 
                          <STRONG>*IF YOU ARE LATE TO THE MEETING SESSION, YOU WILL MISS THE ROLL CALL AND RIGHT TO VOTE
                          </STRONG>
                        </p>
                      </div>
                    </div>
                    <div class="clearfix ">
                      <div style="overflow-x: auto;">
                        <table border=1 width=100% cellspacing=0 cellpadding="2% 0% 2% 0%">
                          <tbody>
                            <tr>
                              <td width=35%>
                                <h3 style="text-align: center;">
                                  <strong>Venue
                                  </strong>
                                </h3>
                              </td>
                              <td width=35%>
                                <h3 style="text-align: center;">
                                  <strong>Time
                                  </strong>
                                </h3>
                              </td>
                              <td width=65%>
                                <h3 style="text-align: center;">
                                  <strong>Agenda
                                  </strong>
                                </h3>
                              </td>
                            </tr>
                            <td width=35%>
                              <p style="text-align: center;">PICC Room Allocated
                              </p>
                            </td>
                            <td width=35%>
                              <p style="text-align: center;"> 10.00 am - 10.30 am
                              </p>
                            </td>
                            <td width=65%>
                              <p style="text-align: center;">Committee Session 5
                              </p>
                            </td>
                          </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">PICC Room Allocated
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">10.30 am - 11.00 am
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Coffe Break
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">PICC Room Allocated
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">11.00 am - 01.20 pm
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Committee Session 6
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">PICC Room Allocated
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">01.35 pm - 02.10 pm
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Lunch & Back to Hotel
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">PICC Perdana Hall
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">06.30 pm - 07.00 pm
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Registration
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">PICC Perdana Hall
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">07.00 pm - 09.00 pm
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Closing Ceremony
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">PICC Perdana Hall
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">08.00 pm - 08.45 pm
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Dinner
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">PICC Perdana Hall
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">07.00 pm - 11.15 pm
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Cultural Performance, National Parade & Awarding Session
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">PICC Perdana Hall
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">11.15 pm - 11.30 pm
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Closing
                            </p>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    <P>
                    </P>
                    <p> 
                      <STRONG>*IF YOU ARE LATE TO THE MEETING SESSION, YOU WILL MISS THE ROLL CALL AND RIGHT TO VOTE
                      </STRONG>
                    </p>
                  </div>
                </div>
                <div class="clearfix ">
                  <div style="overflow-x: auto;">
                    <table border=1 width=100% cellspacing=0 cellpadding="2% 0% 2% 0%">
                      <tbody>
                        <tr>
                          <td width=35%>
                            <h3 style="text-align: center;">
                              <strong>Venue
                              </strong>
                            </h3>
                          </td>
                          <td width=35%>
                            <h3 style="text-align: center;position: middle">
                              <strong>Time
                              </strong>
                            </h3>
                          </td>
                          <td width=65%>
                            <h3 style="text-align: center;">
                              <strong>Agenda
                              </strong>
                            </h3>
                          </td>
                        </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">Hotel Allocated
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">08:00 am - 11:30 am
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Check out*
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td width=35%>
                            <p style="text-align: center;">KLIA & KLIA 2
                            </p>
                          </td>
                          <td width=35%>
                            <p style="text-align: center;">09:00 am - 02:00 pm
                            </p>
                          </td>
                          <td width=65%>
                            <p style="text-align: center;">Airport Assistance**
                            </p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <p>
                      <strong>*CHECKOUT FOR FULL-ACCOMMODATION DELEGATES ONLY
                      </strong>
                    </p>
                    <p>
                      <strong>**AIRPOT ASSISTANCE FOR FULL-ACCOMMODATION DELEGATES ONLY
                      </strong>
                    </p>
                    <p>
                      <strong>**SPECIFIC TIME WILL BE INFORMED
                      </strong>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
</section>
<section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-b9333ea elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=b9333ea data-element_type=section>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-533dca1" data-id=533dca1 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-b9c71aa elementor-widget elementor-widget-premium-addon-dual-header" data-id=b9c71aa data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>Delegates Update 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-41bc815 elementor-widget elementor-widget-divider" data-id=41bc815 data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-4db5bf6 elementor-widget elementor-widget-jet-circle-progress" data-id=4db5bf6 data-element_type=widget data-widget_type=jet-circle-progress.default>
              <div class=elementor-widget-container>
                <div class="elementor-jet-circle-progress jet-elements">
                  <div class=circle-progress-wrap data-duration=1500>
                    <div class=circle-progress-bar>
                      <svg class=circle-progress width=200 height=200 viewBox="0 0 200 200" data-radius=50 data-circumference=314.15926535898 data-responsive-sizes="{&quot;desktop&quot;:{&quot;size&quot;:200,&quot;viewBox&quot;:&quot;0 0 200 200&quot;,&quot;center&quot;:100,&quot;radius&quot;:50,&quot;valStroke&quot;:100,&quot;bgStroke&quot;:100,&quot;circumference&quot;:314.1592653589793},&quot;tablet&quot;:{&quot;size&quot;:200,&quot;viewBox&quot;:&quot;0 0 200 200&quot;,&quot;center&quot;:100,&quot;radius&quot;:50,&quot;valStroke&quot;:100,&quot;bgStroke&quot;:100,&quot;circumference&quot;:314.1592653589793},&quot;mobile&quot;:{&quot;size&quot;:200,&quot;viewBox&quot;:&quot;0 0 200 200&quot;,&quot;center&quot;:100,&quot;radius&quot;:50,&quot;valStroke&quot;:100,&quot;bgStroke&quot;:100,&quot;circumference&quot;:314.1592653589793}}">
                        <linearGradient id="circle-progress-meter-gradient-4db5bf6" gradientUnits="objectBoundingBox" gradientTransform="rotate(0 0.5 0.5)" x1="-0.25" y1="0.5" x2="1.25" y2="0.5">
                          <stop offset=0% stop-color />
                          <stop offset=100% stop-color />
                        </linearGradient>
                        <linearGradient id="circle-progress-value-gradient-4db5bf6" gradientUnits="objectBoundingBox" gradientTransform="rotate(180 0.5 0.5)" x1="-0.25" y1="0.5" x2="1.25" y2="0.5">
                          <stop offset=0% stop-color=rgba(18,52,94,0.92) />
                          <stop offset=100% stop-color=#001db2 />
                        </linearGradient>
                        <circle class=circle-progress__meter cx=100 cy=100 r=50 stroke=rgba(110,193,228,0.32) stroke-width=100 fill=none />
                        <circle class=circle-progress__value cx=100 cy=100 r=50 stroke=url(#circle-progress-value-gradient-4db5bf6) stroke-width=100 data-value=100 style="stroke-dasharray: 314.15926535898; stroke-dashoffset: 314.15926535898;" fill=none />
                      </svg>
                      <div class=position-in-circle>
                        <div class=circle-counter>
                          <div class=circle-val>
                            <span class=circle-counter__number data-to-value=100>0
                            </span>
                            <span class=circle-counter__suffix>%
                            </span>
                          </div>
                          <div class=circle-counter__content>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-cd9eb9c elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=cd9eb9c data-element_type=section>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-114d330" data-id=114d330 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-21a8abf elementor-widget elementor-widget-counter" data-id=21a8abf data-element_type=widget data-widget_type=counter.default>
              <div class=elementor-widget-container>
                <div class=elementor-counter>
                  <div class=elementor-counter-number-wrapper>
                    <span class=elementor-counter-number-prefix>
                    </span>
                    <span class=elementor-counter-number data-duration=2000 data-to-value=35214 data-from-value data-delimiter=.>
                    </span>
                    <span class=elementor-counter-number-suffix>
                    </span>
                  </div>
                  <div class=elementor-counter-title>Total Registrant
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-db539fd" data-id=db539fd data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-6f4db32 elementor-widget elementor-widget-counter" data-id=6f4db32 data-element_type=widget data-widget_type=counter.default>
              <div class=elementor-widget-container>
                <div class=elementor-counter>
                  <div class=elementor-counter-number-wrapper>
                    <span class=elementor-counter-number-prefix>
                    </span>
                    <span class=elementor-counter-number data-duration=2000 data-to-value=17001 data-from-value data-delimiter=.>
                    </span>
                    <span class=elementor-counter-number-suffix>
                    </span>
                  </div>
                  <div class=elementor-counter-title>Selected Registrants
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-64441fa" data-id=64441fa data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-3b74fc6 elementor-widget elementor-widget-counter" data-id=3b74fc6 data-element_type=widget data-widget_type=counter.default>
              <div class=elementor-widget-container>
                <div class=elementor-counter>
                  <div class=elementor-counter-number-wrapper>
                    <span class=elementor-counter-number-prefix>
                    </span>
                    <span class=elementor-counter-number data-duration=2000 data-to-value=1518 data-from-value=0 data-delimiter=.>0
                    </span>
                    <span class=elementor-counter-number-suffix>
                    </span>
                  </div>
                  <div class=elementor-counter-title>Confirmed Delegates
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-7175467 elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=7175467 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6b177db" data-id=6b177db data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-ccd5a3a elementor-widget elementor-widget-spacer" data-id=ccd5a3a data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-4c973d7 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=4c973d7 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-57d6e7e" data-id=57d6e7e data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-a1295f7 elementor-widget elementor-widget-premium-addon-dual-header" data-id=a1295f7 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>GALLERY OF AYIMUN 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-ed21123 elementor-widget elementor-widget-divider" data-id=ed21123 data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-c83216a elementor-section-content-middle elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=c83216a data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-d41b381" data-id=d41b381 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-9ef502f elementor-widget elementor-widget-jet-images-layout" data-id=9ef502f data-element_type=widget data-widget_type=jet-images-layout.default>
              <div class=elementor-widget-container>
                <div class="elementor-jet-images-layout jet-elements">
                  <div class="jet-images-layout layout-type-masonry" data-settings='{"layoutType":"masonry","columns":3,"columnsTablet":"","columnsMobile":"","justifyHeight":300}'>
                    <div class="jet-images-layout__list column-desktop-3 column-tablet-2 column-mobile-1" data-columns>
                      <div class="jet-images-layout__item ">
                        <div class=jet-images-layout__inner>
                          <div class=jet-images-layout__image-loader>
                            <span>
                            </span>
                          </div>
                          <a class="jet-images-layout__link " href=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-2.jpg data-elementor-open-lightbox=yes data-elementor-lightbox-slideshow=9ef502f>
                            <div class=jet-images-layout__image>
                              <img class=jet-images-layout__image-instance src=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-2.jpg alt="foto gallery 2">
                            </div>
                            <div class=jet-images-layout__content>
                            </div>
                          </a>
                        </div>
                      </div>
                      <div class="jet-images-layout__item ">
                        <div class=jet-images-layout__inner>
                          <div class=jet-images-layout__image-loader>
                            <span>
                            </span>
                          </div>
                          <a class="jet-images-layout__link " href=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-6.jpg data-elementor-open-lightbox=yes data-elementor-lightbox-slideshow=9ef502f>
                            <div class=jet-images-layout__image>
                              <img class=jet-images-layout__image-instance src=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-6.jpg alt="foto gallery 6">
                            </div>
                            <div class=jet-images-layout__content>
                            </div>
                          </a>
                        </div>
                      </div>
                      <div class="jet-images-layout__item ">
                        <div class=jet-images-layout__inner>
                          <div class=jet-images-layout__image-loader>
                            <span>
                            </span>
                          </div>
                          <a class="jet-images-layout__link " href=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-4.jpg data-elementor-open-lightbox=yes data-elementor-lightbox-slideshow=9ef502f>
                            <div class=jet-images-layout__image>
                              <img class=jet-images-layout__image-instance src=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-4.jpg alt="foto gallery 4">
                            </div>
                            <div class=jet-images-layout__content>
                            </div>
                          </a>
                        </div>
                      </div>
                      <div class="jet-images-layout__item ">
                        <div class=jet-images-layout__inner>
                          <div class=jet-images-layout__image-loader>
                            <span>
                            </span>
                          </div>
                          <a class="jet-images-layout__link " href=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-8.jpg data-elementor-open-lightbox=yes data-elementor-lightbox-slideshow=9ef502f>
                            <div class=jet-images-layout__image>
                              <img class=jet-images-layout__image-instance src=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-8.jpg alt="foto gallery 8">
                            </div>
                            <div class=jet-images-layout__content>
                            </div>
                          </a>
                        </div>
                      </div>
                      <div class="jet-images-layout__item ">
                        <div class=jet-images-layout__inner>
                          <div class=jet-images-layout__image-loader>
                            <span>
                            </span>
                          </div>
                          <a class="jet-images-layout__link " href=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-1.jpg data-elementor-open-lightbox=yes data-elementor-lightbox-slideshow=9ef502f>
                            <div class=jet-images-layout__image>
                              <img class=jet-images-layout__image-instance src=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-1.jpg alt="foto gallery 1">
                            </div>
                            <div class=jet-images-layout__content>
                            </div>
                          </a>
                        </div>
                      </div>
                      <div class="jet-images-layout__item ">
                        <div class=jet-images-layout__inner>
                          <div class=jet-images-layout__image-loader>
                            <span>
                            </span>
                          </div>
                          <a class="jet-images-layout__link " href=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-3.jpg data-elementor-open-lightbox=yes data-elementor-lightbox-slideshow=9ef502f>
                            <div class=jet-images-layout__image>
                              <img class=jet-images-layout__image-instance src=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-3.jpg alt="foto gallery 3">
                            </div>
                            <div class=jet-images-layout__content>
                            </div>
                          </a>
                        </div>
                      </div>
                      <div class="jet-images-layout__item ">
                        <div class=jet-images-layout__inner>
                          <div class=jet-images-layout__image-loader>
                            <span>
                            </span>
                          </div>
                          <a class="jet-images-layout__link " href=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-7.jpg data-elementor-open-lightbox=yes data-elementor-lightbox-slideshow=9ef502f>
                            <div class=jet-images-layout__image>
                              <img class=jet-images-layout__image-instance src=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-7.jpg alt="foto gallery 7">
                            </div>
                            <div class=jet-images-layout__content>
                            </div>
                          </a>
                        </div>
                      </div>
                      <div class="jet-images-layout__item ">
                        <div class=jet-images-layout__inner>
                          <div class=jet-images-layout__image-loader>
                            <span>
                            </span>
                          </div>
                          <a class="jet-images-layout__link " href=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-5.jpg data-elementor-open-lightbox=yes data-elementor-lightbox-slideshow=9ef502f>
                            <div class=jet-images-layout__image>
                              <img class=jet-images-layout__image-instance src=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-5.jpg alt="foto gallery 5">
                            </div>
                            <div class=jet-images-layout__content>
                            </div>
                          </a>
                        </div>
                      </div>
                      <div class="jet-images-layout__item ">
                        <div class=jet-images-layout__inner>
                          <div class=jet-images-layout__image-loader>
                            <span>
                            </span>
                          </div>
                          <a class="jet-images-layout__link " href=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-9.jpg data-elementor-open-lightbox=yes data-elementor-lightbox-slideshow=9ef502f>
                            <div class=jet-images-layout__image>
                              <img class=jet-images-layout__image-instance src=https://modelunitednation.org/wp-content/uploads/2019/02/foto-gallery-9.jpg alt="foto gallery 9">
                            </div>
                            <div class=jet-images-layout__content>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-3e4f543 elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=3e4f543 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-d371b08" data-id=d371b08 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-5d50016 elementor-widget elementor-widget-spacer" data-id=5d50016 data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
<script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">jQuery(document).ready(function () {
                        jQuery(".elementor-element-7413b88").prepend('<div class=eae-section-bs><div class=eae-section-bs-inner></div></div>');
                        var bgimage = '';
                        if ('yes' == 'yes') {
                            //if(bgimage == ''){
                            //    var bgoverlay = '';
                            //}else{
                            var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder//assets/lib/vegas/overlays/00.png';
                            // }
                        } else {
                            if ('') {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/.png';
                            } else {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/00.png';
                            }
                        }
                        jQuery(".elementor-element-7413b88").children('.eae-section-bs').children('.eae-section-bs-inner').vegas({
                            slides: [{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto5.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN2.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN3.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN4.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/Diplomatic-Dinner.jpg"}],
                            transition: 'fade2',
                            animation: 'kenburnsLeft',
                            overlay: bgoverlay,
                            cover: true,
                            delay: 4000,
                            timer: false                });
                        if ('yes' == 'yes') {
                            jQuery(".elementor-element-7413b88").children('.eae-section-bs').children('.eae-section-bs-inner').children('.vegas-overlay').css('background-image', '');
                        }
                    });</script> 
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-7413b88 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=7413b88 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;pyramids&quot;}>
  <div class=elementor-background-overlay>
  </div>
  <div class="elementor-shape elementor-shape-top" data-negative=false>
    <svg xmlns=http://www.w3.org/2000/svg viewBox="0 0 1000 100" preserveAspectRatio=none>
      <path class=elementor-shape-fill d=M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9 />
    </svg>
  </div>
  <div class="elementor-container elementor-column-gap-default">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-444fe46" data-id=444fe46 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-417ea93 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=417ea93 data-element_type=section>
              <div class="elementor-container elementor-column-gap-default">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-0644840" data-id=0644840 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-5dd2043 elementor-widget elementor-widget-heading" data-id=5dd2043 data-element_type=widget data-widget_type=heading.default>
                          <div class=elementor-widget-container>
                            <h2 class="elementor-heading-title elementor-size-default">Make an Impact
                            </h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-85659db elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=85659db data-element_type=section>
              <div class="elementor-container elementor-column-gap-default">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0284de7" data-id=0284de7 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-2521bf4 elementor-align-center elementor-mobile-align-center elementor-widget elementor-widget-button" data-id=2521bf4 data-element_type=widget data-widget_type=button.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-button-wrapper>
                              <a href=https://modelunitednation.org/waiting-list/ class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                <span class=elementor-button-content-wrapper>
                                  <span class=elementor-button-text>I Want To Be a Delegate
                                  </span>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-6d68ca9 elementor-align-right elementor-mobile-align-right elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button" data-id=6d68ca9 data-element_type=widget data-widget_type=button.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-button-wrapper>
                              <a href=# class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                <span class=elementor-button-content-wrapper>
                                  <span class=elementor-button-text>Registration has CLOSED
                                  </span>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="has_eae_slider elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-69d0f44" data-id=69d0f44 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-f9869bf elementor-align-center elementor-mobile-align-center elementor-widget elementor-widget-button" data-id=f9869bf data-element_type=widget data-widget_type=button.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-button-wrapper>
                              <a href=https://modelunitednation.org/result class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                                <span class=elementor-button-content-wrapper>
                                  <span class=elementor-button-text>Check Your Result
                                  </span>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div class="elementor-element elementor-element-b72f3f6 elementor-widget elementor-widget-spacer" data-id=b72f3f6 data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-10999cb elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=10999cb data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-068bba8" data-id=068bba8 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-f844f7d elementor-widget elementor-widget-spacer" data-id=f844f7d data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-5284ce3 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=5284ce3 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-cf7cdf3" data-id=cf7cdf3 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-263da37 elementor-widget elementor-widget-premium-addon-dual-header" data-id=263da37 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
              <div class=elementor-widget-container>
                <div class=premium-dual-header-container>
                  <div class=premium-dual-header-first-container>
                    <h2 class="premium-dual-header-first-header ">
                      <span class=premium-dual-header-first-span>ALUMNI TESTIMONIES 
                      </span>
                      <span class="premium-dual-header-second-header ">
                      </span>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-6eda910 elementor-widget elementor-widget-divider" data-id=6eda910 data-element_type=widget data-widget_type=divider.default>
              <div class=elementor-widget-container>
                <div class=elementor-divider>
                  <span class=elementor-divider-separator>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-80b8587 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=80b8587 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c0831b3" data-id=c0831b3 data-element_type=column>
        <div class=elementor-column-wrap>
          <div class=elementor-widget-wrap>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-79412e5 elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=79412e5 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c91f64d" data-id=c91f64d data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-faea789 elementor-widget elementor-widget-spacer" data-id=faea789 data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-410d6b0 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=410d6b0 data-element_type=section>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-881cfb6" data-id=881cfb6 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-49eb14c elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=49eb14c data-element_type=section>
              <div class="elementor-container elementor-column-gap-no">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-c972473" data-id=c972473 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-5f3f4fb elementor-widget elementor-widget-premium-addon-dual-header" data-id=5f3f4fb data-element_type=widget data-widget_type=premium-addon-dual-header.default>
                          <div class=elementor-widget-container>
                            <div class=premium-dual-header-container>
                              <div class=premium-dual-header-first-container>
                                <h2 class="premium-dual-header-first-header ">
                                  <span class=premium-dual-header-first-span>OFFICIAL AIRLINE ALLIANCE PARTNER 
                                  </span>
                                  <span class="premium-dual-header-second-header ">
                                  </span>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-33af974 elementor-widget elementor-widget-divider" data-id=33af974 data-element_type=widget data-widget_type=divider.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-divider>
                              <span class=elementor-divider-separator>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-44c1e50 elementor-widget elementor-widget-image" data-id=44c1e50 data-element_type=widget data-widget_type=image.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-image>
                              <img width=300 height=246 src=https://modelunitednation.org/wp-content/uploads/2019/05/oneworld-travel-bright-Vertical-NEG_130kb-300x246.png class="attachment-medium size-medium" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/05/oneworld-travel-bright-Vertical-NEG_130kb-300x246.png 300w, https://modelunitednation.org/wp-content/uploads/2019/05/oneworld-travel-bright-Vertical-NEG_130kb-768x631.png 768w, https://modelunitednation.org/wp-content/uploads/2019/05/oneworld-travel-bright-Vertical-NEG_130kb-1024x841.png 1024w, https://modelunitednation.org/wp-content/uploads/2019/05/oneworld-travel-bright-Vertical-NEG_130kb-600x493.png 600w, https://modelunitednation.org/wp-content/uploads/2019/05/oneworld-travel-bright-Vertical-NEG_130kb-510x419.png 510w, https://modelunitednation.org/wp-content/uploads/2019/05/oneworld-travel-bright-Vertical-NEG_130kb.png 1819w" sizes="(max-width: 300px) 100vw, 300px">
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-9e0e1a2 elementor-invisible elementor-widget elementor-widget-jet-brands" data-id=9e0e1a2 data-element_type=widget data-settings={&quot;_animation&quot;:&quot;slideInLeft&quot;} data-widget_type=jet-brands.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-jet-brands jet-elements">
                              <div class=brands-wrap>
                                <div class="brands-list col-row">
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-12.png alt="image (12)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-11.png alt="image (11)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-10.png alt="image (10)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-9.png alt="image (9)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-8.png alt="image (8)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-7.png alt="image (7)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-3746bf3 elementor-invisible elementor-widget elementor-widget-jet-brands" data-id=3746bf3 data-element_type=widget data-settings={&quot;_animation&quot;:&quot;slideInLeft&quot;} data-widget_type=jet-brands.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-jet-brands jet-elements">
                              <div class=brands-wrap>
                                <div class="brands-list col-row">
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-6.png alt="image (6)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-5.png alt="image (5)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-4.png alt="image (4)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-3.png alt="image (3)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image-1.png alt="image (1)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/image.png alt=image class=brands-list__item-img>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-3659002 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=3659002 data-element_type=section>
              <div class="elementor-container elementor-column-gap-no">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-7094bae" data-id=7094bae data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-89da7d9 elementor-widget elementor-widget-premium-addon-dual-header" data-id=89da7d9 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
                          <div class=elementor-widget-container>
                            <div class=premium-dual-header-container>
                              <div class=premium-dual-header-first-container>
                                <h2 class="premium-dual-header-first-header ">
                                  <span class=premium-dual-header-first-span>Supported by 
                                  </span>
                                  <span class="premium-dual-header-second-header ">
                                  </span>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-65409f3 elementor-widget elementor-widget-divider" data-id=65409f3 data-element_type=widget data-widget_type=divider.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-divider>
                              <span class=elementor-divider-separator>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-84b6252 elementor-widget elementor-widget-image" data-id=84b6252 data-element_type=widget data-widget_type=image.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-image>
                              <img width=500 height=171 src=https://modelunitednation.org/wp-content/uploads/2019/04/logo-Kalibrr1.png class="attachment-shop_single size-shop_single" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/04/logo-Kalibrr1.png 500w, https://modelunitednation.org/wp-content/uploads/2019/04/logo-Kalibrr1-300x103.png 300w" sizes="(max-width: 500px) 100vw, 500px">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-e49ae8f elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=e49ae8f data-element_type=section>
              <div class="elementor-container elementor-column-gap-no">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6d6d928" data-id=6d6d928 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-0e5aa50 elementor-widget elementor-widget-premium-addon-dual-header" data-id=0e5aa50 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
                          <div class=elementor-widget-container>
                            <div class=premium-dual-header-container>
                              <div class=premium-dual-header-first-container>
                                <h2 class="premium-dual-header-first-header ">
                                  <span class=premium-dual-header-first-span>Strategic Partner 
                                  </span>
                                  <span class="premium-dual-header-second-header ">
                                  </span>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-48d60e5 elementor-widget elementor-widget-divider" data-id=48d60e5 data-element_type=widget data-widget_type=divider.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-divider>
                              <span class=elementor-divider-separator>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-a5163d4 elementor-widget elementor-widget-image" data-id=a5163d4 data-element_type=widget data-widget_type=image.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-image>
                              <img width=510 height=206 src=https://modelunitednation.org/wp-content/uploads/2019/05/sayLogo-510x206.png class="attachment-shop_single size-shop_single" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/05/sayLogo-510x206.png 510w, https://modelunitednation.org/wp-content/uploads/2019/05/sayLogo-300x121.png 300w, https://modelunitednation.org/wp-content/uploads/2019/05/sayLogo-768x310.png 768w, https://modelunitednation.org/wp-content/uploads/2019/05/sayLogo-600x242.png 600w, https://modelunitednation.org/wp-content/uploads/2019/05/sayLogo.png 842w" sizes="(max-width: 510px) 100vw, 510px">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-7d435d5 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=7d435d5 data-element_type=section>
              <div class="elementor-container elementor-column-gap-no">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ee459b5" data-id=ee459b5 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-ee28e2f elementor-widget elementor-widget-premium-addon-dual-header" data-id=ee28e2f data-element_type=widget data-widget_type=premium-addon-dual-header.default>
                          <div class=elementor-widget-container>
                            <div class=premium-dual-header-container>
                              <div class=premium-dual-header-first-container>
                                <h2 class="premium-dual-header-first-header ">
                                  <span class=premium-dual-header-first-span>MEDIA PARTNERS 
                                  </span>
                                  <span class="premium-dual-header-second-header ">
                                  </span>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-f37b09e elementor-widget elementor-widget-divider" data-id=f37b09e data-element_type=widget data-widget_type=divider.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-divider>
                              <span class=elementor-divider-separator>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-e83cbbe elementor-invisible elementor-widget elementor-widget-jet-brands" data-id=e83cbbe data-element_type=widget data-settings={&quot;_animation&quot;:&quot;slideInLeft&quot;} data-widget_type=jet-brands.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-jet-brands jet-elements">
                              <div class=brands-wrap>
                                <div class="brands-list col-row">
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/inspirator-indonesia-.png alt="inspirator indonesia" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/hipwee2.png alt=hipwee2 class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/KU.png alt=KU class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/medpart3.png alt=medpart3 class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/info_beasiswa_trspnt.png alt=info_beasiswa_trspnt class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/GN.png alt=GN class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/PNG-LOGO-IR-STUDENT-1.png alt="PNG LOGO IR STUDENT 1" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/Logo-Rumah-Millennials1.png alt="Logo Rumah Millennials1" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/youth-oppotunities.png alt=youth-oppotunities class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/rintisan.png alt=rintisan class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/taiwanGPS.png alt=taiwanGPS class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/NegarawanMuda.png alt=NegarawanMuda class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/08/logo-luar-sekolah-baru.png alt="logo luar sekolah baru" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/akutahu-Logo_Medium1.png alt=akutahu-Logo_Medium1 class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/Yot-Yogyakarta-1.png alt="Yot Yogyakarta (1)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/Rammu2.png alt=Rammu2 class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/DIvine.png alt=DIvine class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/SarjanaVolunterr_jpg.jpeg alt=SarjanaVolunterr_jpg class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/LSR.png alt=LSR class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/SDSN_Youth_logo.png alt=SDSN_Youth_logo class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/Logo-Sekolah@MMU1_resize.png alt="Logo Sekolah@MMU1_resize" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/iium-fm.png alt=iium-fm class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/aweglobal.png alt=aweglobal class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/04/iyoin.png alt=iyoin class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/sandyainstitute.jpg alt=sandyainstitute class=brands-list__item-img>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-ea4ae7e elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=ea4ae7e data-element_type=section>
              <div class="elementor-container elementor-column-gap-no">
                <div class=elementor-row>
                  <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-4e27021" data-id=4e27021 data-element_type=column>
                    <div class="elementor-column-wrap elementor-element-populated">
                      <div class=elementor-widget-wrap>
                        <div class="elementor-element elementor-element-34ee5e6 elementor-widget elementor-widget-premium-addon-dual-header" data-id=34ee5e6 data-element_type=widget data-widget_type=premium-addon-dual-header.default>
                          <div class=elementor-widget-container>
                            <div class=premium-dual-header-container>
                              <div class=premium-dual-header-first-container>
                                <h2 class="premium-dual-header-first-header ">
                                  <span class=premium-dual-header-first-span>mun partners 
                                  </span>
                                  <span class="premium-dual-header-second-header ">
                                  </span>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-ddae30e elementor-widget elementor-widget-divider" data-id=ddae30e data-element_type=widget data-widget_type=divider.default>
                          <div class=elementor-widget-container>
                            <div class=elementor-divider>
                              <span class=elementor-divider-separator>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-8e7040d elementor-invisible elementor-widget elementor-widget-jet-brands" data-id=8e7040d data-element_type=widget data-settings={&quot;_animation&quot;:&quot;slideInLeft&quot;} data-widget_type=jet-brands.default>
                          <div class=elementor-widget-container>
                            <div class="elementor-jet-brands jet-elements">
                              <div class=brands-wrap>
                                <div class="brands-list col-row">
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/pimun2.png alt=pimun2 class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/02/dimun2.png alt=dimun2 class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/15352692651021_200kb.png alt=15352692651021_200kb class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/globalAliance.jpg alt=globalAliance class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/03/mun-partner-1.png alt="mun-partner (1)" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/04/daimun.png alt=daimun class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/joinmun-2019-logo.png alt="joinmun 2019 logo" class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/DMUN_300kb.png alt=DMUN_300kb class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/kitmun_FIX.png alt=kitmun_FIX class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/05/VJMUN.jpeg alt=VJMUN class=brands-list__item-img>
                                    </div>
                                  </div>
                                  <div class="brands-list__item col-desk-6 col-tab-2 col-mob-3">
                                    <div class=brands-list__item-img-wrap>
                                      <img src=https://modelunitednation.org/wp-content/uploads/2019/06/IsarMUN-Logo_fix.png alt="IsarMUN Logo_fix" class=brands-list__item-img>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-fc754dd elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=fc754dd data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
  <div class="elementor-container elementor-column-gap-no">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2dd82b7" data-id=2dd82b7 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-19f3b79 elementor-widget elementor-widget-spacer" data-id=19f3b79 data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-7c908a8 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=7c908a8 data-element_type=section>
  <div class="elementor-container elementor-column-gap-default">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-ab83e51" data-id=ab83e51 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-3170a3c elementor-widget elementor-widget-image-carousel" data-id=3170a3c data-element_type=widget data-settings={&quot;slides_to_show&quot;:&quot;1&quot;,&quot;navigation&quot;:&quot;none&quot;,&quot;autoplay_speed&quot;:1000,&quot;pause_on_hover&quot;:&quot;no&quot;,&quot;speed&quot;:1000,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;infinite&quot;:&quot;yes&quot;,&quot;effect&quot;:&quot;slide&quot;} data-widget_type=image-carousel.default>
              <div class=elementor-widget-container>
                <div class="elementor-image-carousel-wrapper swiper-container" dir=ltr>
                  <div class="elementor-image-carousel swiper-wrapper swiper-image-stretch">
                    <div class=swiper-slide>
                      <figure class=swiper-slide-inner>
                        <img class=swiper-slide-image src=https://modelunitednation.org/wp-content/uploads/2019/02/picc9-1024x680.jpg alt=picc9>
                      </figure>
                    </div>
                    <div class=swiper-slide>
                      <figure class=swiper-slide-inner>
                        <img class=swiper-slide-image src=https://modelunitednation.org/wp-content/uploads/2019/02/picc5-min.png alt=picc5-min>
                      </figure>
                    </div>
                    <div class=swiper-slide>
                      <figure class=swiper-slide-inner>
                        <img class=swiper-slide-image src=https://modelunitednation.org/wp-content/uploads/2019/02/picc3_rez-1024x680.jpg alt=picc3_rez>
                      </figure>
                    </div>
                    <div class=swiper-slide>
                      <figure class=swiper-slide-inner>
                        <img class=swiper-slide-image src=https://modelunitednation.org/wp-content/uploads/2019/02/picc7.jpg alt=picc7>
                      </figure>
                    </div>
                    <div class=swiper-slide>
                      <figure class=swiper-slide-inner>
                        <img class=swiper-slide-image src=https://modelunitednation.org/wp-content/uploads/2019/02/putrajaya-international-convention-center-4-1024x651.jpg alt=putrajaya-international-convention-center-4>
                      </figure>
                    </div>
                    <div class=swiper-slide>
                      <figure class=swiper-slide-inner>
                        <img class=swiper-slide-image src=https://modelunitednation.org/wp-content/uploads/2019/02/picc6-1024x682.jpg alt=picc6>
                      </figure>
                    </div>
                    <div class=swiper-slide>
                      <figure class=swiper-slide-inner>
                        <img class=swiper-slide-image src=https://modelunitednation.org/wp-content/uploads/2019/02/picc10_rez-1024x681.jpg alt=picc10_rez>
                      </figure>
                    </div>
                    <div class=swiper-slide>
                      <figure class=swiper-slide-inner>
                        <img class=swiper-slide-image src=https://modelunitednation.org/wp-content/uploads/2019/02/picc3_rez-1-1024x680.jpg alt=picc3_rez>
                      </figure>
                    </div>
                    <div class=swiper-slide>
                      <figure class=swiper-slide-inner>
                        <img class=swiper-slide-image src=https://modelunitednation.org/wp-content/uploads/2019/02/picc4-_rez-1024x678.jpg alt="picc4 _rez">
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="has_eae_slider elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-8095823" data-id=8095823 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-e7e1220 elementor-widget elementor-widget-image" data-id=e7e1220 data-element_type=widget data-widget_type=image.default>
              <div class=elementor-widget-container>
                <div class=elementor-image>
                  <figure class=wp-caption>
                    <a href=https://goo.gl/maps/GtADAK3YKQx>
                      <img width=1008 height=540 src=https://modelunitednation.org/wp-content/uploads/2019/02/putrajaya-ayimun.png class="attachment-large size-large" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/putrajaya-ayimun.png 1008w, https://modelunitednation.org/wp-content/uploads/2019/02/putrajaya-ayimun-300x161.png 300w, https://modelunitednation.org/wp-content/uploads/2019/02/putrajaya-ayimun-768x411.png 768w, https://modelunitednation.org/wp-content/uploads/2019/02/putrajaya-ayimun-600x321.png 600w, https://modelunitednation.org/wp-content/uploads/2019/02/putrajaya-ayimun-510x273.png 510w" sizes="(max-width: 1008px) 100vw, 1008px"> 
                    </a>
                    <figcaption class="widget-image-caption wp-caption-text">Venue Address : Jalan P5, Presint 5, 62200 Putrajaya, Wilayah Persekutuan Putrajaya, Malaysia | Phone +60 3-8887 6000
                    </figcaption>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> 
<script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">jQuery(document).ready(function () {
                        jQuery(".elementor-element-42be9ab").prepend('<div class=eae-section-bs><div class=eae-section-bs-inner></div></div>');
                        var bgimage = '';
                        if ('yes' == 'yes') {
                            //if(bgimage == ''){
                            //    var bgoverlay = '';
                            //}else{
                            var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder//assets/lib/vegas/overlays/00.png';
                            // }
                        } else {
                            if ('') {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/.png';
                            } else {
                                var bgoverlay = 'https://modelunitednation.org/wp-content/plugins/addon-elements-for-elementor-page-builder/assets/lib/vegas/overlays/00.png';
                            }
                        }
                        jQuery(".elementor-element-42be9ab").children('.eae-section-bs').children('.eae-section-bs-inner').vegas({
                            slides: [{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/foto5.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN1.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN2.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN3.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/AYIMUN4.jpg"},{"src":"https:\/\/modelunitednation.org\/wp-content\/uploads\/2019\/02\/Diplomatic-Dinner.jpg"}],
                            transition: 'fade2',
                            animation: 'kenburnsLeft',
                            overlay: bgoverlay,
                            cover: true,
                            delay: 4000,
                            timer: false                });
                        if ('yes' == 'yes') {
                            jQuery(".elementor-element-42be9ab").children('.eae-section-bs').children('.eae-section-bs-inner').children('.vegas-overlay').css('background-image', '');
                        }
                    });</script> 
<section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-42be9ab elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=42be9ab data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;pyramids&quot;}>
  <div class=elementor-background-overlay>
  </div>
  <div class="elementor-shape elementor-shape-top" data-negative=false>
    <svg xmlns=http://www.w3.org/2000/svg viewBox="0 0 1000 100" preserveAspectRatio=none>
      <path class=elementor-shape-fill d=M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9 />
    </svg>
  </div>
  <div class="elementor-container elementor-column-gap-default">
    <div class=elementor-row>
      <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c37a3a6" data-id=c37a3a6 data-element_type=column>
        <div class="elementor-column-wrap elementor-element-populated">
          <div class=elementor-widget-wrap>
            <div class="elementor-element elementor-element-93e30c2 elementor-widget elementor-widget-heading" data-id=93e30c2 data-element_type=widget data-widget_type=heading.default>
              <div class=elementor-widget-container>
                <h2 class="elementor-heading-title elementor-size-default">Make an Impact
                </h2>
              </div>
            </div>
            <div class="elementor-element elementor-element-b76da9c elementor-widget elementor-widget-spacer" data-id=b76da9c data-element_type=widget data-widget_type=spacer.default>
              <div class=elementor-widget-container>
                <div class=elementor-spacer>
                  <div class=elementor-spacer-inner>
                  </div>
                </div>
              </div>
            </div>
            <div class="elementor-element elementor-element-008281b elementor-align-center elementor-mobile-align-right elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button" data-id=008281b data-element_type=widget data-widget_type=button.default>
              <div class=elementor-widget-container>
                <div class=elementor-button-wrapper>
                  <a href=# class="elementor-button-link elementor-button elementor-size-sm elementor-animation-pulse-shrink" role=button>
                    <span class=elementor-button-content-wrapper>
                      <span class=elementor-button-text>Registration has CLOSED
                      </span>
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
</div>
</div>
<div id=snippet-box class=snippet-type-2 style="background:#F5F5F5; color:#333333; border:1px solid #ACACAC;">
  <div class=snippet-title style="background:#E4E4E4; color:#333333; border-bottom:1px solid #ACACAC;">Summary
  </div>
  <div itemscope itemtype=https://schema.org/Event>
    <script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">jQuery(document).ready(function() {
                            jQuery(".snippet-label-img").addClass("snippet-clear");
                        });</script>
    <div class=aio-info>
      <div class=snippet-label-img>Event
      </div>
      <div class=snippet-data-img>​
        <span itemprop=name>AYIMUN - Asia Youth International Model United Nations 2019
        </span>
      </div>
      <meta itemprop=url content=modelunitednation.org>
      <div class=snippet-clear>
      </div>
      <div class=snippet-label-img>Location
      </div>
      <div class=snippet-data-img>
        ​
        <span itemprop=location itemscope itemtype=https://schema.org/Place>
          <span itemprop=name>Putrajaya International Convention Centre
          </span>,
          <span itemprop=address itemscope itemtype=https://schema.org/PostalAddress>
            <span itemprop=streetAddress>Jalan P5, Presint 5, 62200 Putrajaya, Wilayah Persekutuan Putrajaya, Malaysia
            </span>,
            <span itemprop=addressLocality>Putrajaya, Malaysia
            </span>,-
            <span itemprop=postalCode>62200
            </span>
          </span>
        </span>
      </div>
      <div class=snippet-clear>
      </div>
      <div class=snippet-label-img>Starting on
      </div>
      <div class=snippet-data-img> 
        <span itemprop=startDate datetime="25 August 2019T00:00-00:00">25 August 2019
        </span>
      </div>
      <div class=snippet-clear>
      </div>
      <div class=snippet-label-img>Ending on
      </div>
      <div class=snippet-data-img> 
        <span itemprop=endDate datetime="28 August 2019T00:00-00:00">28 August 2019
        </span>
      </div>
      <div class=snippet-clear>
      </div>
      <div class=snippet-label-img>Description
      </div>
      <div class=snippet-data-img> 
        <span itemprop=description>Asia Youth International Model United Nations (AYIMUN) 2019 is international United Nation simulation conference where youth can share thoughts/ideas about social issues and learn diplomacy, leadership, networking &amp; negotiation skills.
        </span>
      </div>
      <div class=snippet-clear>
      </div>
    </div>
  </div>
</div>
<meta itemprop=description content=Event>
<div class=snippet-clear>
</div>
</div>
<footer id=colophon class="site-footer ">
  <div data-elementor-type=jet_footer data-elementor-id=364 class="elementor elementor-364" data-elementor-settings=[]>
    <div class=elementor-inner>
      <div class=elementor-section-wrap>
        <section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-5cc260a elementor-section-content-top elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=5cc260a data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
          <div class="elementor-container elementor-column-gap-no">
            <div class=elementor-row>
              <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-69e4452" data-id=69e4452 data-element_type=column>
                <div class="elementor-column-wrap elementor-element-populated">
                  <div class=elementor-widget-wrap>
                    <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-55e19cc elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id=55e19cc data-element_type=section>
                      <div class="elementor-container elementor-column-gap-default">
                        <div class=elementor-row>
                          <div class="has_eae_slider elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3c64a59" data-id=3c64a59 data-element_type=column>
                            <div class="elementor-column-wrap elementor-element-populated">
                              <div class=elementor-widget-wrap>
                                <div class="elementor-element elementor-element-e1ddaf8 elementor-widget elementor-widget-image" data-id=e1ddaf8 data-element_type=widget data-widget_type=image.default>
                                  <div class=elementor-widget-container>
                                    <div class=elementor-image>
                                      <img width=150 height=150 src=https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN-LOGO-08-150x150.png class="attachment-thumbnail size-thumbnail" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN-LOGO-08-150x150.png 150w, https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN-LOGO-08-230x230.png 230w, https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN-LOGO-08-400x400.png 400w, https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN-LOGO-08-640x640.png 640w, https://modelunitednation.org/wp-content/uploads/2019/02/AYIMUN-LOGO-08-100x100.png 100w" sizes="(max-width: 150px) 100vw, 150px">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="has_eae_slider elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-19b1b10" data-id=19b1b10 data-element_type=column>
                            <div class="elementor-column-wrap elementor-element-populated">
                              <div class=elementor-widget-wrap>
                                <div class="elementor-element elementor-element-27fb847 elementor-widget elementor-widget-heading" data-id=27fb847 data-element_type=widget data-widget_type=heading.default>
                                  <div class=elementor-widget-container>
                                    <h5 class="elementor-heading-title elementor-size-default">Contact
                                    </h5>
                                  </div>
                                </div>
                                <div class="elementor-element elementor-element-1f66da2 elementor-align-left elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id=1f66da2 data-element_type=widget data-widget_type=icon-list.default>
                                  <div class=elementor-widget-container>
                                    <ul class=elementor-icon-list-items>
                                      <li class=elementor-icon-list-item>
                                        <a href="/cdn-cgi/l/email-protection#8eeff7e7e3fbe0cee7e0faebfce0effae7e1e0efe2e9e2e1ecefe2e0ebfaf9e1fce5a0ede1e3" target=_blank> 
                                          <span class=elementor-icon-list-icon>
                                            <i aria-hidden=true class="far fa-envelope">
                                            </i> 
                                          </span>
                                          <span class=elementor-icon-list-text>
                                            <span class="__cf_email__" data-cfemail="5d3c24343028331d343329382f333c293432333c313a31323f3c313338292a322f36733e3230">[email&#160;protected]
                                            </span>
                                          </span>
                                        </a>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="has_eae_slider elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-d9cadac" data-id=d9cadac data-element_type=column>
                            <div class="elementor-column-wrap elementor-element-populated">
                              <div class=elementor-widget-wrap>
                                <div class="elementor-element elementor-element-ca816dc elementor-widget elementor-widget-heading" data-id=ca816dc data-element_type=widget data-widget_type=heading.default>
                                  <div class=elementor-widget-container>
                                    <h5 class="elementor-heading-title elementor-size-default">Follow Us
                                    </h5>
                                  </div>
                                </div>
                                <div class="elementor-element elementor-element-db7e151 elementor-shape-circle elementor-grid-0 elementor-widget elementor-widget-social-icons" data-id=db7e151 data-element_type=widget data-widget_type=social-icons.default>
                                  <div class=elementor-widget-container>
                                    <div class="elementor-social-icons-wrapper elementor-grid">
                                      <div class=elementor-grid-item>
                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-facebook-f elementor-animation-pulse elementor-repeater-item-3ecbd84" href=https://www.facebook.com/internationalmodelun/ target=_blank>
                                          <span class=elementor-screen-only>Facebook-f
                                          </span>
                                          <i class="fab fa-facebook-f">
                                          </i> 
                                        </a>
                                      </div>
                                      <div class=elementor-grid-item>
                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-pulse elementor-repeater-item-d3bd20a" href=https://www.instagram.com/internationalmun/ target=_blank>
                                          <span class=elementor-screen-only>Instagram
                                          </span>
                                          <i class="fab fa-instagram">
                                          </i> 
                                        </a>
                                      </div>
                                      <div class=elementor-grid-item>
                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-youtube elementor-animation-pulse elementor-repeater-item-d8d1df4" href=https://www.youtube.com/channel/UCji9RDaSd46lsxpwx7lSPqw target=_blank>
                                          <span class=elementor-screen-only>Youtube
                                          </span>
                                          <i class="fab fa-youtube">
                                          </i> 
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-0aa9097 elementor-section-full_width elementor-section-content-top elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-height-default elementor-section-height-default" data-id=0aa9097 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
          <div class="elementor-container elementor-column-gap-no">
            <div class=elementor-row>
              <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4b9c771" data-id=4b9c771 data-element_type=column>
                <div class="elementor-column-wrap elementor-element-populated">
                  <div class=elementor-widget-wrap>
                    <section class="has_eae_slider elementor-section elementor-inner-section elementor-element elementor-element-9d0d29b elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id=9d0d29b data-element_type=section>
                      <div class="elementor-container elementor-column-gap-default">
                        <div class=elementor-row>
                          <div class="has_eae_slider elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-835678a" data-id=835678a data-element_type=column>
                            <div class="elementor-column-wrap elementor-element-populated">
                              <div class=elementor-widget-wrap>
                                <div class="elementor-element elementor-element-56fdac9 elementor-widget elementor-widget-image" data-id=56fdac9 data-element_type=widget data-widget_type=image.default>
                                  <div class=elementor-widget-container>
                                    <div class=elementor-image>
                                      <img width=472 height=109 src=https://modelunitednation.org/wp-content/uploads/2018/07/logo-ayimun-transparent.png class="attachment-full size-full" alt loading=lazy srcset="https://modelunitednation.org/wp-content/uploads/2018/07/logo-ayimun-transparent.png 472w, https://modelunitednation.org/wp-content/uploads/2018/07/logo-ayimun-transparent-300x69.png 300w" sizes="(max-width: 472px) 100vw, 472px">
                                    </div>
                                  </div>
                                </div>
                                <div class="elementor-element elementor-element-c0550a2 elementor-widget elementor-widget-heading" data-id=c0550a2 data-element_type=widget data-widget_type=heading.default>
                                  <div class=elementor-widget-container>
                                    <h2 class="elementor-heading-title elementor-size-default">
                                      <strong>International Global Network
                                      </strong>, 
                                      <br>100, Jalan Putra, Chow Kit, 50350 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia
                                    </h2>
                                  </div>
                                </div>
                                <div class="elementor-element elementor-element-2e513d7 elementor-align-center elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id=2e513d7 data-element_type=widget data-widget_type=icon-list.default>
                                  <div class=elementor-widget-container>
                                    <ul class=elementor-icon-list-items>
                                      <li class=elementor-icon-list-item>
                                        <a href="/cdn-cgi/l/email-protection#2a4b5343475f446a43445e4f58444b5e4345444b464d4645484b46444f5e5d45584104494547" target=_blank> 
                                          <span class=elementor-icon-list-icon>
                                            <i aria-hidden=true class="far fa-envelope">
                                            </i> 
                                          </span>
                                          <span class=elementor-icon-list-text>
                                            <span class="__cf_email__" data-cfemail="8cedf5e5e1f9e2cce5e2f8e9fee2edf8e5e3e2ede0ebe0e3eeede0e2e9f8fbe3fee7a2efe3e1">[email&#160;protected]
                                            </span>
                                          </span>
                                        </a>
                                      </li>
                                      <li class=elementor-icon-list-item>
                                        <a href target=_blank> 
                                          <span class=elementor-icon-list-icon>
                                            <i aria-hidden=true class="fas fa-phone">
                                            </i> 
                                          </span>
                                          <span class=elementor-icon-list-text>+62-813-1623-8523
                                          </span>
                                        </a>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                                <div class="elementor-element elementor-element-58ac976 elementor-shape-circle elementor-grid-0 elementor-widget elementor-widget-social-icons" data-id=58ac976 data-element_type=widget data-widget_type=social-icons.default>
                                  <div class=elementor-widget-container>
                                    <div class="elementor-social-icons-wrapper elementor-grid">
                                      <div class=elementor-grid-item>
                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-facebook elementor-animation-pulse elementor-repeater-item-3ecbd84" href=https://www.facebook.com/internationalmodelun/ target=_blank>
                                          <span class=elementor-screen-only>Facebook
                                          </span>
                                          <i class="fa fa-facebook">
                                          </i>
                                        </a>
                                      </div>
                                      <div class=elementor-grid-item>
                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-pulse elementor-repeater-item-d3bd20a" href=https://www.instagram.com/internationalmun/ target=_blank>
                                          <span class=elementor-screen-only>Instagram
                                          </span>
                                          <i class="fa fa-instagram">
                                          </i>
                                        </a>
                                      </div>
                                      <div class=elementor-grid-item>
                                        <a class="elementor-icon elementor-social-icon elementor-social-icon-youtube elementor-animation-pulse elementor-repeater-item-d8d1df4" href=https://www.youtube.com/channel/UCji9RDaSd46lsxpwx7lSPqw target=_blank>
                                          <span class=elementor-screen-only>Youtube
                                          </span>
                                          <i class="fa fa-youtube">
                                          </i>
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-a129825 elementor-section-full_width elementor-section-content-middle elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-height-default elementor-section-height-default" data-id=a129825 data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
          <div class="elementor-container elementor-column-gap-no">
            <div class=elementor-row>
              <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1ed4de5" data-id=1ed4de5 data-element_type=column>
                <div class="elementor-column-wrap elementor-element-populated">
                  <div class=elementor-widget-wrap>
                    <div class="elementor-element elementor-element-5a5271c elementor-widget elementor-widget-heading" data-id=5a5271c data-element_type=widget data-widget_type=heading.default>
                      <div class=elementor-widget-container>
                        <h2 class="elementor-heading-title elementor-size-default">
                          <p>
                            <span>Organized by 
                              <a href=https://internationalglobalnetwork.com/>
                              <strong>International Global Network
                              </strong>
                              </a>   -  
                            <a href=https://modelunitednation.org/terms-and-condition/ target=_blank rel=noopener>
                              <strong>TERMS AND CONDITIONS
                              </strong>
                            </a>
                          </span>
                        </p>
                      </h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
      </div>
      </section>
    <section class="has_eae_slider elementor-section elementor-top-section elementor-element elementor-element-db0beeb elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id=db0beeb data-element_type=section data-settings={&quot;background_background&quot;:&quot;classic&quot;}>
      <div class="elementor-container elementor-column-gap-narrow">
        <div class=elementor-row>
          <div class="has_eae_slider elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-0f56139" data-id=0f56139 data-element_type=column>
            <div class="elementor-column-wrap elementor-element-populated">
              <div class=elementor-widget-wrap>
                <div class="elementor-element elementor-element-876c1ca elementor-widget elementor-widget-heading" data-id=876c1ca data-element_type=widget data-widget_type=heading.default>
                  <div class=elementor-widget-container>
                    <h2 class="elementor-heading-title elementor-size-default">
                      <p class=tulfoot2> Organized by 
                        <a href=https://internationalglobalnetwork.com/ style="color: #ffffff;" target=_blank>
                          <strong>International Global Network
                          </strong>
                        </a> | © Copyright&nbsp;
                        <a href=https://modelunitednation.org target=_blank style="color: #ffffff;">
                          <strong>(AYIMUN) Asia Youth International MUN
                          </strong>
                        </a>&nbsp; -&nbsp;&nbsp;
                        <a href=https://modelunitednation.org/terms-and-condition/ target=_blank style="color: #ffffff;">
                          <strong>TERMS AND CONDITIONS
                          </strong>
                        </a>
                      </p>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>
</div>
</footer>
</div>
<noscript>
  <iframe
          src="https://www.googletagmanager.com/ns.html?id=GTM-MXB4C94"
          height=0 width=0 style=display:none;visibility:hidden>
  </iframe>
</noscript> 
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">
</script>
<script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">window.__lc = window.__lc || {};
        window.__lc.license = 10350237;
        window.__lc.chat_between_groups = false;
        (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
        })();</script> 
<noscript>
  <a
     href=https://www.livechatinc.com/chat-with/10350237/ rel=nofollow>Chat with us
  </a>,
  powered by 
  <a
     href=https://www.livechatinc.com/?welcome rel="noopener nofollow" target=_blank>LiveChat
  </a>
</noscript>
<div id=pum-42130 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake="{&quot;id&quot;:42130,&quot;slug&quot;:&quot;fao&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;.faossss&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;medium&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}" role=dialog aria-hidden=true>
  <div id=popmake-42130 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>INI adalah FAO
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      CLOSE 
    </button>
  </div>
</div>
<div id=pum-50704 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:50704,&quot;slug&quot;:&quot;for-munpedia&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-50704 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>&lt;a href=&#8221;https://www.w3schools.com&#8221;&gt;
        <br>
        &lt;img border=&#8221;0&#8243; alt=&#8221;W3Schools&#8221; src=&#8221;logo_w3s.gif&#8221; &gt;
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37061 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37061,&quot;slug&quot;:&quot;dr-dino-patti-djalal&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37061 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.16.1&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.16.1&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.16.1&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/AYIMUN-SPEAKER-01.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.16.1&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Dr. Dino Patti Djalal is Founder of Foreign Policy Community of Indonesia (FPCI). Previously, he was a career diplomat and ambassador, best selling author, accomplished academic, youth activist, and leader of the Indonesian diaspora community. He earned his Bachelor&#8217;s Degree in Political Science from Carleton University (Ottawa, Canada); a Masters Degree in Political Science from Simon Fraser University (Vancouver, Canada), and a Phd in International Relations from the London School of Economics and Political Science (London, UK). In 2004, when President Susilo Bambang Yudhoyono began his term, Dino was appointed Special Staff of the President for International Affairs. From 2010 to 2013, Dino served as Indonesia&#8217;s ambassador to the United States. In June 2014, Dino was appointed Vice Minister for Foreign Affairs, until October that year. He has founded the Foreign Policy Community of Indonesia, which now has become the largest foreign policy group in the country, with over 60,000 people in FPCI network.
      </p>
      <hr style="border-width: 3px;">
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-41100 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:41100,&quot;slug&quot;:&quot;testimonial-video-basak-nur-karasen&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-41100 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>
        <iframe title="AYIMUN 2018 | Basak Nur Karasen" width=1200 height=675 src="https://www.youtube.com/embed/gvrlWiol5GY?feature=oembed" frameborder=0 allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
        </iframe>
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-38213 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:38213,&quot;slug&quot;:&quot;thxvote&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-38213 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2>
      </h2>
      <h2 style="text-align: center;">
        <strong>Thank you for sparing your time to vote for Hand in Hand with AYIMUN, future leaders! Kindly wait for our further notice.
        </strong>
      </h2>
      <p>&nbsp;
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-38015 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:38015,&quot;slug&quot;:&quot;reg_popup&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-38015 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37477 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37477,&quot;slug&quot;:&quot;aung-ko-oo&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37477 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.17.2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.17.2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.17.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-29-at-10.27.34.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.17.2&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>The one sociable and dynamic age of 18 youth named Aung Ko Oo (Token) from Myanmar and currently I&#8217;m studying about Bachelor of Business Administration in specialize of Marketing at Bangkok University International. Being a part of volunteer team for AYIMUN makes me feel totally energetic and hopefully could get lots of network and initiative for my future. At last but not least, warmly welcome to Thailand and waiting for you guys to keep up well conversation.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37475 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37475,&quot;slug&quot;:&quot;robin-jerome-banta&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37475 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.17.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.17.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.17.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-29-at-10.28.02.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.17.2&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Robin Jerome Banta is a senior Mechanical Engineering student from the University of the Philippines Diliman. He is a student assistant in the university, a volunteer in the college, and an active member in his academic and socio-civic organizations. He is currently studying Automation Engineering at King Mongkut’s University of Technology Thonburi in Thailand through the ASEAN International Mobility Scholarship Program. As an exchange student, he is engaged in the international setting. AYIMUN 2018 would enable him to share knowledge, insights and experiences to the international community and to learn ideas from them as well.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37473 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37473,&quot;slug&quot;:&quot;amree-dueramae&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37473 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.17.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.17.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.17.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-29-at-10.26.52.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.17.2&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hi everyone, my name is Amree Dueramae you can call me arm from Thailand, currently, studying at University Utara Malaysia in Bachelor of International Affairs Management with Honors Minor in Diplomacy and International Law. I’m an active, passionate, ambitious, funny and open-minded person. I was a committee of Thai Student Association in Malaysia as well as Vice president of Thai Student in North of Malaysia. I was attended many programs for improve myself thought challenging and opportunity. Look forward to see you all.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37469 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37469,&quot;slug&quot;:&quot;chornelius-fivtria-yuwana&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37469 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.17.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.17.2&#8243; module_alignment=&#8221;center&#8221; make_fullwidth=&#8221;on&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.17.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-29-at-10.26.23-1.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.17.2&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>My name is Chornelius Fivtria Yuwana Putra, all my friends call me Cornelius, Cornel, or Korn. Currently, I am doing my bachelor degree majoring in Internatinal Business in Rangsit International College, Rangsit University, Thailand. I have an interest in social and cultural fields. Now, I am taking part as a member of Indonesian Student Association in Thailand ( PERTMITHA ) and also Generasi Wisata ( GenWI) Thailand Chapter which responsible for promoting Indonesia culture and tourism potential. I have a friendly and cheerful personality, therefore I have many friends in campus and organizations. In addition, my friends know me as an optimistic person and do not hestitate to help friends who are in need of my help. Participating as a commitee in &#8220;MUN&#8221; is such a valuable experience for me becuase I can meet with other students from various countries who have different background, which can be my benefit to enhance my knowlegde and relation for my future. My hope that in the future, I can participate in the next &#8220;MUN&#8221; not only as commitee, but also as the delegate, so that I can improve my critical thinking skill and gain my awareness towards my social environment.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37423 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37423,&quot;slug&quot;:&quot;dominique&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37423 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.17.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.17.2&#8243; module_alignment=&#8221;center&#8221; make_fullwidth=&#8221;on&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.17.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-23-at-15.00.53.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.17.2&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Dominique is a final year law student in Faculty of Law, Universitas Indonesia. Currently majoring in Public International Law , MUN has been equipping her for her studies throughout her college life. Her extensive MUN journey consists of her participating as delegate, chair, and Secretariat.
      </p>
      <p>As a delegate, she has joined numerous national and international MUNs and snatched awards, notably Diplomatic Commendation of Legal Committee in Harvard National MUN 2018 and Best Delegate of UN Human Rights Council in The European International MUN 2017. As a Secretariat, she just finished her duty as the Secretary General of Asia World MUN 2018, while previously served as an Under Secretary-General for Asia Youth International MUN 2017 and Indonesia MUN 2017. She&#8217;s currently an Under Secretary-General for Committee Management for Geneva International MUN 2019.
      </p>
      <p>Her chairing experience also ranges from national to several international MUNs, most notably Director of G20 in The European International MUN 2018 and Chairperson of Legal Committee in Markaz United Youth Summit 2018 in India.
      </p>
      <p>She is pleased to serve as the chair for this year&#8217;s AYIMUN and she&#8217;s looking forward to meet you in Bangkok!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37386 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37386,&quot;slug&quot;:&quot;safarov-jasur&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37386 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.17.1&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.17.1&#8243; module_alignment=&#8221;center&#8221; make_fullwidth=&#8221;on&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.17.1&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-19-at-16.20.37.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.17.1&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Good day
        <br>
        My full name is Safarov Jasur and i am from Uzbekistan as one of the committee of Asia Youth International Model United Nations 2018. Being confident by myself, I have a passion on challenging myself in such great events all the time. Participation in the “MUN” as a volunteer would be an honorable opportunity to submerge in an international atmosphere. Also it is a huge chance for everybody to do their best and show their selves in such kind of international conference as Asia Youth International Model United Nations.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37063 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37063,&quot;slug&quot;:&quot;sipim-sornbanlang-b-a-m-p-s-ph-d&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37063 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.16.1&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.16.1&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.17&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/AYIMUN-SPEAKER-03-1.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.16.1&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Sipim Sornbanlang presently lives and works in Bangkok she originally hails from Chiang mai in northern Thailand.  Sipim received her Ph.D. and M.P.S. in International Relations from Chulalongkorn University, while earlier obtaining a B.A. in English from Chiang Mai University. She has extensive experience working in the public and private sector, with companies and government agencies, and more recently has entered academia as a lecturer at Srinakharinwirot University.
      </p>
      <p>Over the past couple of years Sipim has served as a guest lecturer and moderator at many workshops and seminars dealing with migration, security issues for migrant workers, human trafficking, and globalization and its effects on countries and people living in Thailand and other southeast Asian countries.
      </p>
      <p>Being interested in international security issues Sipim has been involved in a number research projects dealing with the interaction of security on associated national immigration policies and their effects on migrant workers in Thailand and the region.  Sipim grew up in a rural community in northern Thailand and this experience has helped foster a deep commitment to improving human rights and human security issues; by promoting the political rights of women and migrant works.  One of Sipim&#8217;s goals is to help reduce or hopefully eliminate poverty in Thailand and in other parts of the world. Much of Sipim&#8217;s work revolves on finding practical ways to improve the standard of life for people, especially for underrepresented minorities.
      </p>
      <hr style="border-width: 3px;">
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37058 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37058,&quot;slug&quot;:&quot;ass-prof-dr-rosalia-sciortino-sumaryono&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37058 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.16.1&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.16.1&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.16.1&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/AYIMUN-SPEAKER-02.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.16.1&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Assc. Prof. Dr. Rosalia (Lia) Sciortino Sumaryono, a cultural anthropologist and development sociologist by training earned her doctorate at the Vrije Universiteit, Amsterdam with honors. Currently, she is Associate Professor at the Institute for Population and Social Research, Mahidol University in Thailand, Visiting Professor at the Master in International Development at Chulalongkorn University, and Senior Advisor to two Indonesia-Australia social development programs in Indonesia, namely the Empowering Indonesia Women for Poverty Reduction and the Knowledge Sector Initiative.
      </p>
      <p>Most recently, Dr. Sciortino was IDRC Regional Director for Southeast and East Asia (2010-2014), Senior Adviser to the Australian Agency for International Development in Indonesia (2009-2010), and Regional Director for Southeast Asia of the Rockefeller Foundation (2000-2007) establishing during her tenure the Foundation’s Southeast Asia Office in Bangkok. Prior to that, she was program officer Human Development and Reproductive Health at the Indonesia and Philippines offices of the Ford Foundation from 1993 to 2000.
      </p>
      <p>Dr. Sciortino has acted as a consultant for international and regional organizations, as an adviser for non-profit organizations, and as a visiting professor at various Southeast Asian and Dutch universities. She has published widely on development issues in Southeast Asia, and her books Care-Takers of Cure (Perawat Puskesmas di antara Pengobatan dan Perawatan) and “Civilized Health Care” (Kesehatan Madani) are widely read by those interested in social health in Indonesia. A native of Palermo, Italy, she fluently speaks Italian, Dutch, English and Bahasa Indonesia.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37027 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37027,&quot;slug&quot;:&quot;taratan-intarachatorn&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37027 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.16.1&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.16.1&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.16.1&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-11-at-13.04.25.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.16.1&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Taratan Intarachatorn or Janjao is currently a first year student from the faculty of political science, majoring in international relations from Thammasat University, Thailand. She is also a master of ceremonies of her university. For more information, please check on facebook page ‘MC of Thammasat’. Taratan is very ambitious and she is always trying to improve herself through challenging opportunities. She urges to learn from new environments and is willing to adapt to them as well!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-37025 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:37025,&quot;slug&quot;:&quot;weerapot-bunlong&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-37025 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.16.1&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.16.1&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.16.1&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-11-at-15.40.26.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.16.1&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Weerapot Bunlong or Gotji is currently a second year student from the faculty of political science, majoring international relations, Thammasat University. He is also the master of ceremonies of the university (you can go have a look at his previous hosted events on Facebook page ‘MC of Thammasat’ or ‘MC Thammasat’) He is a very open-minded person who eagerly would like to accumulate experience and learn new things from different kind of people. Meaning that he is looking forward to meeting everyone 🙂
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33700 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33700,&quot;slug&quot;:&quot;chindy-fitridani&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33700 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-08-at-17.55.44.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Chindy Fitridani is a fourth year, undergraduate student in University of the Thai Chamber of Commerce in Bangkok, Thailand, majoring in Business English. She is interested in English Language and have been working as freelance interpreter for English-Bahasa Indonesia Language and since past three years she has spending her free time as a tour guide for Indonesian community who comes to Thailand for travelling. She is Friendly and easy going person.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36938 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36938,&quot;slug&quot;:&quot;selly-remiandayu&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36938 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-05-at-15.10.17-1.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hello, my name is Selly from Indonesia. I love meeting new people and learning new things, so please feel free to say hello and share a story with me. I am thrilled and truly blessed to be a part of Asia Youth International Model United Nation. See you guys in Bangkok!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36942 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36942,&quot;slug&quot;:&quot;john-braynel-mago-pural&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36942 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-05-at-15.10.18.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>I am John Braynel Mago Pural, 21 years old, single, a first year law student from the Philippines. I graduated from the University of Santo Tomas (UST), with a Bachelor’s degree of AB Legal Management. I spearheaded several community development projects as I was the External Vice President of UST Rotaract. I have become part of several organizations and student council during my college days. I am very friendly, outgoing and professional. I consider myself as a team player. I know the importance of communication. I am very well aware of international, legal and political issues in Asia.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36940 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36940,&quot;slug&quot;:&quot;yen-my-vo&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36940 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-05-at-15.10.17-2.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hello, I am Yen My from Vietnam as one of the committee of Asia Youth International Model United Nations 2018. I am glad to see all of you – delegates in this year. With 9 committee, AYIMUN hopes you can cultivate knowledge, skills and boost your confidence through solving hot issues. I am here to support and give a hand for all of you if needed. Thank you for joining us and hope all delegates will have a precious time. See you in Bangkok!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36934 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36934,&quot;slug&quot;:&quot;hang-tola&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36934 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-05-at-15.10.17.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hello! My name is Hang Tola from Cambodia. I am graduated in 2015 in the field of Tourism and Hospitality Management at one local university. Currently I am a teacher at one private school in city. In my opinion, the better education I provide the better community everyone will live in. I am so curios of this career
        <br>
        and I strongly believe that after back from the AYIMUN, I will have so much to share with my students as well as my colleagues. I can not wait to see you in Bangkok.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36930 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36930,&quot;slug&quot;:&quot;badamkhatan-tserenchimed&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36930 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-05-at-15.10.16.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Born and raised in Ulaanbaatar, Badamkhatan currently studying international business and trade at Ming-Chuan University in Taiwan. She was discovered passion for international human rights while attending the National University of Mongolia. During her first year at National University of Mongolia, Badamkhatan was avid member of debate club which discussion on human rights. She developed interest in environment and sustainable lifestyle when she was living in Taiwan. When she is not in school and when she is not preoccupied by other activities, Badamkhatan prefers to spending her time to participating in beach cleaning up in the local areas.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33711 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33711,&quot;slug&quot;:&quot;vimal-raj-vivekanandah&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33711 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-05-at-15.09.38-1.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hi,I’m Vimal Vivekanandah from Malaysia.Currently pursuing in Bachelor of Social Science in Psychology at The National University of Malaysia.This is my first time to be part of Committee for Model United Nations kind of conference.Before this,I was a delegate in Global Goal Model United Nations 2018 held in Malaysia.I’m being very passionate towards volunteering since 2015 and had involved myself in numerous volunteering activities.Apart from being volunteering,I dedicated myself in upholding and spread the awareness about Sustainable Development Goals (SDG 2030).I participated in few conferences,programs and events focusing SDGs.I mainly focuses in Quality Education,Zero Hunger,Gender Education,Reduced Inequalities,Responsible Productions and Consuptions and Partnership for the Goals.Volunteering is being my Everything now.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36870 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36870,&quot;slug&quot;:&quot;arslan-rana&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36870 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; make_fullwidth=&#8221;on&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-03-at-16.30.31.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Rising marketing enthusiast with a demonstrated history and proven record of generating and building relationships, managing projects and events from concept to completion. A strong marketing professional with a bachelor&#8217;s degree of Business Administration and exceptional team player and a team leader as he worked in both roles. This time he is highly motivated to be a part of AYIMUN as a Liaison Officer of Delegates, Hoping to see all Delegates at AYIMUN, See you at Bangkok!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36874 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36874,&quot;slug&quot;:&quot;muoyleng-khov&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36874 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-03-at-16.30.32.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>My name is Muoyleng Khov from Cambodia. I am a senior at two universities, International Relations at Institute of Foreign Languages, Royal University of Phnom Penh and Economics at Pannasastra University of Cambodia. Presently, I am a chair at PUCMUN 2018 in the ECOSOC committee. I am now working as a Young Water Ambassador at Center for Sustainable Water. I was once served as a delegate at PUCMUN 2017 in the UNSC
        <br>
        committee. Beside my national MUN, I was chosen to participate in 2018 Beijing International Model United Nations (BIMUN) as a delegation from Cambodia.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36872 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36872,&quot;slug&quot;:&quot;faidh-qurrotu-ainy&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36872 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-03-at-16.30.32-1.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221#8221;]
      </p>
      <p>Halo my name is Faidh Qurrotu Ainy. I’m in my last year management undergraduate student. I’d love to meet new people, get a new experience, and I’d love to learn something new. Therefore, this will be my first MUN ever in my life, my first experience be an international volunteer and also my very first experience stepping my foot in Bangkok, Thailand. And I believe this event will be challenging and also fascinating. Hope to see you soon in Bangkok!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36835 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36835,&quot;slug&quot;:&quot;muk-nattida-phuengpanit&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36835 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-02-at-10.08.13.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Muk is a funny and active girl, who lives the whole life in the heart of Thailand named Bangkok, where she was born and rised. Currently, she is studying as the third year student in Humanity and Social sciences, English major at Bansomdejchaopraya Rajabhat University
        <br>
        then she is going to graduate soonly in 2 years. Otherwise, She also does a part-time receptionist at the hostel. She is not the smartest, but she always loves to find any interesting or challenging experiences to learn and improve herself. Her motto is &#8220;just try even if it fails, luckily you tried your best&#8221;.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36833 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36833,&quot;slug&quot;:&quot;grace-eliana-wibisono&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36833 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-02-at-09.53.30.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Greetings! My name is Grace Eliana Wibisono and I am one of the committees for AYIMUN 2018. I am an
        <br>
        Indonesian who is currently in my final year as a double degree student in Thailand. I major in hotel and
        <br>
        tourism management, and have always been passionate in handling events and meeting new people
        <br>
        from different backgrounds.
        <br>
        The MUN has been one of my many interests since my high school days, but I have never gotten the
        <br>
        opportunity to experience it firsthand. This will be my first MUN experience, even though I will be
        <br>
        participating as a committee. Being able to participate in a large scale international event as this is such
        <br>
        a great opportunity for me not only for experience, but also to broaden my knowledge and connections.
        <br>
        I hope I can learn as much as I can from both delegates and my teammates.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36838 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36838,&quot;slug&quot;:&quot;sarfaraz-hussain&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36838 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-02-at-10.08.13-1.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hello, I am Sarfaraz Hussain from India as one of the committee of Asia Youth International Model United Nations 2018.I am looking forward in meeting all you delegates. The experience of attending an MUN is something which none of you will forget.Make sure you have fun and bring the most out of you in those four days .Will be looking forward to meet you guys.See you in Bangkok!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36831 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36831,&quot;slug&quot;:&quot;mol-sokvanrith&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36831 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-02-at-09.53.31.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>MOL Sokvanrith is a fresh graduate engineer, specialized on materials experiment. Beside his main activities, he tried to involve himself into the society in the form of volunteering due to he found that volunteerism is the crucial way to increase self-confidence and to overcome the public speaking fear. He was also volunteered as liaison officer during the GGMUN conference in Kuala Lumpur. This time he is delighted to assist the delegates once again during the Asia Youth International Model United Nations 2018. He thinks AYIMUN is the great international conference, to bring all delegates around the world to unfold their notion in the debate session about the relevant issue.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36829 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36829,&quot;slug&quot;:&quot;aruhvi-krishnasammy&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36829 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-02-at-09.53.30-1.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Greetings! I’m Aruhvi Krishnasammy. Proud Indian from Malaysia. Pursuing Bachelor of Economics at The National University of Malaysia.
        <br>
        Currently serving as one of the committee of Asia Youth International Model United Nations 2018. Keen on volunteering because I want to be able to contribute my share of experience and understanding to each and every individual I encounter. I hope this platform will serve well for the youths of the future to raise awareness and achieve Sustainable Development Goals (SDGs) as well as enhance understanding of different cultures and most importantly, join forces in solving current global issues. As an enthusiast in my cause, it is a great pleasure to be a part of AYIMUN 2018. Hope to see you’ll at Bangkok!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36827 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36827,&quot;slug&quot;:&quot;angga-gumilar&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36827 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-02-at-09.53.31-1.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Angga was born in Bandung, Indonesia. He recently graduated from International Relations major at Padjadjaran University and is currently working for United Nations Development Programme in Indonesia under environmental conservation project. Angga has developed himself through MUNs as delegate, chair and committee. He participated in Harvard World MUN 2016 in Rome, organizing Padjadjaran MUN 2016 and participated as delegate in the same conference in 2014. Other than MUNs, Angga enjoys travelling, listening to music and learning foreign languages. For him, it is an honor to be part of AYIMUN 2018 and he is looking forward to meeting all of you in Bangkok!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36401 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36401,&quot;slug&quot;:&quot;mohammed-naim-uddin&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36401 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/09/Mohammed-Naim-Uddin-min.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Mohammed Naim Uddin is carrying out his study on Masters of Social Science in Chittagong University and his major is &#8220;Public Administration&#8221;.
      </p>
      <p>He started his MUN journey with “Chittagong University Model United Nations Association (CUMUNA)” in 2014 as Executive member. Later on this journey was continued as &#8220;Head of Research&#8221; &amp; “Organizing Secretary” respectively in the tenure of 2016-17 &amp; 2017-18.
        <br>
        Beside that he played active role as &#8220;Chief of staffs&#8221; in CUMUN 2018, USG of “Branding &amp; Hospitality Management&#8221; in CUMUN 2017, “Branding &amp; Logistic Management” in CUMUN 2016 significantly, throughout his organizing since CUMUN 2015.
        <br>
        Earlier he participated in various MUN conferences like UNYSAB MUN&#8217;14, JMUN&#8217;15, BIMUN&#8217;15, BUPMUN&#8217;16, UNYSAB MUN&#8217;16, KiiT International MUN&#8217;16 (India), BUGMUN&#8217;16 and bagged several awards. As Dais member, he took part in various MUNs in Bangladesh.
        <br>
        In his brief, yet with positive message, he says- “Greetings to secretariat and best wishes to all delegates.”
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36397 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36397,&quot;slug&quot;:&quot;fahad-shahbaz&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36397 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/09/Fahad-Shahbaz-min.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>My name is Fahad Shahbaz, President of Youth General Assembly and while I am studying Media, I have an immense passion for public speaking and convening to confront. The urge to debate has given me a sense of individuality and has helped groom my personality. I foster for elocution, leadership, and public speaking. I strongly believe that the importance of expressing one’s opinions, irrespective of the level of diversity and the number of individuals that may be the audience to our comments, specifically in the realms of foreign policy and global diplomacy, has immense value. I would, therefore, assert that a leadership organization is not designed to merely be a forum for individuals to represent themselves, engage in rhetoric and enjoy the sites; the objective is for all participants to comprehend the intricacies of cultural realities and global dynamics amidst multilateral policy frameworks. Asia Youth International Model United Nations shall indeed be an exciting experience. I can assure each of you that the experience will truly be unparalleled. The coordination and consensus amongst the delegates represent the basic tenets of diplomacy, which is characteristic of all committees and organs of the United Nations. I would be delighted to provide as much assistance as necessary to all the delegates, both prior to and during the sessions. Step in with your democrat hats on!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-36399 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:36399,&quot;slug&quot;:&quot;astrid-nadya-rizqita&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-36399 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.15&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.15&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.15&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/09/Astrid-Nadya-Rizqita-min.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.15&#8243; text_font=&#8221;Tahoma||||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Astrid Nadya Rizqita is a final-year undergraduate student of Development Economics at Syarif Hidayatullah State Islamic University Jakarta (UIN Jakarta). She is currently serving as the research assistant at the Faculty of Economics and Business UIN Jakarta and is a part of Youth on Organization of Islamic Cooperation Indonesia (OIC Youth Indonesia). Her recent chairing experiences include serving as Co-Chairperson of Futuristic ECOSOC in TISKLMUN 2018, Co-Director of UNESCO in JAVA MUN 2018, Co-Chairperson of UNESCO in Johor MUN 2018, Co-Vice Chairperson of WTO in Asian University for Women International MUN 2018, Chaiperson of IFAD in Chittagong University MUN 2018, Vice-Chairperson of ECOFIN in BRAC University MUN 2018, and Chairperson of the 2nd Committee of the Model Meeting Simulation in the International Conference on Islamic Youth Education held by the Ministry of Youth and Sports of the Republic of Indonesia.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-35502 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:35502,&quot;slug&quot;:&quot;nhung-nguyen&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-35502 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.13.1&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.13.1&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.13.1&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/09/AYIMUN-Volunteers-2018_1-13-min.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.13.1&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hello, agent of changes. My name is Nhung Nguyen. I am a junior student majoring in Technology field at Hanoi University of Science and Technology, Vietnam. When I was a freshman, I found myself completely passionate in Extra-curricula activities that equip a series of
        <br>
        essential soft skills as well as unrivalled knowledge in different area. That is strong reason why I have grown up and nurtured dream to become a helpful socialist for future career.
      </p>
      <p>have experienced in some youth- led organizations from domestic to international one such as Students&#8217; Union, Communist of Youth Gathering and AIESEC Hanoi. I have been involved in several MUNs around ASIA worked as both delegates and committee members functions. Therefore, in this eminent mordent AYIMUN 2018, I have the audacity to make an outstanding MUN again in collaborating with other excellent volunteers and organizers.
      </p>
      <p>Additionally, I am currently running an English Speaking Club which is created an opennessive positively English environment for anyone who looking for learning, practicing, presenting and deliberating about self-development aspect. Helping, teaching and mentoring for others is what I love to do entire my life
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-35505 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:35505,&quot;slug&quot;:&quot;chahabuddeen-hayeebilang&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-35505 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.13.1&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.13.1&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.13.1&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/09/AYIMUN-Volunteers-2018_1-14-min.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.13.1&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hi everyone my name is Chahabuddeen Hayeebilang or you can call me Shihab
        <br>
        From Thailand, last semester student studying at University Utara Malaysia, Bachelor of International Affairs Management with Honors Minor in Diplomacy and International Law. Currently Vice President of Thai Students Group in Northern Malaysia 2018/2019 and the Committee Member of Thai Students Association in Malaysia. Recently I had participated several of MUN conference throughout my student life. Its a great opportunity for me to be apart of AYIMUN 2018 committee member, I will truly work with fully commitment by using my personal skills as teamwork, independence, integrity, planning and organization and social skills to make AYIMUN 2018 successfully achieve the goal. The power of youth is the future of our world and See you in Bangkok !
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33717 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33717,&quot;slug&quot;:&quot;zhanning-ma&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33717 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-02.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hi, I am Zhanning Ma from China as one of the committee of Asia Youth International Model United Nations 2018. I hope everyone, no matter what your background is, can enjoy learning from researching, communicating with a variety of people and gain what you expect to get and feel empowered throughout the whole conference. The world is waiting for you. See you in Bangkok! I&#8217;ll send you the short video soon later once my cell phone is fixed.Sorry for the inconvenience.Look forward to seeing you all.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33714 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33714,&quot;slug&quot;:&quot;yoshihiro-gurtiza&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33714 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-03.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>I’m Yoshihiro Gurtiza. Currently, I’m the Marketing and Communication Director for International Youth United (IYU), a board member of Base Advocacy of the Youth for Absolute National Integrity (BAYANI). And in the past, I was the Financial and Sponsorship Director of Speak Out, Reach Out, Project Marawi (SPROUT &#8211; Marawi), I also become the Ambassador for International MUN, Philippine MUN and Borneo MUN. I’m also a Youth Representative for Global Youth Summit. I join youth organizations like these not only to enhance my skills as a leader, but to also, in time, inspire others and create new leaders through this.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33706 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33706,&quot;slug&quot;:&quot;snezhana-krot&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33706 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-04.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>I’m a student of Minsk State Linguistic University learning English and methods of teaching languages. My hobbies are: photography, sport and volunteering. I’m interested in a lot of things, but these days I spend my hours reading articles and watching YouTube videos about Human Rights and Problems of Education. So I’m a member of Belarusian Students Union where we try to solve problems connected with violation of student’s rights. And I’m very happy to be involved in such a huge and important thing as AYIMUN, because this is something that will be the beginning of my fruitful social activity.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33704 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33704,&quot;slug&quot;:&quot;senkevich-dziyana&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33704 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; make_fullwidth=&#8221;on&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-05.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>My name is Diana. I live in Minsk, which is the capital of Belarus. I graduated from linguistic college and entered the linguistic university. At the same time, I started working as a tutor. I love my job and I would never change it. I started volunteering many years ago and I pursue this hobby since then. I try to participate in two or three projects each year. I think it’s a good way to experience something new, improve different skills and make friends. Of cause, the opportunity to be the part of something huge and important is priceless.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33702 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33702,&quot;slug&quot;:&quot;rasulov-mukhammadjon&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33702 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-09.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hello I am Rasulov Mukhammadjon from Uzbekistan as one of the committee of Asia Youth International Model United Nations 2018.
        <br>
        Being selected as a volunteer is a big pleasure for me and I also want to send all my warm wishes to all members of Asia Youth International Model United Nations 2018.
        <br>
        Being confident by myself, I am enthusiastic on challenging myself in such great events all the time. Participation in the “MUN” as a volunteer would be an honorable opportunity to submerge in an international atmosphere.
        <br>
        This is an attribute that I am going to improve my conceptual and interpersonal skills. Partaking in “MUN”, would help me to have broader world outlook. I would like to widen our heartfelt gratitude to you for organizing such a wonderful event as it gives fellow students a chance to feel what it is like to gather around the table and discuss predominant issues, crucial political decisions and their overall impacts on our world.
        <br>
        That is why, it is a huge chance for everybody to do their best and show their selves in such kind of international conference as Asia Youth International Model United Nations.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33697 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33697,&quot;slug&quot;:&quot;jessica-paola-camelo-garcia&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33697 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-10.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Jessica Paola Camelo is a student at the University of Southern Denmark.
        <br>
        She is a citizen of both Colombia and Spain currently pursuing a bachelor’s in Economics and
        <br>
        Business Administration at the University of Southern Denmark (Campus Sønderborg). Jessica is currently enrolled at Fudan University in Shanghai, China where she is currently studying one semester abroad. Her international experience of living and study in five different countries (Colombia, Spain, USA, China, and Denmark) makes her a great international communicator. Besides, Jessica has also been accepted to the University of Sydney Business School to the Master of Management (CEMS) which she plans to start in 2019.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33632 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33632,&quot;slug&quot;:&quot;danil-koshuev&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33632 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-01.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>
        <strong>Experience
        </strong>
        <br>
        <strong>Co-Founder and Operations Manager at “Kyrgyz Global”
        </strong>
        <br>
        Non-profit organization which aims to unite all Kyrgyz abroad and contribute altogether to Kyrgyzstan.
      </p>
      <p>
        <strong>Designer at the “The UNIST Journal”
        </strong>
        <br>
        It is the sole newspaper of UNIST run by undergraduate students.
      </p>
      <p>
        <strong>Language Instructor at “UNIST Language Education Center”
        </strong>
      </p>
      <p>
        <strong>Consultant and Public Relations Manager at “Global Consulting Partners”
        </strong>
      </p>
      <p>
        <strong>Vice President at “UNIST International Students Organization”
        </strong>
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33630 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33630,&quot;slug&quot;:&quot;christian-dave-madula&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33630 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-12.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>My name is Christian Dave L. Madula, 21 years old from the Philippines. I am a graduate of Bachelor of Arts in Communication and currently working as a graphic designer. I am a freelance photographer and film maker. I recently just started my YouTube channel. I like to sing (but I dont have that golden voice). The kind of person I am is that I am an outgoing person, and an open-minded. I like being with people who are fun to be with. Though sometimes I tend not to speak because I am a shy person as well but if we&#8217;re already close, I think you need to be prepared for my craziness. Fear not for I am also a man who is serious at times, I&#8217;m quite flexible with interacting to other people.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33627 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33627,&quot;slug&quot;:&quot;alipatova-mariia&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33627 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; module_alignment=&#8221;center&#8221; make_fullwidth=&#8221;on&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-06.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>My name is Mariia Alipatova. I&#8217;m student from Ukraine. I&#8217;m a found of sport. My favorite are: tennis, football and basketball My free time I always try to spend with my family and friends. We visit some new places of our city. My hobbies are: traveling (I&#8217;ve visited 15 countries) and sport. I was working as a volunteer at UEFA Champion League, when the final game between Real Madrid and Liverpool was in Kyiv in May 2018 So, that is all interesting about me. Best wishes and see you on Bangkok
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-33616 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:33616,&quot;slug&quot;:&quot;aida-b-villegas&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-33616 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.12.2&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.12.2&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12.2&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/AYIMUN-Volunteers-2018_1-11.jpg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12.2&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>I am Aida B. Villegas, I am from the province of Sirawai, Zamboanga Del Norte, Philippines. Currently staying in Manila, Philippines. 
        <span style="font-weight: 400;">
          <br>
        </span>
        <span style="font-weight: 400;">I graduated in Mindanao State University Main Campus Marawi City with a degree of Bachelor of Science in Tourism. 
        </span>
        <span style="font-weight: 400;">
          <br>
        </span>
        <span style="font-weight: 400;">I am working at Asiana Airlines as Ground Crew. 
        </span>
        <span style="font-weight: 400;">My main responsibility is to help the passengers in check in, check their travel documents, check in baggage, and let them proceed to the next step for Immigration and so wait for their boarding time. 
        </span>
        <span style="font-weight: 400;">
          <br>
        </span>
        <span style="font-weight: 400;">The goal is to have a smooth and happy travel. I believe that safety on air starts on ground. 
        </span>
      </p>
      <p>
        <span style="font-weight: 400;">I was also Customer Service Agent for Microsoft in Convergys Philippines. 
        </span>
        <span style="font-weight: 400;">I was assigned in outbound team. My goal was to help the customers in their issues with Microsoft. I also do troubleshooting. 
        </span>
        <span style="font-weight: 400;">During my college days I was a member of Protocol Committee in our University, we assist the VIP, VVIP, Meet and Assist. 
        </span>
        <span style="font-weight: 400;">I was also member of TRM 53 Event Management Team we organized different events like Wedding, social gathering, Team builiding, and birthday parties. 
        </span>
        <span style="font-weight: 400;">I was also a college officer for two years. I participated in semi academic and public organization in the university. 
        </span>
        <span style="font-weight: 400;">I was also a tour guide for the Aga Khan Museum in Mindanao State University. 
        </span>
        <span style="font-weight: 400;">With the services I provided in the university I was awareded as College Service Awardee and Best in Tour Guiding. 
        </span>
        <span style="font-weight: 400;">I was a member of Bangon Marawi Volunteer we distribute relief goods and clothings to those community who we&#8217;re affected by Marawi siege. 
        </span>
        <span style="font-weight: 400;">I am a time conscious person, out going and conversational. 
        </span>
        <span style="font-weight: 400;">I always spend my time talking with my friends during spare time. 
        </span>
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-28808 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:28808,&quot;slug&quot;:&quot;natalia-ayu-rizki-asti&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-28808 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.12&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12&#8243; module_alignment=&#8221;center&#8221; make_fullwidth=&#8221;on&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12&#8243; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-13-at-16.23.32.jpeg&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Natalia Ayu is a senior student in Faculty of Law, Universitas Sebelas Maret, Indonesia. Her excitement in Model UN started since late 2016. Therefore she has joined multiple MUN conferences, be it local or international. She is now the President of International Law Community in Faculty of Law UNS. She believes that Model UN is a chance for young people to convey their thoughts regarding worldwide issue. Despite her arguably intimidating facade, she is more than happy to have a long talk with delegates wishing to obtain any feedback, clarify any doubts or anything at all.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-28771 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:28771,&quot;slug&quot;:&quot;md-shakhawat-hossain&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-28771 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.12&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_row _builder_version=&#8221;3.12&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.12&#8243; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-13-at-16.23.31.jpeg&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.12&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>MD. Shakhawat Hossain known as Shuvoraj, originally from Bangladesh is now a student of Computer &amp; Information Science at University of Macau. He started to engage in MUN &amp; MEU (Model European Union) through Macao Model United Nations Association (MUNA) and Jean Monnet Programme under European Union Academic Program (EUAP).
      </p>
      <p>Since then, he has joined several MUNs, MEUs and Model European Commissions both as delegate and Chair at home and abroad such as MMEU in Macao, GGMUN in Malaysia, AWMUN in South Korea, Asia Pacific MUN in Australia. In addition, he has become a part of Public Speaking U-Team and Toastmaster International. He is also a member of AIESEC China and has joined Global Volunteer Program in Middle East as part of taking action towards Sustainable Development Goals (SDGs). At present, he is acting as the Vice President of International Students’ Association (ISA) and Vice Minister of Model United Nations Association in Macao.
      </p>
      <p>He acted as SMR (Social Media Reporter) in African University week 2017 and an active Campus News Reporter &amp; Photo Journalist at a same time. In addition, he is the current Global Youth Ambassador of an UK based non governmental organization ‘Their World’ and also the United Nations sponsored &#8216;Hult Prize&#8217; Campus Director as well.
      </p>
      <p>He is excited to meet all the delegates in Legal Committee and committed to learn and share from each other through AYIMUN platform.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-22909 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:22909,&quot;slug&quot;:&quot;raditya-rahim&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-22909 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.41.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hi there! I am your one and only chair Raditya S. Rahim from Indonesia, will serve you as one of Board of Dais of Asia Youth International Model United Nations 2018. I will be your IMO’s chair in this year AYIMUN. This would be the end of my MUN journey, therefore, bring your “A” game but still enjoy this MUN and expand your connection and knowledge with me as well as with the others! REMEMBER, if this is your first time coming to Thailand–do not forget to enjoy the country and cherish every moment! But if is not, then tell me what should I do here in Thailand. See you in Bangkok, cheers!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-22966 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:22966,&quot;slug&quot;:&quot;syed-muhammad-salman-haider&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-22966 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.40.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Syed Muhammad Salman is a marketing student from Pakistan but with an interest in world politics and global issues. Salman has been a part of several national and international Model United Nations conferences – both as chair and delegate – and won many awards. Salman was also present at AYIMUN’2017 in Kuala Lumpur and he most recently won the award for Best Position Paper at FWWMUN’2018, United Nations New York. Salman also actively participates in social welfare activities and has tirelessly worked for refugees all over the world. Salman is also a writer at My Voice Unheard and several Pakistan based blogs.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-22958 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:22958,&quot;slug&quot;:&quot;udit-malik&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-22958 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.48-1.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Udit Malik is from Amity University,India he is an avid right activist and a blogger who want to serve his nation.He is currently pursuing his undergraduate degree in law majoring in criminology and have Diploma from Institute of United Nations Studies.
      </p>
      <p>A politics aficionado, his interest and knowledge in World Affairs is a force to reckon with.As said about the power of his words, he&#8217;s an amazing orator with exceptional skills of conviction and perseverance. Indulge in a conversation and you might seek something which you might not have heard.He believes in cracking a different approach to every possible ordinary thing that a person does. He has been part of over many national and International Model United Nations and Youth Parliaments across the world.With all the experience in hand, he expects that he&#8217;d not let a single person leave empty handed in AYIMUN.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-22955 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:22955,&quot;slug&quot;:&quot;nur-robiahtul-sofia&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-22955 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.47.59.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>My name is Nur Robiahtul Sofia and I am studying at University of Malaya, Kuala Lumpur, Malaysia. I am taking Media Studies as I really want to pursue my career in public relation or a diplomat in the future. Currently, I am the president of Johor Student Leaders Council Alumni (JSLCA) and the representative for Iskandar Malaysia Youth Committer in education sector. I first started MUN when I attended Taylor&#8217;s Model United Nations (TAYMUN) and received &#8220;The Most Outstanding Delegate&#8221; in UNHCR as the delegate of Afghanistan. Then, I continue my journey in MUN as I realised my passion in MUN is really high and JSLCA started to organise our own MUN which called JOHORMUN where the dais at that time were all our JSLC members including me. After received a lot of positive feedback, we decided to have dais from all over the world and have around 200 delegates as a starting point to make JOHORMUN well known among the MUN-ers.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-22950 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:22950,&quot;slug&quot;:&quot;anggoro-galuh-pandhito&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-22950 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_px=&#8221;1081px&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.45.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Greetings!! My name is Anggoro Galuh Pandhito and it is my immense pleasure to serve you as you Chair in UNESCO. I am currently a senior student in Universitas Jenderal Achmad Yani, majoring in International Relations whilst enjoying my minor in International Security. I have involved myself in different position in Model UN since 2012 and have been attending 45 conferences both Nationally and Internationally. Coffee and Books are my best friend and the best companion while doing my favourite thing, Travel. Do approach me when you see me, I’m not a scary person despite my size. See you in AYIMUN 2018!!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-22940 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:22940,&quot;slug&quot;:&quot;mohammad-huzaifa-fazal&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-22940 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.36.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hey, I am Huzaifa. I am Indian but have been living in Malaysia for the past 9 years. I am pursuing an undergraduate degree in Engineering. Besides academics, I enjoy casual debates with friends and family as well as debating and MUN as a sport. On the physical side, I play quite a few sports, with basketball being up there (something you will realise when you see my height haha). I spend lots of time listening to music, watching all sorts of movies and playing video games. When all&#8217;s said and done, I try to enjoy the little things in life. I believe the ultimate goal should always be where one is happy; settling for anything less should not be an option. Always on the lookout for the next big thing.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-22969 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:22969,&quot;slug&quot;:&quot;john-caleb-roxas&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-22969 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.44.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Caleb is an Agricultural Economics undergraduate student at University of the Philippines Los Baños who is deeply engaged in the practice of MUN and the pursuit of the SDGs.
      </p>
      <p>He is the Founding Executive Director of the UP Sustainable Development Initiative, a student organization that contributes to the global effort of achieving the UN SDGs. Furthermore, he works with the UP Model United Nations, a student organization that promotes and advances education about the United Nations and its global work, and he is the Deputy Secretary General of the 2019 session of its annual flagship activity, the Southern Luzon Model United Nations Conference 2019 in Los Baños, Philippines.
      </p>
      <p>Outside aforementioned pursuits, he is a youth engagement advocate and aspiring public speaker
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-22982 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:22982,&quot;slug&quot;:&quot;lymeng-chi&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-22982 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.43.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>I’m Chi Lymeng from Cambodia. I finished bachelor degree of International Studies, specializing in International Economics at Royal University of Phnom Penh. Professionally, I am working at Transparency International Cambodia on good governance and capacity strengthening program. Regarding to MUN, beside classroom level, this year I attended two UNSC simulations by PUC Model United Nation and Phnom Penh Model United Nations (PPMUN), where I represented Republic of France and dual delegate acting as chair and representing Republic of Poland on the topic of the current situation in Central African Republic and Syria respectively. Together, let’s make AYIMUN the best!
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-23083 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:23083,&quot;slug&quot;:&quot;musfirah-nadeem&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-23083 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.51.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hailing from Pakistan, Musfirah Nadeem is no stranger in the MUNing circuit and has the absolute privilege of serving as the Under Secretary General of Asia Youth International MUN. She has been an exceptional MUNer since her junior year and currently holds the honour of being the youngest MUNer in the circuit. Aside from MUNing, she has always been a formidable debater and has bagged multiple awards in her high school year. Being a big supporter of Sustainable Development Goals, she is currently working in a number of organisations with the aim of a peaceful and sustainable world one day. Her missions and our theme, &#8220;Pursuing Global Peace through Diplomacy&#8221; connects so well and we are really confident that she will leave this conference with nothing else but success.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-23175 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:23175,&quot;slug&quot;:&quot;arbaz-ahmed-yar-khan&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-23175 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; make_fullwidth=&#8221;on&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.47.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>I, Arbaz Yar Khan from Pakistan will be assisting you as Co-chair of WHO in AYIMUN.
        <br>
        I hope my delegates will give their best in the committee snd I hope that we will learn a lot from each other. See you at Bangkok.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-23193 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:23193,&quot;slug&quot;:&quot;hassaan-sudozai&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-23193 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; custom_padding_last_edited=&#8221;off|&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.51-1.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>I am Hassaan Sudozai, Business Administration Student of Karachi Institute of Economics and Technology. A Public Speaker and Youth Activist from Karachi, Pakistan. Serving as Marketing Executive of Youth Innovative Dynamic Society and Editor in Chief of راوی Publications. Apart from all this, I have a part of MUN circuit since last 3 years, have attended more or less around 35 Model UN Conference of College, University and International Level as Delegate, Chair and Organizing Body Member. I started my MUN career in 2015, with my first MUN conference at home institute KIETMUN as a delegate in Intelligence Summit, a specialized council and after this first attempt I never looked back. I participated in 11 MUN Conference as Delegate winning 9 of them with different awards including an international MUN conference. With this I also have chaired around 20 council of different mandates in MUN conferences including international MUN. For me MUN are all about learning, socializing and showing the talent a person is blessed with. Looking forward to share my experience and knowledge.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-23185 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:23185,&quot;slug&quot;:&quot;abhishek-poddar&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-23185 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.46.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hi, I am Abhishek Poddar from India as one of the members of the Board of Dias of Asia Youth International Model United Nations 2018. I will be assisting you as your Chair in Food and Agriculture Organization (FAO) in AYIMUN 18’. I am really looking forward to meeting each one of you and sharing a committee where we not only deliberate on the Agendas but acknowledge and anatomize the need of our society and find a solution for optimum utilization of resources for the betterment of the international community. I hope that the MUN not only stands as a ground of debate and deliberation but also a space of open interaction where every individual can perform to their fullest. See you in Bangkok.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-23181 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:23181,&quot;slug&quot;:&quot;nur-faizah-mas-mohd-khalik&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-23181 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.42.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Hailing from Singapore, Faizah is no stranger to the regional Model UN scene. In addition to having been the Deputy Secretary-General of Finance of the reputed Malaysian National Model United Nations for three years, she has also chaired in conferences as far as Bangladesh.
      </p>
      <p>Outside of MUN and her professional pursuits, Faizah has a keen interest in the Health Sciences. She is also a classical musician and an aviation enthusiast and loves everything about flying airplanes and drones.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-23198 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:23198,&quot;slug&quot;:&quot;sayan-mukherjee&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-23198 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221; custom_padding=&#8221;0px|0px|0px|0px&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.48.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_font_size=&#8221;15px&#8221; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>I&#8217;m Sayan Mukherjee and I&#8217;m from the City of Joy-Kolkata,India. I started my Model United Nations journey back during 2016 and there is no look back after that. I have done MUNs all over India and mostly around the Eastern India. I&#8217;m also a prominent member of BESC Assembly of Nations. I have been a BoD Memeber at India and Bangladesh MUN Conferences.
        <br>
        I&#8217;m also an avid Football lover and Footballer who supports Barcelona and Liverpool. I love travelling a lot and clicking pictures which turn into memories.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-22990 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:22990,&quot;slug&quot;:&quot;sakhir-ali-qureshi-2&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_left&quot;:false,&quot;position_bottom&quot;:false,&quot;position_right&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-22990 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>[et_pb_section bb_built=&#8221;1&#8243; specialty=&#8221;off&#8221; _builder_version=&#8221;3.9&#8243; custom_padding=&#8221;0px|0px|0px|0px&#8221; background_color=&#8221;rgba(255,255,255,0)&#8221;][et_pb_row _builder_version=&#8221;3.9&#8243; module_alignment=&#8221;center&#8221; use_custom_width=&#8221;on&#8221; width_unit=&#8221;off&#8221; custom_width_percent=&#8221;100%&#8221; use_custom_gutter=&#8221;on&#8221; gutter_width=&#8221;2&#8243; make_equal=&#8221;on&#8221;][et_pb_column type=&#8221;1_2&#8243;][et_pb_image _builder_version=&#8221;3.9&#8243; src=&#8221;https://modelunitednation.org/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-02-at-14.49.50.jpeg&#8221; align=&#8221;center&#8221; force_fullwidth=&#8221;on&#8221; /][/et_pb_column][et_pb_column type=&#8221;1_2&#8243;][et_pb_text _builder_version=&#8221;3.9&#8243; text_font=&#8221;Tahoma|500|||||||&#8221; text_text_color=&#8221;#000000&#8243; text_letter_spacing=&#8221;1px&#8221; text_orientation=&#8221;justified&#8221;]
      </p>
      <p>Sakhir is a freshman at University of London pursuing a degree in law and has the absolute privilege of serving as the Secretary General of Asia Youth International MUN 2018. Being an avid MUNer, an outstanding debater and and an excellent event manager, we are confident that he will leave no stones unturned to make this year&#8217;s conference a one-of-a-kind-experience. He has previously served as the President of Sadiq Model United Nations, as an Under Secretary General at Asia World MUN and as the Chairman of Bahawalpur Art &amp;amp; Science Olympiad. Aside from his managing experience, he has been to a number of MUN conferences as delegate where he bagged multiple awards, furthermore, he has served as a chair at multiple international conferences such as Global Goals MUN, Kuala Lumpur. He is currently serving as the Vice President of a Youth Organisation in Pakistan namely Youth General Assembly. We believe, this conference is bound to be a success under his leadership.
      </p>
      <hr style="border-width: 3px;" color=#6BB6E5>
      <p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-8522 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:8522,&quot;slug&quot;:&quot;sakhir-ali-qureshi&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-8522 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>SAKHIR ALI QURESHI
        </strong>
      </h2>
      <p>
        <a href=https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi.jpg>
          <img loading=lazy class="wp-image-8510 size-medium aligncenter" src=https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi-300x300.jpg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi-300x300.jpg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi-400x400.jpg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi-100x100.jpg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi-510x510.jpg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi-150x150.jpg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi-768x769.jpg 768w, https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi-1024x1024.jpg 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi-1080x1081.jpg 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/sakhir-ali-qureshi.jpg 900w" sizes="(max-width: 300px) 100vw, 300px">
        </a>
      </p>
      <h4>
        <strong>EXPERIENCE
        </strong>
      </h4>
      <p>
        <strong>International Conference on culture, Pakistan
        </strong>
        <br>
        Member of executive committee October 2017
        <br>
        <strong>Bahawalpur Art &amp; Science Olympiad
        </strong>
        <br>
        <strong>Founder and chairperson
        </strong>
        <br>
        Arranged this platform for art and science enthusiasts in
        <br>
        collaboration with Sadiq Public School. It included a total of 8
        <br>
        events ranging from art, science, public speaking and sports.
        <br>
        the profit was donated to charity. August 2017
        <br>
        <strong>Chair Indo-Pak summit at Sadiq MUN’16
        </strong>
        <br>
        <strong>Chair SPECPOL at GAMMUN’16
        </strong>
        <br>
        <strong>Chair DISEC at SAMMUN ‘16
        </strong>
        <br>
        <strong>Chair UNHRC at MOMUN’16
        </strong>
        <br>
        <strong>Co-Chair UNHRC at Sadiq MUN’15
        </strong>
      </p>
      <h4>
        <strong>EDUCATION
        </strong>
      </h4>
      <p>
        <strong>LLB(Hons) University of London International Programme
        </strong>
        <br>
        University College Lahore 2017- Present
        <br>
        <strong>A levels
        </strong>
        <br>
        Sadiq Public School, BWP 2015-2017
        <br>
        <strong>O levels
        </strong>
        <br>
        Beacon House School System, BWP 2012-2015
      </p>
      <h4>
        <strong>AWARDS
        </strong>
      </h4>
      <p>
        <strong>MUNIC
        </strong>
        <br>
        Best delegate award 2016
        <br>
        <strong>International Affairs Conference, Pakistan
        </strong>
        <br>
        Best Delegate Award 2016
        <br>
        <strong>BSMUN
        </strong>
        <br>
        Best delegate award 2016
      </p>
      <p>
        <strong>ACMUN
        </strong>
        <br>
        Honorable mention 2015
        <br>
        <strong>AUCMUN
        </strong>
        <br>
        Best delegate award 2015
        <br>
        <strong>JHMUN
        </strong>
        <br>
        Best delegate award 2015
        <br>
        <strong>LOMUN
        </strong>
        <br>
        best delegate award 2015
        <br>
        <strong>SAMUN
        </strong>
        <br>
        best delegate award 2014
      </p>
      <h4>
        <strong>MEMBERSHIPS
        </strong>
      </h4>
      <p>
        <strong>Youth General Assembly | 2017-Present
        </strong>
        <br>
        President of Lahore chapter
        <br>
        www.ygapakistan.org
        <br>
        <strong>Cholistan Development Authority
        </strong>
        <br>
        NGO that works for welfare of Cholistan 2014- Present
        <br>
        <strong>Sadiq MUN society
        </strong>
        <br>
        President 2016-2017
        <br>
        <strong>Sadiq Science society
        </strong>
        <br>
        Vice president 2016-2017
        <br>
        <strong>Sadiq character building society
        </strong>
        <br>
        Vice president 2016-2017
        <br>
        <strong>Sadiq photographic society
        </strong>
        <br>
        President 2016-2017
        <br>
        <strong>Sadiq Adventure Club
        </strong>
        <br>
        Leader 2016-2017
        <br>
        <strong>Sadiq debating society
        </strong>
        <br>
        Secretary 2016-2017
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-8503 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:8503,&quot;slug&quot;:&quot;hassaan-nasim&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-8503 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h1 style="text-align: center;">
        <strong>Hassaan Nasim
        </strong>
      </h1>
      <p>
        <a href=https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim.jpg>
          <img loading=lazy class="wp-image-8350 size-medium aligncenter" src=https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim-300x300.jpg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim-300x300.jpg 300w, https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim-400x400.jpg 400w, https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim-100x100.jpg 100w, https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim-510x510.jpg 510w, https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim-150x150.jpg 150w, https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim-768x768.jpg 768w, https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim-1024x1024.jpg 1024w, https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim-1080x1080.jpg 1080w, https://modelunitednation.org/wp-content/uploads/2017/10/Hassaan-Nasim.jpg 900w" sizes="(max-width: 300px) 100vw, 300px">
        </a>
      </p>
      <h3>
        <strong>MUN And Debating Career:
        </strong>
      </h3>
      <p>All Pakistan Inter college bilingual Declamation Contest Palandri Cadet College 2012
        <br>
        All Pakistan Cadet College Declamation Contest Cadet College Sanghar 2012
        <br>
        Celebrating Urdu (Declamation Contest) PAF Academy Risalpure 2012
        <br>
        Express Career and Education Expo Karachi 2016
      </p>
      <p>&nbsp;
      </p>
      <table width=664>
        <tbody>
          <tr>
            <td colspan=4 width=664>
              <strong>MUN EXPERIENCE
              </strong>
            </td>
          </tr>
          <tr>
            <td width=158>
              <strong>CONFERENCE
              </strong>
            </td>
            <td width=158>
              <strong>COMMITTEE
              </strong>
            </td>
            <td width=189>
              <strong>PARTICIPATE AS / AWARDs
              </strong>
            </td>
          </tr>
          <tr>
            <td width=158>PAF-KIET MUM 15
            </td>
            <td width=158>Intelligence Submit
            </td>
            <td width=189>&#8211;
            </td>
          </tr>
          <tr>
            <td width=158>BEACONMUN 15
            </td>
            <td width=158>DISEC
            </td>
            <td width=189>Honorable Mention
            </td>
          </tr>
          <tr>
            <td width=158>FURIC MUN
            </td>
            <td width=158>Pakistan National Assembly
            </td>
            <td width=189>Best Delegate
            </td>
          </tr>
          <tr>
            <td width=158>Hamdard University MUN 16
            </td>
            <td width=158>All  PARTIES NEGOCIATIION CABINET
            </td>
            <td width=189>BEST DELEGATE
            </td>
          </tr>
          <tr>
            <td width=158>Hyderabad-MUN-III
            </td>
            <td width=158>UNSDS
            </td>
            <td width=189>Honorable Mention
            </td>
          </tr>
          <tr>
            <td width=158>IQRA UNVERSITY MUN 16
            </td>
            <td width=158>Pakistan National Assembly
            </td>
            <td width=189>Best Delegate
            </td>
          </tr>
          <tr>
            <td width=158>Whales MUN 16-17
            </td>
            <td width=158>Pakistan National Assembly
            </td>
            <td width=189>Honorable Mention
            </td>
          </tr>
          <tr>
            <td width=158>NUST International MUN 17
            </td>
            <td width=158>ECOFIN
            </td>
            <td width=189>Honorable Mention
            </td>
          </tr>
          <tr>
            <td width=158>IUMUN’17
            </td>
            <td width=158>Majlis-e-Shora
            </td>
            <td width=189>Best Delegate
            </td>
          </tr>
        </tbody>
      </table>
      <p>&nbsp;
      </p>
      <p>
        <strong>
          <u>Chairing Experience:
          </u>
        </strong>
      </p>
      <table width=506>
        <tbody>
          <tr>
            <td width=158>BEACON MUN 16
            </td>
            <td width=158>Nacros (specialize committee)
            </td>
            <td width=189>Assistant Committee Director
            </td>
          </tr>
          <tr>
            <td width=158>PAF-KIET MUN 16
            </td>
            <td width=158>Pakistan Supreme Court
            </td>
            <td width=189>Assistant Committee Director
            </td>
          </tr>
          <tr>
            <td width=158>Model UN Sukkur 17
            </td>
            <td width=158>Pakistan High Level Meeting
            </td>
            <td width=189>Assistant Committee Director
            </td>
          </tr>
          <tr>
            <td width=158>Habib Public School MUN 17
            </td>
            <td width=158>Pakistan National Assembly
            </td>
            <td width=189>Assistant Committee Director
            </td>
          </tr>
          <tr>
            <td width=158>BEHRIA COLLEGE KARACHI-MUN 17
            </td>
            <td width=158>Indo-Pak Submit
            </td>
            <td width=189> Committee Director
            </td>
          </tr>
          <tr>
            <td width=158>GUARDS PUBLIC COLLEGE-MUN 17
            </td>
            <td width=158>Joint Provisional Cabinet
            </td>
            <td width=189>Assistant Committee Director
            </td>
          </tr>
          <tr>
            <td width=158>NASRA-MUN
            </td>
            <td width=158>Special Commission Turkey
            </td>
            <td width=189>Committee Director
            </td>
          </tr>
          <tr>
            <td width=158>Kiet Crisis and Negotiations Cabinet 16
            </td>
            <td width=158>&nbsp;
        </p>
          <p>&#8211;
        </td>
        <td width=189>Secretary General
        </td>
        </tr>
      <tr>
        <td width=158>Hamdard University MUN17
        </td>
        <td width=158>UN-DP
        </td>
        <td width=189>Committee Director
        </td>
      </tr>
      <tr>
        <td width=158>DHA-Suffa University MUN17
        </td>
        <td width=158>UN-Women
        </td>
        <td width=189>Committee Director
        </td>
      </tr>
      <tr>
        <td width=158>SEHWAN MUN17
        </td>
        <td width=158>Pakistan National Aassembly
        </td>
        <td width=189>Committee Director
        </td>
      </tr>
      <tr>
        <td width=158>TRSMUN 17
        </td>
        <td width=158>PNA
        </td>
        <td width=189>Committee Director
        </td>
      </tr>
      <tr>
        <td width=158>BeaconMUN17
        </td>
        <td width=158>Oscars
        </td>
        <td width=189>Committee Director
        </td>
      </tr>
      </tbody>
    </table>
  <p>&nbsp;
  </p>
  <p>
    <strong>
      <u>International MUN:
      </u>
    </strong>
  </p>
  <table width=506>
    <tbody>
      <tr>
        <td width=158>
          <h3>NUST International MUN 17
          </h3>
        </td>
        <td width=158>
          <h3>ECOFIN
          </h3>
        </td>
        <td width=189>
          <h3>Honorable Mention
          </h3>
        </td>
      </tr>
      <tr>
        <td width=158>GAIC-MUN17
        </td>
        <td width=158>UN-EP
        </td>
        <td width=189>Deputy Chair (appointed)
        </td>
      </tr>
      <tr>
        <td width=158>UCIMUN
        </td>
        <td width=158>Crisis Committee
        </td>
        <td width=189>Committee Director
        </td>
      </tr>
    </tbody>
  </table>
  <p>
    <u> 
    </u>
  </p>
  <p>
    <u>Organizing Experience: 
    </u>
  </p>
  <p>TRS-MUN 2017 Badin – Conference Manager
  </p>
  <p>KIET Crisis and Negotiation Cabinet- Secretary General
  </p>
  <p>KIETMUN 16- Organizing Committee Member
  </p>
  <p>MUNSK 16- Organizing Committee Member
  </p>
  <p>KIETMUN 17- Deputy Director General
  </p>
</div>
<button type=button class="pum-close popmake-close" aria-label=Close>
  &#215; 
</button>
</div>
</div>
<div id=pum-8281 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:8281,&quot;slug&quot;:&quot;muhamad-rizki-nugraha-darma-nagara&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-8281 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Muhamad Rizki Nugraha Darma Nagara
        </strong>
      </h2>
      <p>
        <a href=https://modelunitednation.org/wp-content/uploads/2017/08/afif-ridwan.jpg>
          <img loading=lazy class="aligncenter wp-image-8280 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/afif-ridwan-300x300.jpg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/afif-ridwan-300x300.jpg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/afif-ridwan-400x400.jpg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/afif-ridwan-100x100.jpg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/afif-ridwan-150x150.jpg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/afif-ridwan.jpg 414w" sizes="(max-width: 300px) 100vw, 300px">
        </a>
      </p>
      <h3>
        <strong>Education and Qualifications
        </strong>
      </h3>
      <p>
        <strong>2015-2017
        </strong>
      </p>
      <p>Senior Student of President University
      </p>
      <p>Majoring in International Relations
      </p>
      <p>Cumulative GPA: 3.8
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>Activities and Awards
        </strong>
      </h2>
      <p>
        <strong>President University Model United Nations (PUMUN) 2015-2017 
        </strong>
      </p>
      <p>&nbsp;
      </p>
      <p>
        <strong>Member of Conference
        </strong>
      </p>
      <p>Responsibilities included: responsible for all substantial matters of Model United Nations(MUN).
      </p>
      <p>&nbsp;
      </p>
      <p>
        <strong>International Association of Students in 
        </strong>
        <strong>Economic and Commercial Sciences 
        </strong>
        <strong>(AIESEC) Official Expansion of 
        </strong>
        <strong>President University 2015-2016
        </strong>
        <br>
        Manager of Talent Development in Talent Management Division Responsibilities included: ensure the development of AIESEC’s members by giving seminars and trainings.
      </p>
      <p>&nbsp;
      </p>
      <p>
        <strong>IM3 Ooredoo Brand Ambassador and 
        </strong>
        <strong>Social Media Influencer 2017
        </strong>
      </p>
      <p>
        <strong>International Youth Academy (IYA)
        </strong>
        <br>
        <strong>Ambassador 2017
        </strong>
      </p>
      <p>
        <strong>Chair of President Model United Nations
        </strong>
        <br>
        <strong>ECOFIN 2017
        </strong>
      </p>
      <p>
        <strong>Top 10 th Best Speaker and Third Winner
        </strong>
        <br>
        <strong>of International Humanitarian Law
        </strong>
        <br>
        <strong>Debate Indonesia 2017
        </strong>
      </p>
      <p>
        <strong>Verbal Commendation of SE- MUN PGD
        </strong>
        <br>
        <strong>2017
        </strong>
      </p>
      <p>
        <strong>Chair of President Model United Nations
        </strong>
        <br>
        <strong>ECOSOC 2016
        </strong>
      </p>
      <p>
        <strong>The Most Outstanding Delegate in the
        </strong>
        <br>
        <strong>council of UN High Commissioner for
        </strong>
        <br>
        <strong>Refugees Committee 2016 – ALSA
        </strong>
        <br>
        <strong>UNPAD MUN
        </strong>
      </p>
      <p>
        <strong>The Most Outstanding Delegate in the
        </strong>
        <br>
        <strong>council of UN High Commissioner for
        </strong>
        <br>
        <strong>Refugees Committee 2016 – Bandung
        </strong>
        <br>
        <strong>MUN
        </strong>
      </p>
      <p>
        <strong>Best Delegate in ASEAN Youth General
        </strong>
        <br>
        <strong>Forum MUN &#8211; UN General Assembly
        </strong>
        <br>
        <strong>Committee 2016
        </strong>
      </p>
      <p>
        <strong>Best Delegate in ASEAN Youth General
        </strong>
        <br>
        <strong>Forum MUN – FGD AEC 2016
        </strong>
      </p>
      <p>
        <strong>Top 10 Duta Generasi Berencana
        </strong>
        <br>
        <strong>Nasional and the Winner of Duta
        </strong>
        <br>
        <strong>Generasi Berencana (GenRe) BKKBN
        </strong>
        <br>
        <strong>Provinsi Jawa Barat 2016
        </strong>
        <br>
        <strong>(The Winner of West Java Generation
        </strong>
        <br>
        <strong>Planning Ambassador 2016)
        </strong>
      </p>
      <p>Responsibilities included: Socialize and promote the GenRe program among Youth and Students.
      </p>
      <p>
        <strong>The Runner Up of Mister Teen Indonesia
        </strong>
        <br>
        <strong>West Java 2016
        </strong>
      </p>
      <p>Responsibilities included: stimulate the teens to develop their talents and interests in various fields and promote the West Java’s tourism.
      </p>
      <p>J
        <strong>ajaka Wakil 1 Jawa Barat
        </strong>
        <br>
        <strong>(The Runner Up of West Java Tourism
        </strong>
        <br>
        <strong>Ambassadors for Male 2015)
        </strong>
      </p>
      <p>Responsibilities: help the government in promoting and introducing art, culture, and tourism of West Java to society especially West Java’s youth.
      </p>
      <p>
        <strong>Best Delegate in UI MUN CLUB GGA &#8211;
        </strong>
        <br>
        <strong>UN High Commissioner of Refugee
        </strong>
        <br>
        <strong>Committee 2015
        </strong>
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-7904 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:7904,&quot;slug&quot;:&quot;register&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-7904 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>
        <a href=https://modelunitednation.org/registration/>
        <img loading=lazy class="alignnone wp-image-7905 size-full" src=https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12.jpeg alt width=1280 height=1280 srcset="https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12.jpeg 900w, https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12-400x400.jpeg 400w, https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12-100x100.jpeg 100w, https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12-510x510.jpeg 510w, https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12-150x150.jpeg 150w, https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12-300x300.jpeg 300w, https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12-768x768.jpeg 768w, https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12-1024x1024.jpeg 1024w, https://modelunitednation.org/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-10-at-11.27.12-1080x1080.jpeg 1080w" sizes="(max-width: 1280px) 100vw, 1280px">
      </a>
    </p>
</div>
<button type=button class="pum-close popmake-close" aria-label=Close>
  &#215; 
</button>
</div>
</div>
<div id=pum-3757 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:3757,&quot;slug&quot;:&quot;asean-regional-forum&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-3757 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p align=justify>
        <img loading=lazy class="aligncenter wp-image-248 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-06-300x300.png alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-06-300x300.png 300w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-06-400x400.png 400w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-06-100x100.png 100w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-06-510x510.png 510w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-06-150x150.png 150w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-06-768x768.png 768w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-06-1024x1024.png 1024w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-06-1080x1080.png 1080w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <h2 style="text-align: center;" align="justify">
        <span style="color: #808080;">
          <strong>ASEAN REGIONAL FORUM (ARF)
          </strong>
        </span>
      </h2>
      <p align=justify>
        <strong>Topic A : Combating Terrorism and Extremism in Southeast Asia.
        </strong>
        <br>
        <strong>Topic B : Addressing Rohingya Refugees in Southeast Asia
        </strong>
      </p>
      <p style="text-align: justify;" align=justify>ASEAN Regional Forum (ARF) is a forum established by ASEAN in 1994 for open dialogue and consultation on regional and security issues, to discuss and reconcile the differing views between ARF participants in order to reduce risk to security. The ARF recognizes that the concept of comprehensive security includes not only military aspects but also political, economic, social and other issues. ARF participants Comprise all 10 ASEAN member countries, 10 ASEAN Dialogue Partners (USA, Australia, Canada, China, India, Japan, Republic of Korea, Russia, New Zealand, and European Union), Papua New Guinea, Mongolia, Democratic People’s Republic of Korea, Pakistan, East Timor, Bangladesh and Sri lanka. In this council, there will be two topics that will arise as the topic of this council, the first topic will discuss about the terrorism and extremism in Southeast Asia, and the second topic will discuss about Rohingya refuges in Southeast Asia.
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2168 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2168,&quot;slug&quot;:&quot;russell-marino-soh&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2168 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Russell Marino Soh
        </strong>
      </h2>
      <p>
        <img loading=lazy class="aligncenter wp-image-2090 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/russel-300x300.jpeg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/russel-300x300.jpeg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/russel-400x400.jpeg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/russel-100x100.jpeg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/russel-510x510.jpeg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/russel-150x150.jpeg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/russel-768x768.jpeg 768w, https://modelunitednation.org/wp-content/uploads/2017/08/russel-1024x1024.jpeg 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/russel-1080x1080.jpeg 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/russel.jpeg 900w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EDUCATION
        </strong>
      </h2>
      <p>2008 &#8211; 2013 : NUS High School of Mathematics and Science
      </p>
      <p>2016 – Present : National University of Singapore
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EXPERIENCE
        </strong>
      </h2>
      <p>2015 – Present : Legislative Assistant
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>ACCOLADES
        </strong>
      </h2>
      <p>2010 Moot Parliament Programme
        <br>
        Best Team Award; Best Written Bill (Second Place)
      </p>
      <p>2011 Singapore Microsoft Office Academic Skills Challenge
        <br>
        Champion, PowerPoint 2007 Category
      </p>
      <p>2011 Certiport Worldwide Competition on Microsoft Office
        <br>
        Finalist, PowerPoint 2007 Category
      </p>
      <p>2012 National Arts Council Heartland Haiku Competition
        <br>
        Top Prize, Senior Category
      </p>
      <p>2012 World Expo Model United Nations
        <br>
        Honorable Mention, DUMUN-UNSCOP
      </p>
      <p>2013 Berkeley Model United Nations
        <br>
        Research Award, APEC
      </p>
      <p>2013 Singapore Model United Nations
        <br>
        Best Position Paper Award, WHO
      </p>
      <p>2013 Singapore Science and Engineering Fair
        <br>
        Silver Award
      </p>
      <p>2016 Pan-Asia Model United Nations
        <br>
        Best Diplomacy Award, FAO
      </p>
      <p>2017 NTU Model United Nations
        <br>
        Best Position Paper Award, WHO
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-3759 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:3759,&quot;slug&quot;:&quot;unesco&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-3759 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p align=justify>
        <img loading=lazy class="aligncenter wp-image-246 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-01-300x219.png alt width=300 height=219 srcset="https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-01-300x219.png 300w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-01-510x372.png 510w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-01-768x561.png 768w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-01-1024x748.png 1024w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-01-1080x789.png 1080w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <h2 style="text-align: center;" align="justify">
        <strong>UNESCO
        </strong>
      </h2>
      <p align=justify>
        <strong>Topic A : Protecting Cultural Heritage
        </strong>
        <br>
        <strong>Topic B : Protection of Journalist in conflicted Area
        </strong>
      </p>
      <p align=justify>United Nations Educational, Scientific, and Cultural Organization (UNESCO) works to create the conditions for dialogue among civilizations, cultures and peoples, based upon respect for commonly shared values. It is through this dialogue that the world can achieve global visions of sustainable development encompassing observance of human rights, mutual respect and the alleviation of poverty. All of which are at the heart of UNESCO’s mission and activities. UNESCO’s mission is to contribute to the building of peace, the
        <br>
        eradication of poverty, sustainable development and intercultural dialogue through education, the sciences, culture, communication and information. Furthermore, the protection of cultural site and Journalist will be the focus of this MUN.
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-7724 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:7724,&quot;slug&quot;:&quot;press-corps&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-7724 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>
        <img loading=lazy class="size-medium wp-image-1197 aligncenter" src=https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-08-300x249.png alt width=300 height=249 srcset="https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-08-300x249.png 300w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-08-510x423.png 510w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-08-768x636.png 768w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-08-1024x849.png 1024w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-08-1080x895.png 1080w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <h2 style="text-align: center;">
        <strong>PRESS CORPS
        </strong>
      </h2>
      <p style="text-align: justify;">In Press Corps, delegates will represent remarkable news agencies from all over the world, reporting the overall events of Asia Youth International Model United Nations (AYIMUN) 2017 including the committee session and social events. Delegates will publish news fliers’ everyday which will make AYIMUN 2017 more interesting, also by promoting freedom of press. AYIMUN 2017 invites all passionate delegates to be a part of the Press Corps.
      </p>
      <p>&nbsp;
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-3765 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:3765,&quot;slug&quot;:&quot;unhrc&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-3765 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p>
        <img loading=lazy class="aligncenter wp-image-249 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-04-300x300.png alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-04-300x300.png 300w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-04-400x400.png 400w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-04-100x100.png 100w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-04-510x510.png 510w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-04-150x150.png 150w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-04-768x768.png 768w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-04-1024x1024.png 1024w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-04-1080x1080.png 1080w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <h2 style="text-align: center;">
        <strong>UNHRC
        </strong>
      </h2>
      <p>
        <strong>Topic A : Ending modern form of slavery in the 21 st Century
        </strong>
        <br>
        <strong>Topic B : Addressing Human Rights violation in Rakhine State
        </strong>
      </p>
      <p style="text-align: justify;">
        The Human Rights Council is an inter-governmental body within the United Nations system made up of 47 States responsible for the promotion and protection of all human rights around the globe. The topic of this council will talk about slavery in 21 st Century and also human rights violation that happen in the Rakhine State.
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-3763 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:3763,&quot;slug&quot;:&quot;unhcr&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-3763 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p align=justify>
        <img loading=lazy class="aligncenter wp-image-247 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-02-300x248.png alt width=300 height=248 srcset="https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-02-300x248.png 300w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-02-510x422.png 510w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-02-768x635.png 768w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-02-1024x847.png 1024w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-02-1080x894.png 1080w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <h2 style="text-align: center;" align="justify">
        <strong>UNHCR
        </strong>
      </h2>
      <p align=justify>
        <strong>Topic A : Addressing Resettlement Planning of Refugees.
        </strong>
        <br>
        <strong>Topic B : the Protection and Rights of Internally-Displaced Person.
        </strong>
      </p>
      <p align=justify>
        UNHCR, the UN Refugee Agency, is a global organization dedicated to saving lives, protecting rights and building a better future for refugees, forcibly displaced communities and stateless people. This council will serve a dynamic debate which mainly talks about the refugees and the use of diplomacy to tackle the refugee’s problem in terms of burden sharing and other mechanism which related to the UNHCR procedure. This council will also bring the case of the refugees and its mechanism on how to take the responsibility through the refugees, burden sharing mechanism, funding mechanism, and how to ensure the refugees get their own rights. In the other hand, the second topic will talk about the Protection and Rights of Internally-Displaced Person.
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-351 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:351,&quot;slug&quot;:&quot;oic&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true aria-labelledby=pum_popup_title_351>
  <div id=popmake-351 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div id=pum_popup_title_351 class="pum-title popmake-title">
      OIC
    </div>
    <div class="pum-content popmake-content">
      <p>
        <img loading=lazy class="size-medium wp-image-250 aligncenter" src=https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-07-300x298.png alt width=300 height=298 srcset="https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-07-300x298.png 300w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-07-100x100.png 100w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-07-510x507.png 510w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-07-150x150.png 150w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-07-768x763.png 768w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-07-1024x1017.png 1024w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-07-1080x1073.png 1080w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <h2 style="text-align: center;">
        <strong>OIC
        </strong>
      </h2>
      <p>&nbsp;
      </p>
      <p align=justify>
        <strong>Topic A : Combating Violent Sectarian Extremism in the Middle East.
        </strong>
        <br>
        <strong>Topic B : Qatar Diplomatic Crisis
        </strong>
      </p>
      <p align=justify>The Organization of Islamic Cooperation (OIC) is the second largest inter-governmental organization after the United Nations with a membership of 57 states spread over four continents. The Organization is the collective voice of the Muslim world. It endeavors to safeguard and protect the interests of the Muslim world in the spirit of promoting international peace and harmony among various people of the world. The OIC will become one of the council in the Asia Youth International Model United Nations (AYIMUN) 2017 which mainly discuss about the sectarian extremism and Qatar diplomatic crisis.
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-3761 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:3761,&quot;slug&quot;:&quot;imf&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-3761 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <p align=justify>
        <img loading=lazy class="aligncenter wp-image-251 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-05-300x300.png alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-05-300x300.png 300w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-05-400x400.png 400w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-05-100x100.png 100w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-05-510x510.png 510w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-05-150x150.png 150w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-05-768x768.png 768w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-05-1024x1024.png 1024w, https://modelunitednation.org/wp-content/uploads/2017/07/lgo-logo-05-1080x1080.png 1080w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <h2 style="text-align: center;" align="justify">
        <strong>IMF
        </strong>
      </h2>
      <p align=justify>
        <strong>Topic A : Future Framework for Condition of IMF Loans
        </strong>
        <br>
        <strong>Topic B : Addressing Resettlement Planning of the global financial assistance
        </strong>
      </p>
      <p style="text-align: justify;" align=justify>The International Monetary Fund (IMF) is an organization of 189 countries, working to foster global monetary cooperation, secure financial stability, facilitate international trade, promote high employment and sustainable economic growth, and reduce poverty around the world.
        <br>
        Created in 1945, the IMF is governed by that accountable to the 189 countries that make up its near-global membership.
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2160 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2160,&quot;slug&quot;:&quot;mayang-krisnawardhani&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2160 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Mayang Krisnawardhani
        </strong>
      </h2>
      <p>
        <a href=https://modelunitednation.org/wp-content/uploads/2017/08/Mayang.jpg>
          <img loading=lazy class="size-medium wp-image-7706 aligncenter" src=https://modelunitednation.org/wp-content/uploads/2017/08/Mayang-300x300.jpg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/Mayang-300x300.jpg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/Mayang-400x400.jpg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/Mayang-100x100.jpg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/Mayang-510x510.jpg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/Mayang-150x150.jpg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/Mayang-768x768.jpg 768w, https://modelunitednation.org/wp-content/uploads/2017/08/Mayang-1024x1024.jpg 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/Mayang-1080x1080.jpg 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/Mayang.jpg 1280w" sizes="(max-width: 300px) 100vw, 300px">
        </a>
      </p>
      <h2>
      </h2>
      <h2>
        <strong>Education
        </strong>
      </h2>
      <p>JULY, 2013 &#8211; Present
      </p>
      <p>
        <strong>Social Welfare
        </strong>
        <br>
        <strong>University of Indonesia &#8211; Depok, Indonesia
        </strong>
      </p>
      <p>JUNE, 2010 &#8211; APRIL 2013
      </p>
      <p>
        <strong>Social Studies
        </strong>
        <br>
        <strong>Regina Pacis Senior High School &#8211; Bogor, Indonesia
        </strong>
      </p>
      <h3>
        <strong>PERSONAL PROJECTS
        </strong>
      </h3>
      <p>
        <strong>Sandya Institute (06/2016 – Present)
        </strong>
      </p>
      <ul>
        <li style="text-align: justify;">Non-profit organization that works along three streams, which are peacebuilding, counter-violent extremism and human rights advocacy.
        </li>
      </ul>
      <p>
        <strong>UNICEF Digital Story Telling (08/2016 – 03/2017)
        </strong>
      </p>
      <ul>
        <li style="text-align: justify;">Creating documentaries for UNICEF with the theme of Children in Changing Climate. Focusing on how climate change effect children in Slum Area across Jakarta
        </li>
      </ul>
      <h3>
        <strong>ACHIEVEMENTS
        </strong>
      </h3>
      <p>
        <strong>Assistant Chair of Harvard World Model United
        </strong>
        <br>
        <strong>Nations (11/2016 – 03/2016)
        </strong>
      </p>
      <p>Directing over 250 delegate across 130 nations
      </p>
      <p>
        <strong>Honorable Mention in Asia-Pacific Model United
        </strong>
        <br>
        <strong>nations Conference (07/2016 – 07/2016)
        </strong>
      </p>
      <p>Manage to snatch 3rd place in the United nations Development Programme council while competing with over 30 delegate across 10 nations
      </p>
      <p>
        <strong>Best Delegate of Indonesia Model United Nations
        </strong>
        <br>
        <strong>(10/2015 – 10/2015)
        </strong>
      </p>
      <p>Manage to get 1st place while competing with more than 30 people across 5 nations
      </p>
      <h3>
        <strong>ORGANIZATIONS
        </strong>
      </h3>
      <p>
        <strong>The Climate Reality Project (04/2016 – Present)
        </strong>
        <br>
        Climate Leader and Presenter (speaker and project developer for climate change awareness among youth)
      </p>
      <p>
        <strong>Universitas Indonesia Model United nations Club
        </strong>
        <br>
        <strong>(12/2015 – 01/2017)
        </strong>
        <br>
        Secretary General or President (Managing over 600 people across faculty in University Indonesia and succeed to obtain awards in both national and international competition)
      </p>
      <p>
        <strong>Youth for Climate Change Indonesia (09/2014 –
        </strong>
        <br>
        <strong>10/2016)
        </strong>
        <br>
        Advisor (watch over project development and maintain network with government and other NGOs)
      </p>
      <h3>
        <strong>WORK EXPERIENCE
        </strong>
      </h3>
      <p>
        <strong>Intern of ASEAN Multilateral Division
        </strong>
        <br>
        Indonesia&#8217;s Ministry of Foreign Affairs
      </p>
      <p>
        <strong>Junior Assistant for Netherland 
        </strong>
        <strong>Representative
        </strong>
        <br>
        Indonesia-Netherland join cooperation in Water
      </p>
      <p>
        <strong>Intern of Sustainability and Climate Change Consultant
        </strong>
        <br>
        Pricewaterhouse Cooper Advisory
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2363 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2363,&quot;slug&quot;:&quot;ang-mei-jun-sophie&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2363 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Ang Mei Jun, Sophie
        </strong>
      </h2>
      <p>
        <img loading=lazy class="size-full wp-image-2360 aligncenter" src=https://modelunitednation.org/wp-content/uploads/2017/08/shopi.png alt width=414 height=414 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/shopi.png 414w, https://modelunitednation.org/wp-content/uploads/2017/08/shopi-400x400.png 400w, https://modelunitednation.org/wp-content/uploads/2017/08/shopi-100x100.png 100w, https://modelunitednation.org/wp-content/uploads/2017/08/shopi-150x150.png 150w, https://modelunitednation.org/wp-content/uploads/2017/08/shopi-300x300.png 300w" sizes="(max-width: 414px) 100vw, 414px">
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EDUCATION
        </strong>
      </h2>
      <p>
        <strong>2016 &#8211; Present
        </strong> : Yale-NUS College
        <br>
        <strong>2014 &#8211; 2015
        </strong> : Victoria Junior College (VJC)
        <br>
        <strong>2012 &#8211; 2013
        </strong> : Victoria Junior College (Integrated Programme)
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EXPERIENCE
        </strong>
      </h2>
      <p>
        <strong>Oct 2015 &#8211; Present
        </strong> : Siglap South Community Centre Youth Executive Committee
        <br>
        <strong>Dec 2015 &#8211; April 2016
        </strong> : The Thought Collective
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>CO-CURRICULAR ACTIVITIES
        </strong>
      </h2>
      <p>
        <strong>Sep 2016 &#8211; Present
        </strong> : NUS Political Science Society (Member)
        <br>
        <strong>Jan 2017 &#8211; Present
        </strong> : YNC Let Them Eat Cake (Baking Club) (Welfare Head)
        <br>
        <strong>Oct 2016 &#8211; Present
        </strong> : YNC Visual Arts Society (Member)
        <br>
        <strong>Jan 2017 &#8211; Present
        </strong> : YNCD (Kpop) (Member)
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>COMMUNITY SERVICE
        </strong>
      </h2>
      <p>
        <strong>May 2014 &#8211; April 2015
        </strong> :Sunlove Home at Chai Chee (Tutor)
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2265 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2265,&quot;slug&quot;:&quot;muhammad-rizal-saanun&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2265 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Muhammad Rizal Saanun
        </strong>
      </h2>
      <p>
        <img loading=lazy class="aligncenter wp-image-2256 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.10.39-300x300.jpeg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.10.39-300x300.jpeg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.10.39-400x400.jpeg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.10.39-100x100.jpeg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.10.39-510x510.jpeg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.10.39-150x150.jpeg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.10.39.jpeg 589w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EDUCATION
        </strong>
      </h2>
      <p>
        <strong>2014-Present
        </strong> : UNIVERSITAS MUHAMMADIYAH YOGYAKARTA
        <br>
        <strong>2011-2014
        </strong> : SMA NEGERI 01 NAMLEA
        <br>
        <strong>2008-2011
        </strong> : SMP NEGERI 01 NAMLEA
        <br>
        <strong>2003-2008
        </strong> : SD ALHILAAL 01 NAMLEA
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>INFORMAL EDUCATIONAL BACKGROUND
        </strong>
      </h2>
      <p>
        <strong>2016-Present
        </strong> : LINGOES CLUB YOGYAKARTA
        <br>
        <strong>2016-Present
        </strong> : CILACS UII LANGUAGE COURSE
        <br>
        <strong>2016- Present
        </strong> : CMM ASMAT PRO
        <br>
        <strong>2010
        </strong> : NEW CONCEPT ENGLISH COURSE
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>TRAINING
        </strong>
      </h2>
      <p>
        <strong>September, 4th-5th, 2014
        </strong> : LEMBAGA PENGKAJIAN DAN PENGAMALAN ISLAM (LPPI), UMY
        <br>
        <strong>November, 16th , 2014
        </strong> : SPIRITUAL LEADERSHIP TRAINING AND EDUCATION OF UMY
        <br>
        <strong>September, 13th – December 13th, 2014
        </strong> : INTERNATIONAL PROGRAM OF INTERNATIONAL RELATIONS TRAINING
        <br>
        <strong>June, 12nd , 2015
        </strong> : LEMBAGA PENGKAJIAN DAN PENGAMALAN ISLAM (LPPI), UMY
        <br>
        <strong>April, 17th , 2016
        </strong> : INTERNATIONAL RELATIONS DEPARTMENT
        <br>
        <strong>April, 24th , 2016
        </strong> : INTERNATIONAL RELATIONS DEPARTMENT
        <br>
        <strong>November, 20th , 2016
        </strong> : INTERNATIONAL RELATIONS DEPARTMENT
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>ORGANIZATION EXPERIENCES
        </strong>
      </h2>
      <p>
        <strong>2015-2016 
        </strong>: STUDENT EXECUTIVE BOARD OF FACULTY OF SOCIAL AND POLITICAL SCIENCES (BEM FISIPOL UMY)
        <br>
        <strong>2015- now
        </strong> : UMY MODEL UNITED NATIONS COMMUNITY
        <br>
        <strong>2014 &#8211; now
        </strong> : STUDENT ENGLISH ACTIVITY (SEA)
        <br>
        <strong>2015 &#8211; 2016
        </strong> : SAHABAT BEASISWA CHAPTER YOGYAKARTA
        <br>
        <strong>2016
        </strong> : ASEAN YOUTH LEADERS ASSOCIATION (AYLA) CHAPTER INDONESIA
        <br>
        <strong>2016 &#8211; 2017
        </strong> : INDONESIA GLOBAL NETWORK (IGN)
        <br>
        <strong>2015 &#8211; 2016
        </strong> : YOUTH INTERFAITH PEACEMAKER COMMUNITY (YIPC)
        <br>
        <strong>2012 &#8211; 2013
        </strong> : FORUM ANAK DAERAH KAYU PUTIH (FANDKIP)
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>ACHIEVEMENTS
        </strong>
      </h2>
      <p>
        <strong>2017
        </strong> Delegate of National Fellowship Camp 2017 by Merit Indonesia for the United Nations Sustainable Development Goals (27th-29th January 2017).
        <br>
        <strong>2017
        </strong> Co-Director (Co-Chair) of Muhammadiyah Yogyakarta Diplomatic Course, European Parliament, EU Model United Nations, Yogyakarta, Indonesia.
        <br>
        <strong>2017
        </strong> The Most Inspiring Youth award of Buru Regency given by the Regent of Buru.
        <br>
        <strong>2017
        </strong> Youth Excursion Campus Ambassador 2017
        <br>
        <strong>2017
        </strong> Youth Ambassador of Youth Excursion by Indonesia Global Network in Singapore.
        <br>
        <strong>2017
        </strong> Adjudicators of Indonesia National Debate.
        <br>
        <strong>2017
        </strong> Youtex Excursion Ambassador awardee for Youtex Excursion 2017 in Kualalumpur, Malaysia (6th-9th February 2017).
        <br>
        <strong>2016
        </strong> Youtex Excursion Ambassador awardee for Youtex Excursion 2016 in Hong Kong (21st-25th November 2016).
        <br>
        <strong>2016
        </strong> The best Presenter of Market Research and Business Presentation of Youtex Excursion in Kualalumpur, Malaysia.
        <br>
        <strong>2016
        </strong> The delegate of Indonesia for ASEAN Youth Leaders Exchange Action Program (AYLEAP) in Singapore.
        <br>
        <strong>2016
        </strong> The honorable mention delegate of Nanyang Technological University Model United Nations (NTU-MUN) in Singapore.
        <br>
        <strong>2016
        </strong> The best Presenter on research Collaboration of “The Future of South China Sea and Geopolitics in ASEAN” in The Hong Kong University (HKU), Hong Kong.
        <br>
        <strong>2016
        </strong> The most Verbal Commendation of United Nations Framework Convention on Climate Change (UNFCCC), Brawijaya Model United Nations Competition (BIMUN), Representing United States of America (USA), Topic : the Effectiveness of COP21, in Malang, East Java.
        <br>
        <strong>2016
        </strong> The Most Outstanding Delegate of Thammasat Model United Nations in Bangkok, Thailand.
        <br>
        <strong>2016
        </strong> Co-Director of European Parliament Simulation in Muhammadiyah Diplomatic Course (MYDC), Yogyakarta.
        <br>
        <strong>2016
        </strong> Top 8 Semifinalist International Humanitarian Law (IHL) English Debate Competition, Semarang, Central Java.
        <br>
        <strong>2015
        </strong> Top 20 semifinalist of Toastmaster/speech Competition in ASIA ENGLISH OLIMPIC, Binus university, Jakarta.
        <br>
        <strong>2015
        </strong> The Most Outstanding delegate of United Nations High Commissioner for Refugee (UNHCR), Representing France, Topic : Rohingya Refugee, UII-Model United Nations, Yogyakarta.
        <br>
        <strong>2015
        </strong> Best delegate of the United Nations Children’s Fund Council (UNICEF), representing The Republic of Indonesia, Topic: Child Abduction and Exploitation, Brawijaya Model United Nations (BIMUN) Competition, Malang, East Java.
        <br>
        <strong>2015
        </strong> The honorable mentions of De La sale MUN in Manila, Philippines.
        <br>
        <strong>2015
        </strong> The Best Speaker of Inspiring Youth Leader Forum Chapter Yogyakarta.
        <br>
        <strong>2015
        </strong> The 2nd Winner of Indonesia Language Debate Championship hosted by BEM FISIPOL UMY
        <br>
        <strong>2015
        </strong> The 2nd winner of “Debat Anti Rokok” held by MTCC
        <br>
        <strong>2013
        </strong> Finalist of National English Olimpic (English For Academic Purpose) in Brawijaya University, Malang.
        <br>
        <strong>2013
        </strong> 3rd Winner of English Speech Competition in the level of High School Student, Namlea, Buru District, Maluku.
        <br>
        <strong>2013
        </strong> 1st Winner of Indonesia Language Speech Competition in the level of High SchoolStudent,Namlea, Buru District, Maluku.
        <br>
        <strong>2013
        </strong> The Winner of Jujaro and Ngongare (Tourism Ambassador) of Buru District, Province Maluku.
        <br>
        <strong>2012
        </strong> The 1st Winner of Child Creative Competition of Buru District.
        <br>
        <strong>2012
        </strong> The 2nd Winner of Child Ambassador of Maluku Province.
        <br>
        <strong>2012
        </strong> The 4th Winner of Cultural Performance in National Child Forum in Yogyakarta.
        <br>
        <strong>2012
        </strong> The 1st Winner of English Debate Championship for Senior High School of Buru Regency.
        <br>
        <strong>2012
        </strong> The 4th Winner of English Debate Championship for Senior High School of Maluku Province.
        <br>
        <strong>2008
        </strong> The 1st Winner of Story Telling Competition of Maluku Province
        <br>
        <strong>2008
        </strong> Top 15 National Story Telling of Junior High School in Nusa Dua, Bali.
        <br>
        <strong>2005
        </strong> The 1st Winner of Elementary School Speech Competition in Namlea, Buru Regency, Maluku.
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>WORKING EXPERIENCES
        </strong>
      </h2>
      <p>
        <strong>2017
        </strong> : Coordinator of Events “Special Lesean”
        <br>
        <strong>2017 : 
        </strong>Standing Committee of Youtex Excursion Event in Kualalumpur, Malaysia.
        <br>
        <strong>2016 : 
        </strong>Standing Committee of Youtex Excursion Event in Hong Kong.
        <br>
        <strong>2015 : 
        </strong>Master of Ceremony of Mataf Fisipol Muhammadiyah University of Yogyakarta.
        <br>
        <strong>2015 : 
        </strong>Master of Ceremony of Gebyar Speaking presented by Student English Activity.
        <br>
        <strong>2015 : 
        </strong>Master of Ceremony of Bincang Sebatik Fresh Club Muhammadiyah University of Yogyakarta.
        <br>
        <strong>2015 : 
        </strong>The Head of event Coordinator of &#8220;Diplomacy on Model United Nations&#8221; presented by UMY MUN Community
        <br>
        <strong>2015 : 
        </strong>Master of Ceremony of Fisivaganza &#8220;Milad Fisipol ke-34&#8221;
        <br>
        <strong>2013 : 
        </strong>Child Ambassador of Buru Regency
        <br>
        <strong>2012 : 
        </strong>Tourism Ambassador of Buru Regency
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>SKILLS
        </strong>
      </h2>
      <p>Leadership, Team Work, Discipline, Hardworking, Persistent, Tough, English Public Speaking, Indonesia Public Speaking, English Speech, Model United Nations, Newscasting, Master of Ceremony, Presenter, Multi-Language Speaker, interpreter and Academic Purpose.
      </p>
      <p>&nbsp;
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2259 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2259,&quot;slug&quot;:&quot;dominique-virgil-tuapetel&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2259 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Dominique Virgil Tuapetel
        </strong>
      </h2>
      <p>
        <img loading=lazy class="aligncenter wp-image-2255 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33-300x300.jpeg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33-300x300.jpeg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33-400x400.jpeg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33-100x100.jpeg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33-510x510.jpeg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33-150x150.jpeg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33-768x768.jpeg 768w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33-1024x1024.jpeg 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33-1080x1080.jpeg 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/WhatsApp-Image-2017-08-13-at-19.11.33.jpeg 900w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>SUMMARY
        </strong>
      </h2>
      <p style="text-align: justify;">A law student of Faculty of Law, University of Indonesia. Interested in international issues and project management. Experienced in various committees and organizations with various core competences. Skilled in project management, public speaking, presentation, team management, research and negotiation.
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EXPERIENCE
        </strong>
      </h2>
      <p>&nbsp;
      </p>
      <p align=justify>
        <span style="font-family: Arial, serif;">
          <span style="font-size: medium;">
            <b>AIESEC in Universitas Indonesia
            </b>
          </span>
        </span>
      </p>
      <p align=justify>Manager of Quality and Delivery, Incoming Global Volunteer (IGV) Project (July 2016 – June 2017)
        <br>
        Organizing Committee (OC) Talent Acquisition, AIESEC UI Hunt (January 2017 – March 2017)
        <br>
        Organizing Committee President (OCP) of Little Star Summer Project 2016 (March 2016 – August 2016)
      </p>
      <p align=justify>
        <b>AIESEC in Bucharest
        </b>
      </p>
      <p align=justify>Exchange Participant of Discover Project, Winter 2016 (January 2016 – February 2016)
      </p>
      <p align=justify>
        <b>Model United Nations
        </b>
      </p>
      <p align=justify>Head Delegation of University of Indonesia for The European International Model United Nations (TEIMUN) 2017
        <br>
        Under-Secretary General (USG) Organizational, Indonesia MUN 2017
        <br>
        Vice Director of Internal, Board of Secretariat UI MUN Club
        <br>
        Assistant Director of United Nations Development Programme (UNDP)
        <br>
        Assistant Director of United Nations Human Rights Council (UNHRC)
        <br>
        Student Executive Board, Faculty of Law, University of Indonesia (BEM FH UI)
        <br>
        Staff of Community Service Department
      </p>
      <p align=justify>
      <h2>
        <strong>SKILLS
        </strong>
      </h2>
      <p>English
        <br>
        Public Speaking
        <br>
        Project Management
        <br>
        Team Management
        <br>
        Leadership
        <br>
        Diplomacy
        <br>
        Public Relations
        <br>
        Research
        <br>
        Negotiation
        <br>
        Presentation
        <br>
        Legal Research
        <br>
        Microsoft Office
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2200 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2200,&quot;slug&quot;:&quot;rizkina-aliya&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2200 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Rizkina Aliya
        </strong>
      </h2>
      <p>
        <img loading=lazy class="aligncenter wp-image-2089 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/rizkina-300x300.jpeg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/rizkina-300x300.jpeg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/rizkina-400x400.jpeg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/rizkina-100x100.jpeg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/rizkina-510x510.jpeg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/rizkina-150x150.jpeg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/rizkina-768x768.jpeg 768w, https://modelunitednation.org/wp-content/uploads/2017/08/rizkina-1024x1024.jpeg 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/rizkina-1080x1080.jpeg 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/rizkina.jpeg 900w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EDUCATION
        </strong>
      </h2>
      <p>
        <strong>University of Indonesia (2015-2019) 
        </strong>Bachelor of Law
        <br>
        <strong>SMA Negeri 8 Jakarta (2011-2015)
        </strong>
        <br>
        <strong>Island Pacific Academy, Hawaii, USA (2013-2014)
        </strong>
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>SKILLS
        </strong>
      </h2>
      <p>Leadership.
        <br>
        Fluent in English &amp; Bahasa Indonesia.
        <br>
        Effective communication &amp; Public Speaking.
        <br>
        Academic &amp; Popular Writing.
        <br>
        Legal Research.
        <br>
        Editing, Proofreading &amp; Translating.
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EXPERIENCE
        </strong>
      </h2>
      <p>
        <strong>Politeia.id as Content Director (July 2017 – Present)
        </strong>
        <br>
        Creates, manages, and edits monthly posts on politeia.id by GGI, an Indonesian youth think tank Manages a team of in-house and freelance contributors from different corners of the world
      </p>
      <p>
        <strong>Babyloania.com as Operations &amp; Legal Intern (September 2015-February 2016)
        </strong>
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>ACHIEVEMENTS
        </strong>
      </h2>
      <p>
        <strong>Internal Contract Drafting Competition 2017
        </strong>
        <br>
        3 rd Place for drafting and negotiating upon a bank merger contract
        <br>
        Event held by Business Law Society, University of Indonesia Faculty of Law
      </p>
      <p>
        <strong>Delegation of Universitas Indonesia for NTU MUN 2017
        </strong>
        <br>
        Honourable Mention (2 nd Place) as a Press Delegate representing Reuters
      </p>
      <p>
        <strong>Kennedy-Lugar Youth Exchange and Study Program (KL-YES 2013-2014)
        </strong>
        <br>
        Received a full grant from the US State Department to study in Hawaii, USA
        <br>
        Intern with former Representative Mele Carroll at Hawaii’s House of Representatives
        <br>
        Received the Global Citizen Award for being an exemplary ambassador for Island Pacific Academy and the International Baccalaureate Program
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>PUBLICATION
        </strong>
      </h2>
      <p>
        <strong>Recognizing Adherents of Traditional Beliefs (2017)
        </strong>
        <br>
        Co-written an academic paper on the “Philosophical, Juridical, and Social Perspectives of Constitutional Court Case No. XX/PUU-XIV/2016 (Reviewing the Blank Religion Column on Family and Personal Identification Documents for Adherents of Traditional Beliefs)”
      </p>
      <p>
        <strong>Politeia.id (2017)
        </strong>
        <br>
        Co-written article titled “Is Indonesia Ready to Handle Saudi’s Billions?” about foreign direct investments to Indonesia.
        <br>
        Co-written article titled “Balancing Between Powers: Thing One and Thing Two” about Indonesia’s dilemma facing the Trans Pacific Partnership (TPP) and the Regional Comprehensive Economic Partnership (RCEP).
        <br>
        Written article titled “Blasphemy?!” about Indonesia’s controversial anti-blasphemy law.
      </p>
      <p>
        <strong>Perspektif (2015-2016)
        </strong>
        <br>
        Article titled “Indonesia and the S-Word” about Indonesia’s hesitancy to provide meaningful education on sex and reproductive health for its population especially its youth
        <br>
        Companion content for a photo essay titled “From the East of Java” that highlights the separatist protests of West Papua in
      </p>
      <p>
        <strong>Jakarta on December 1, 2016. Teknika Days 2015 (October, 2015)
        </strong>
        <br>
        2 nd Place for Teknika Days held by the University of Indonesia’s Faculty of Engineering for an article titled “Prostitusi: Langkah Selanjutnya bagi Indonesia” discussing the prospective solutions concerning prostitution in Indonesia.
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>LEADERSHIP POSITIONS AND ORGANIZATIONS
        </strong>
      </h2>
      <p>
        <strong>Indonesian Students’ Association for International Studies (2016-Present)
        </strong>
        <br>
        Manager of Discussions for the division of Research and Development
        <br>
        Staff of Research and Development
      </p>
      <p>
        <strong>Indonesia Model United Nations (2017)
        </strong>
        <br>
        Head of Events, overseeing the execution of eight social and charity events for IMUN 2017
      </p>
      <p>
        <strong>Grand General Assembly (2016)
        </strong>
        <br>
        Director of Workshop, responsible for arranging Model United Nations workshops to hone a delegate’s skill in Speech, Negotiation, Resolution Drafting, and Research.
      </p>
      <p>
        <strong>Jakarta MUN (2016 &amp; 2017)
        </strong>
        <br>
        Director of the Press Committee (2016 &amp; 2017), responsible for editing and selecting articles of the conference’s press delegates as well as evaluating and deciding award-winners.
        <br>
        Delegate Relations Officer (2016), in charge of managing relations with international and national delegates as well as managing social media content.
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2196 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2196,&quot;slug&quot;:&quot;varun-chopra&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2196 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Varun Chopra
        </strong>
      </h2>
      <p>
        <img loading=lazy class="aligncenter wp-image-2086 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/varun-300x300.jpeg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/varun-300x300.jpeg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/varun-400x400.jpeg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/varun-100x100.jpeg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/varun-510x510.jpeg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/varun-150x150.jpeg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/varun-768x768.jpeg 768w, https://modelunitednation.org/wp-content/uploads/2017/08/varun-1024x1024.jpeg 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/varun-1080x1080.jpeg 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/varun.jpeg 900w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EDUCATION
        </strong>
      </h2>
      <p>
        <strong>Dual Integrated Course
        </strong> – Master of Science (M.Sc.) Mathematics +Bachelors of Engineering (B.E.) Mechanical Engineering
        <br>
        BITS PILANI, K.K Birla Goa Campus, India
        <br>
        <strong>School
        </strong>
        <br>
        D.A.V Public School, Delhi, India
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>WORK EXPERIENCE
        </strong>
      </h2>
      <p>Editor of Poet of the soul, Literally Literary.
        <br>
        Creative Writer at Filter Copy and Indian Buzz.
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>MUN EXPERIENCE
        </strong>
      </h2>
      <p>Participant and honorary mention in TEIMUN 2014, represented republic of china.
        <br>
        Participant and best delegate in TEIMUN 2015, represented Russia.
        <br>
        Participant of DUMUN 2015
        <br>
        Chair position in HRC, BITSMUN 2016
        <br>
        Crisis Team Member in TEIMUN 2016
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>ACHIEVMENTS
        </strong>
      </h2>
      <p>
        <strong>Book Publication, Title &#8211; 100 Million Oblivion
        </strong>
        <br>
        www.amazon.in/100-Million-Oblivion-Varun-Chopra/dp/9352060318
        <br>
        <strong>Winner of best idea contest, Tokyo
        </strong>
        <br>
        bizjapan.org
        <br>
        <strong>Organizing Team Member at TEIMUN, Netherland
        </strong>
        <br>
        teimun.org/the-organisation/staff
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2194 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2194,&quot;slug&quot;:&quot;nariswari-khairanisa-nurjaman&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2194 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Nariswari Khairanisa Nurjaman
        </strong>
      </h2>
      <p>
        <img loading=lazy class="aligncenter wp-image-2082 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI-300x300.jpeg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI-300x300.jpeg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI-400x400.jpeg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI-100x100.jpeg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI-510x510.jpeg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI-150x150.jpeg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI-768x768.jpeg 768w, https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI-1024x1024.jpeg 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI-1080x1080.jpeg 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/NARISWARI.jpeg 900w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EDUCATION
        </strong>
      </h2>
      <p>2013 – 2017: Undergraduate Student at Department of International Relations,
        <br>
        Faculty of Social and Political Sciences, Universitas Indonesia (UI). GPA: 3.80
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>WORKING EXPERIENCE
        </strong>
      </h2>
      <p>2015 – 2016 Contributor at Sp ea k! M ag a zine, The Jakarta Post
        <br>
        2016 – 2017 Brand Ambassador at Jan Sport In don esia
        <br>
        2015 – 2017 Teaching Assistant at D ep artm e nt of In terna tional Relat ion s, University of Indonesia
        <br>
        2017 Member of LINE Think Tank Indonesia
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>ORGANISATIONAL EXPERIENCE
        </strong>
      </h2>
      <p>
        <strong>Indonesian Student Association for International Studies (ISAFIS)
        </strong>
        <br>
        2015 – 2016 : Secretary General
        <br>
        2014 – 2015 : Manager of Organizational Development
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>MODEL UNITED NATIONS EXPERIENCE
        </strong>
      </h2>
      <p>
        <strong>The European International Model United Nations (The Hague, The Netherlands)
        </strong>
        <br>
        Chair of Human Rights Council (2017)
        <br>
        Chair of General Assembly (2016)
        <br>
        Best Delegate of General Assembly (2015)
      </p>
      <p>
        <strong>Indonesia International Model United Nations (Depok, Indonesia)
        </strong>
        <br>
        Under Secretary General of Substance (2016)
        <br>
        Assistant Director of ASEAN Regional Forum (ARF) (2015)
      </p>
      <p>
        <strong>Jakarta Model United Nations (Jakarta , Indonesia)
        </strong>
        <br>
        Chair of Social and Humanitarian (SOCHUM) committee (2016)
      </p>
      <p>
        <strong>Bandung Model United Nations (Bandung, Indonesia)
        </strong>
        <br>
        Diplomacy Award at General Assembly (2015)
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>TRAINING EXPERIENCE
        </strong>
      </h2>
      <p>
        <strong>2016
        </strong>
        <br>
        <strong>National Public Participant of World Culture Forum
        </strong> held by UNESCO and Indonesian Ministry of Education and Culture in Bali, Indonesia
        <br>
        <strong>Master of Ceremony
        </strong> of Asia Europe Meeting (ASEM) Day held by Indonesian Ministry of Foreign Affairs and Department of International Relations University of Indonesia
      </p>
      <p>
        <strong>2015
        </strong>
        <br>
        <strong>Participant
        </strong> of Development Workshop at The International Student Festival in Trondheim (ISFiT) Norway 2015
        <br>
        <strong>Assistant Director
        </strong> of ASEAN Regional Forum (ARF) Indonesia Model United Nations (IMUN) 2015
      </p>
      <p>
        <strong>2014
        </strong>
        <br>
        <strong>Participant 
        </strong>of UNESCO Global Media Forum in Bali, Indonesia
        <br>
        <strong>Participant
        </strong> of UNESCO Asia Pacific Youth Training on Media and Civic Participation in Bali, Indonesia
        <br>
        <strong>Indonesian Representation
        </strong> for UN Women World Congress of Global Partnership in Seoul, South Korea
        <br>
        <strong>Project Officer
        </strong> of ISAFIS Youth Panel 2014 a panel discussion with the theme “Soft Power: The Power of Influence over Coerce”
        <br>
        <strong>Participant
        </strong> of “Millenium Development Goals (MDGs) and Indonesia’s Accomplishment” Seminar by UNDP Indonesia at FISIP UI
      </p>
      <p>
        <strong>2013
        </strong>
        <br>
        <strong>Liaison Officer
        </strong> at National Conference of International Relations Students (PNMHII XXV) at Universitas Indonesia.
        <br>
        <strong>Member of Organizing Com mittee
        </strong> for CIReS’ (Center of International Relations Studies) International Seminar “From Local to Regional: the Strategic Role of Local Government towards ASEAN Economic Community” at Universitas Indonesia
      </p>
      <p>&nbsp;
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2192 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2192,&quot;slug&quot;:&quot;patricia-mae-a-torres&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2192 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Patricia Mae A. Torres
        </strong>
      </h2>
      <p>
        <img loading=lazy class="aligncenter wp-image-2155 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/patricia-300x276.png alt width=300 height=276 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/patricia-300x276.png 300w, https://modelunitednation.org/wp-content/uploads/2017/08/patricia.png 434w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>Academic Background
        </strong>
      </h2>
      <p>University of Asia and the Pacific
        <br>
        Pearl Drive Corner Ortigas Center San Antonio, Pasig City
        <br>
        Master of Arts in Political Economy
        <br>
        with specialization in International Relations and Development
        <br>
        2014-present
        <br>
        `
        <br>
        Financial Assistance (Full-100%) scholar
        <br>
        Rizal High School
        <br>
        Dr. Sixto Antonio Ave. Caniogan, Pasig City
        <br>
        2010-2014
        <br>
        Graduated with honors; Most Outstanding Student for Campus Journalism
      </p>
      <p>Maybunga Elementary School
        <br>
        Dr. Sixto Antonio Ave. Maybunga, Pasig City
        <br>
        2008-2010
        <br>
        Graduated with Honors
      </p>
      <p>La Immaculada Concepcion School
        <br>
        Caruncho Ave. Malinao, Pasig City
        <br>
        2003-2008
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>Work Experience
        </strong>
      </h2>
      <p>&nbsp;
      </p>
      <p>Consultant for Youth and Arts and Culture at Senator Bam Aquino’s Office
        <br>
        August 2016-present
      </p>
      <p>News and Sports Correspondent for The Philippine Daily Inquirer
        <br>
        May 2014- Novermber 2015
      </p>
      <p>Student Assistant for the Communications and Research Center, UA&amp;P
        <br>
        October 2014-present
      </p>
      <p>Mission Representative of ASEAN Youth Leaders’ Association-Philippines
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>Chairing Experience
        </strong>
      </h2>
      <p>
        <strong>Harvard World Model UN Rome
        </strong>
        <br>
        March 21-26, 2016
        <br>
        Rome, Italy
        <br>
        Assistant Chair of the Commission on the
        <br>
        Status of Women
      </p>
      <p>
        <strong>University of the Philippines Diliman
        </strong>
        <br>
        <strong>Model UN
        </strong>
        <br>
        UP Diliman, Quezon City
        <br>
        October 24-26, 2015
        <br>
        Committee Chair of the Human Rights
        <br>
        Council
      </p>
      <p>
        <strong>University of Asia and the Pacific Model 
        </strong>
        <strong>UN Conference 2016
        </strong>
        <br>
        University of Asia and the Pacific, Ortigas
        <br>
        Center, Pasig City
        <br>
        November 3-4, 2016
        <br>
        Committee Vice Chair of the General
        <br>
        Assembly
      </p>
      <p>
        <strong>Philippine Model Congress 2016
        </strong>
        <br>
        Senate of the Philippines, Pasay City
        <br>
        November 5-6, 2016
        <br>
        Chairperson of Committee 3
      </p>
      <p>
        <strong>University of Asia and the Pacific Model 
        </strong>
        <strong>UN Conference 2015
        </strong>
        <br>
        University of Asia and the Pacific, Ortigas
        <br>
        Center, Pasig City
        <br>
        September 24-26, 2015
        <br>
        Committee Chair of the Economic and Social
        <br>
        Council
      </p>
      <p>
        <strong>UA&amp;P Model House of Representatives
        </strong>
        <br>
        University of Asia and the Pacific
        <br>
        May 21-22, 2017
        <br>
        Speaker of the House, Head for Substantives
      </p>
      <p>
        <strong>11 th National Youth Parliament
        </strong>
        <br>
        Sarabia Manor Hotel, IloIlo City
        <br>
        Organized by the National Youth Commission
        <br>
        June 4-7, 2016
        <br>
        National Capital Region Convenor
        <br>
        Vice Chair of the Committee on Health
        <br>
        National Capital Region Delegate
      </p>
      <p>
        <strong>University of Asia and the Pacific Model 
        </strong>
        <strong>House of Representatives
        </strong>
        <br>
        University of Asia and the Pacific, Pearl
        <br>
        Drive, San Antonio, Pasig City
        <br>
        March 30-31, 2015
        <br>
        Chairman of the Dais to the Foreign Affairs
        <br>
        Committee
      </p>
      <p>&nbsp;
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>Delegate Experience
        </strong>
      </h2>
      <p>&nbsp;
      </p>
      <p>
        <strong>De La Salle Model United Nations
        </strong>
        <br>
        De La Salle University
        <br>
        March 11-14, 2016
        <br>
        Received a Best Delegate Award
      </p>
      <p>
        <strong>Harvard World Model United Nations Conference
        </strong>
        <br>
        Kintex Exhibition Complex, Seoul, Korea
        <br>
        March 16-21, 2015
        <br>
        Delegate of the Philippines representing
        <br>
        Germany to the International Monetary Fund
        <br>
        Received a Diplomacy Award
      </p>
      <p>
        <strong>Ateneo Model United Nations
        </strong>
        <br>
        Ateneo de Manila University, Katipunan Ave.,
        <br>
        Quezon City
        <br>
        January 29-31, 2015
        <br>
        Received a Best Delegate Award
      </p>
      <p>
        <strong>Benilde Model United Nations
        </strong>
        <br>
        De La Salle-College of Saint Benilde, 2544
        <br>
        Taft Ave., Malate, Manila
        <br>
        November 12-15, 2014
        <br>
        Received Most Outstanding Delegate award
      </p>
      <p>
        <strong>University of the Philippines Diliman Model United Nations
        </strong>
        <br>
        NCPAG and Ang Bahay ng Alumni, UP
        <br>
        Diliman, Quezon city
        <br>
        October 25-26, 2014
        <br>
        Received Honorable Mention Award
      </p>
      <p>
        <strong>University of Asia and the Pacific Model United Nations
        </strong>
        <br>
        University of Asia and the Pacific, Ortigas
        <br>
        Center, Pasig City
        <br>
        September 25-27, 2014
        <br>
        Delegate
      </p>
      <p>
        <strong>2 nd ASEAN Foundation Model ASEAN
        </strong>
        <br>
        Meeting 2016
        <br>
        International Convention and Training Center,
        <br>
        Vientiane, Laos
        <br>
        October 1-7, 2016
        <br>
        Head of the Philippine Delegation (Head of
        <br>
        Government)
      </p>
      <p>
        <strong>Philippine Model Congress
        </strong>
        <br>
        Senate Hall of the Philippines, Pasay City
        <br>
        October 17-18, 2015
        <br>
        Delegate
      </p>
      <p>
        <strong>University of Asia and the Pacific Model
        </strong>
        <br>
        <strong>UN Conference 2015
        </strong>
        <br>
        University of Asia and the Pacific, Ortigas
        <br>
        Center, Pasig City
        <br>
        September 23-27, 2014
        <br>
        Delegate
      </p>
      <h2>
      </h2>
      <h2>
      </h2>
      <h2>
        <strong>Relevant Conferences/Trainings
        </strong>
      </h2>
      <p>&nbsp;
      </p>
      <p>Regional Youth Advisory Council National Convention.
      </p>
      <p>United Nations Winter Youth Assembly.
      </p>
      <p>ASEAN Youth Initiative Conference.
      </p>
      <p>Asia Pacific Social Protection Week International Conference.
      </p>
      <p>ASEAN Youth Exchange and Action Program
      </p>
      <p>ASEAN Youth Exchange on Education.
      </p>
      <p>Islands of Good Governance: Public Government
        <br>
        Forum.
      </p>
      <p>Head On: Public Government Forum.
      </p>
      <p>Speak Out 2015: LGBT and Social Issues Awareness and Acceptance Training of Trainors.
      </p>
      <p>TEDxOrtigas.
      </p>
      <p>National Schools Press Conference.
      </p>
      <p>&nbsp;
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>Involvements
        </strong>
      </h2>
      <p>&nbsp;
      </p>
      <p>National Youth Commission
        <br>
        11 th National Youth Parliament
        <br>
        National Capital Region Regional Convenor ’16-‘18
      </p>
      <p>ASEAN Youth Leaders’ Association
        <br>
        Mission Representative of the Philippines to the International board ’15-‘17
      </p>
      <p>Task Force on Youth Development-Pasig
        <br>
        Vice President ’14-16
        <br>
        President ’16-‘18
      </p>
      <p>UA&amp;P Model United Nations Secretariat 2015
        <br>
        Under-Secretary General for Administrative Affairs
      </p>
      <p>POLIS (The School of Law and Governance Academic Organization)
        <br>
        Member ’14-‘19
      </p>
      <p>Philippine Debate Union
        <br>
        Debater-member
      </p>
      <p>SABIO (The UA&amp;P Scholars’ Organization)
        <br>
        Project Head ’15-‘16
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2144 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2144,&quot;slug&quot;:&quot;sean-chiong-ajihil&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2144 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Sean Chiong Ajihil
        </strong>
      </h2>
      <p style="text-align: center;">
        <img loading=lazy class="aligncenter wp-image-2080 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/seans-300x300.jpeg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/seans-300x300.jpeg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/seans-400x400.jpeg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/seans-100x100.jpeg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/seans-510x510.jpeg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/seans-150x150.jpeg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/seans-768x768.jpeg 768w, https://modelunitednation.org/wp-content/uploads/2017/08/seans-1024x1024.jpeg 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/seans-1080x1080.jpeg 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/seans.jpeg 900w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <p>&nbsp;
      </p>
      <h2 style="text-align: left;">
        <strong>EDUCATION
        </strong>
      </h2>
      <h4 style="text-align: left;">2014 &#8211; Present
      </h4>
      <p style="text-align: left;">DE LA SALLE UNIVERSITY Bachelor of Arts in Political Science. Dean’s List: 2nd &amp; 3rd Term, AY 2014-2015
      </p>
      <h4 style="text-align: left;">2010 &#8211; 2014
      </h4>
      <p style="text-align: left;">SOUTHVILLE INTERNATIONAL SCHOOL AND COLLEGES High School Diploma, March 2014.
      </p>
      <p>&nbsp;
      </p>
      <h2 style="text-align: left;">
        <strong>CO-CURRICULAR ACTIVITIES
        </strong>
      </h2>
      <p>&nbsp;
      </p>
      <h4 style="text-align: justify;">2014 &#8211; 2015
      </h4>
      <p style="text-align: justify;">EXECUTIVE OFFICER, DLSU-POLITICAL SCIENCE SOCIETY In charge of reserving, arranging, and organizing venues, equipment, and other logistics for events and projects implemented by the organization. Processed all documents needed before, during, and after events/projects.
      </p>
      <h4 style="text-align: justify;">2015 &#8211; 2016
      </h4>
      <p style="text-align: justify;">APSOP AMBASSADOR, DLSU-POLITICAL SCIENCE SOCIETY In charge of planning, reserving, arranging, and organizing venues, equipment, and other logistics for events and projects implemented by the organization. Processed all documents needed before, during, and after events/projects.
      </p>
      <p style="text-align: justify;">EXTERNALS AMBASSADOR, ASSOCIATION OF POLITICAL SCIENCE ORGANIZATIONS OF THE PHILIPPINES In charge of planning, reserving, arranging, and organizing venues, equipment, and other logistics for events and projects implemented by the association.
      </p>
      <p style="text-align: justify;">OPERATIONS AND LOGISTICS, DLSU-Red Cross Youth Member of the Operations and Logistics Team. In charge of reserving, arranging, and organizing venues, equipment, and other logistics for events and projects implemented by the organization.
      </p>
      <p>&nbsp;
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2157 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2157,&quot;slug&quot;:&quot;miguel-leonardo-t-villegas&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2157 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Miguel Leonardo T. Villegas
        </strong>
      </h2>
      <p>
        <img loading=lazy class="aligncenter wp-image-2094 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/miguel-300x300.jpeg alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/miguel-300x300.jpeg 300w, https://modelunitednation.org/wp-content/uploads/2017/08/miguel-400x400.jpeg 400w, https://modelunitednation.org/wp-content/uploads/2017/08/miguel-100x100.jpeg 100w, https://modelunitednation.org/wp-content/uploads/2017/08/miguel-510x510.jpeg 510w, https://modelunitednation.org/wp-content/uploads/2017/08/miguel-150x150.jpeg 150w, https://modelunitednation.org/wp-content/uploads/2017/08/miguel-768x768.jpeg 768w, https://modelunitednation.org/wp-content/uploads/2017/08/miguel-1024x1024.jpeg 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/miguel-1080x1080.jpeg 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/miguel.jpeg 900w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>OBJECTIVE
        </strong>
      </h2>
      <p>To become part of a dynamic multinational company wherein I would be given the opportunity to better hone my skills and abilities or a position that I will be suitable in and will be able to grow from.
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EDUCATION
        </strong>
      </h2>
      <p>&nbsp;
      </p>
      <h3>2014 &#8211; Present
      </h3>
      <p>DE LA SALLE UNIVERSITY Bachelor of Arts in Psychology. 1 st Honor Dean’s List: 1 st – 3 rd Term, AY 2014-2015; 1 st – 3 rd Term, AY 2015-2016. 1 st Term – 2 nd Term, AY 2016-2017. Year End Honor for Academics: AY 2014-2015, AY 2015-2016. Jose Rizal Honor’s Society. Candidate for Magna Cum Laude with a CGPA of 3.658.
      </p>
      <h3>2002 &#8211; 2014
      </h3>
      <p>COLEGIO SAN AGUSTIN – MAKATI High School Diploma, March 2014. Loyalty Awardee
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>WORK EXPERIENCE
        </strong>
      </h2>
      <p>&nbsp;
      </p>
      <h3>January 2017 – March 2017
      </h3>
      <p>INTERN, ST. LUKE’S MEDICAL CENTER, GLOBAL CITY
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>CO-CURRICULAR ACTIVITIES
        </strong>
      </h2>
      <h3>
      </h3>
      <p>HEAD DELEGATE, NATIONAL MODEL UNITED NATIONS – NEW YORK 2017 In charge of all logistics and administrative matters for the delegation as well as monitoring training sessions and progress of the delegation in preparation for the conference
      </p>
      <p>MOST OUTSTANDING POSITION PAPER, NATIONAL MODEL UNITED NATIONS – NEW YORK 2017 Awarded the Outstanding Position Paper award in the General Assembly First Committee
      </p>
      <p>TRAINING TEAM, NATIONAL MODEL UNITED NATIONS – D.C. 2017 Trains and assists the university’s delegation to be sent to NMUN DC
      </p>
      <p>DELEGATE RELATIONS DIRECTOR, PHILIPPINE MODEL CONGRESS 2017 Serves as the primary liaison between the Philippine Model Congress Executive Team and potential and final delegates, and is in charge of the application and deliberation of delegates and ambassadors.
      </p>
      <p>FOUNDING MEMBER, DE LA SALLE MODEL UNITED NATIONS SOCIETY
      </p>
      <p>DELEGATE RELATIONS OFFICER (YOUTH ASSEMBLY RELATIONS), PHILIPPINE HUMANITARIAN FORUM Tasked to address all concerns of and coordinate with all incoming delegates, particularly during the conference proper, from all over the country
      </p>
      <p>CONGRESS OPERATIONS DIRECTOR, PHILIPPINE MODEL CONGRESS 2016 In charge of selecting proposed bills to be discussed and passed to the Congress of the Philippines, and oversees the flow of session during the conference proper.
      </p>
      <p>BATCH GENERAL, DLSU-SANTUGON SA TAWAG NG PANAHON Assisted in the training and campaign of the candidates for the University Student Government Elections
      </p>
      <p>PROJECT HEAD MGThesis 101, DLSU-BUSINESS MANAGEMENT SOCIETY
        <br>
        Spearheaded a seminar for students majoring in Management so
        <br>
        that they may be informed of the process of their thesis application
        <br>
        and writing.
      </p>
      <p>CAMPUS LEVEL FINALIST, MAYBANK GO AHEAD CHALLENGE 2016
        <br>
        Participated in Maybank’s GO Ahead Challenge 2016 and ended as
        <br>
        a finalist for the Campus Level.
      </p>
      <p>PROJECT HEAD spRED THE LOVE, DLSU-BUSINESS MANAGEMENT
        <br>
        SOCIETY
        <br>
        Spearheaded a Valentine’s Day fundraiser for our beneficiary Mano
        <br>
        Amiga.
      </p>
      <p>OPERATIONS AND LOGISTICS CHAIRPERSON, DLSU-FAST2014
        <br>
        UNIVERSITY STUDENT GOVERNMENT
        <br>
        Head of the Operations and Logistics Team. In charge of reserving,
        <br>
        arranging, and organizing venues, equipment, and other logistics for
        <br>
        events and projects implemented by the student government.
        <br>
        Officer in charge for multiple projects and events
      </p>
      <p>DOCUMENTATIONS JUNIOR OFFICER, DLSU-BUSINESS MANAGEMENT
        <br>
        SOCIETY
        <br>
        In charge of reserving, arranging, and organizing venues, equipment,
        <br>
        and other logistics for events and projects implemented by the
        <br>
        organization. Processed all documents needed before, during, and
        <br>
        2after events/projects.
      </p>
      <p>LINKAGES ASSISTANT TEAM LEADER, DLSU-8 TH BUSINESS MANAGEMENT
        <br>
        STUDENTS CONVENTION
        <br>
        In charge of handling the sponsors and participating partners.
        <br>
        2015-2016 PARTNER LINKAGES ASSISTANT TEAM LEADER, DLSU-ART AND APPETITE
        <br>
        Contacted and negotiated with food and art concessionaires which
        <br>
        comprised the entire event dedicated to the beneficiary Mano
        <br>
        Amiga.
      </p>
      <p>ORGANIZATIONAL RESEARCH AND ANALYSIS SENIOR ASSOCIATE,
        <br>
        DLSU- COUNCIL OF STUDENT ORGANIZATIONS (CSO)
        <br>
        Handled surveys, concerns, and inquiries from different organizations
        <br>
        within the university. Innovated ways to improve the systems
        <br>
        conducted within the organization. Officer in charge for The Green
        <br>
        Card Alliance.
      </p>
      <p>JUNIOR CORE MEMBER, DLSU-SANTUGON SA TAWAG NG PANAHON
        <br>
        Campaign officer during Freshmen and General Elections. Trained
        <br>
        with other members towards being a unified premier socio-political
        <br>
        party.
      </p>
      <p>OPERATIONS AND LOGISTICS EXECUTIVE, DLSU-FAST2014 UNIVERSITY
        <br>
        STUDENT GOVERNMENT
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2135 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2135,&quot;slug&quot;:&quot;saarthak-ravi&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quoquot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2135 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Saarthak Ravi
        </strong>
      </h2>
      <p>
        <img loading=lazy class="aligncenter wp-image-1989 size-medium" src=https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min-300x300.png alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min-300x300.png 300w, https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min-400x400.png 400w, https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min-100x100.png 100w, https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min-510x510.png 510w, https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min-150x150.png 150w, https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min-768x768.png 768w, https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min-1024x1024.png 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min-1080x1080.png 1080w, https://modelunitednation.org/wp-content/uploads/2017/08/saarthak-min.png 900w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <h2>
      </h2>
      <h2>
        <strong>EDUCATION
        </strong>
      </h2>
      <p>BGS International Public School, Dwarka
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>Academic Achievements
        </strong>
      </h2>
      <ul>
        <li>
          <strong>2014
          </strong>
          <ul>
            <li>9.2 CGPA in CBSE 10th
            </li>
          </ul>
        </li>
        <li>
          <strong>2013
          </strong>
          <ul>
            <li>10 CGPA in CBSE 9th
            </li>
            <li>Proficiency in English
            </li>
          </ul>
        </li>
        <li>
          <strong>2012
          </strong>
          <ul>
            <li>90.90% in Class 8th
            </li>
            <li>Proficiency in English with 94.6%
            </li>
            <li>Proficiency in Social Studies with 99.50%
            </li>
          </ul>
        </li>
        <li>
          <strong>2011
          </strong>
          <ul>
            <li>10 CGPA in Class 7th
            </li>
          </ul>
        </li>
        <li>
          <strong>2010
          </strong>
          <ul>
            <li>91.74 in Class 6th
            </li>
          </ul>
        </li>
        <li>
          <strong>2009
          </strong>
          <ul>
            <li>92.04% in Class 5th
            </li>
          </ul>
        </li>
        <li>
          <strong>2008
          </strong>
          <ul>
            <li>94.30% in class 4th
            </li>
            <li>Secured “Scholar’s Badge” tie
            </li>
          </ul>
        </li>
        <li>
          <strong>2007
          </strong>
          <ul>
            <li>94.93% in class 3rd
            </li>
          </ul>
        </li>
        <li>
          <strong>2006
          </strong>
          <ul>
            <li>93.06% in class 2nd
            </li>
          </ul>
        </li>
        <li>
          <strong>2005
          </strong>
          <ul>
            <li>98.1% in Class 1st
            </li>
            <li>Proficiency in Maths
            </li>
          </ul>
        </li>
      </ul>
      <p>&nbsp;
      </p>
      <h2>
        <strong>Extra Curricular Achievements
        </strong>
      </h2>
      <p>2016:
        <br>
        Elected Head Boy of BGSIPS, Dwarka
        <br>
        Held Position of Vice President at Operation Neptune Spear at Scottish High MUN
        <br>
        Held position of Crisis Director at SHISMUN
        <br>
        Position of Rapporteur of General Assembly at VISMUN
        <br>
        Position of Chairperson of White House Situation Room at BGSMUN
        <br>
        Position of Secretary General at BGSMUN
        <br>
        Selected to attend Global Young Leaders Program at Wharton Business School, UPenn.
        <br>
        Position of Under-Secretary General at IIMUN Noida
        <br>
        Position of Under-Secretary General at IIMUN Gurugram
        <br>
        First Position in Inter-School Symposium at Basava School
        <br>
        Participated as Delegate of Finland in RISMUN
      </p>
      <p>2015:
        <br>
        High Commendation Award at University of Chicago MUN
        <br>
        Elected Cultural Coordinator of BGSIPS
        <br>
        Best Delegate Award at MRMUN
        <br>
        International Rank 36 at BIFO Finance Olympiad (Stage 2)
        <br>
        National Rank 3 at BIFO Finance Olympiad (Stage 1)
        <br>
        Secretary General of BGSMUN 2015
        <br>
        Chairperson of General Assembly BGSMUN 2015
        <br>
        Spec Men Award in the Security Council VIDMUN 2015
        <br>
        Spec Men Award in the General Assembly MCSMUN 2015
        <br>
        Best Position Paper Award at Commonwealth in BBPSMUN 2015
        <br>
        SpellVocab Challenger Merit Holder
        <br>
        Head Delegate of Best Delegation at VIDMUN 2015
        <br>
        Compering team at Honors Day at BGSIPS
      </p>
      <p>2014:
        <br>
        Elected Head Boy of Amity International School, Lucknow (second year in a row)
        <br>
        Participated in Maxfort MUN
        <br>
        Second Position in Inter-House Declamation Competition
        <br>
        Participated in French Olympiad
        <br>
        Part of BGSIPS Animation Team for Honors Day 2014
        <br>
        Participated in Luminarium at DPS Dwarka in Shakespearean Play
        <br>
        First Position at St. Gregorios Inter School Debate
        <br>
        Special Award at Symposium at Basava 2014
        <br>
        Second position in mental maths competition
      </p>
      <p>2013:
        <br>
        Elected Head Boy of Amity International School, Lucknow
        <br>
        First position in English Inter House Elocution Competition
        <br>
        Second Position in Math Amity Competition
        <br>
        Second Position in Ramanujan Maths Competition
        <br>
        First Position in Mental Maths Competition
        <br>
        Participated in AIMUN 2013
        <br>
        Best Quiz Master Award
        <br>
        First Position at Spellathon 2013
        <br>
        First Position at Amity International Day Cultural Fest
      </p>
      <p>2012:
        <br>
        First Position in English Debate
        <br>
        Participated in GTSE Olympiad
        <br>
        Third position in Hindi Debate
        <br>
        Participated in NCO 2012
        <br>
        Participated in NSO 2012
        <br>
        Elected Sports Captain of AIS Lucknow
        <br>
        Best Sportsperson Award
        <br>
        Participated in UN Information Test Certificate
        <br>
        2nd Position in Vasudha Science Exhibition
      </p>
      <p>2011:
        <br>
        Completed Young Animator Program
        <br>
        Distinguished Performance in ASSET Olympiad
      </p>
      <p>2010:
        <br>
        Creditable Performance in ASSET Olympiad
        <br>
        Participated in DPS Games
        <br>
        Participated in United Colors of India program
        <br>
        Second position in Math Weekly Activity
      </p>
      <p>2009:
        <br>
        Certificate of Appreciation – Fund Raising for National Association for the Blind
        <br>
        Certificate of Appreciation – Volunteer for ‘Clean Campus Program’
        <br>
        Certificate awarding Green Belt in Taekwondo
        <br>
        Semi-finalist in Mini Tennis Tournament by Mahesh Bhupati Tennis Academy
        <br>
        Appointed President of the Environmental Council
      </p>
      <p>2008:
        <br>
        Certificate of Appreciation for Fund Raising for National Association for the Blind
        <br>
        Scholar Badge
        <br>
        Certificate awarding Yellow Belt in Taekwondo
      </p>
      <p>2007:
        <br>
        Securing more than 90% in Acadamics
        <br>
        Certificate for ‘Distinguished Performance’
        <br>
        3rd Prize in Hindi Story Telling Competition
        <br>
        Certificate of Participation in International Informatics Olympiad
        <br>
        Certificate of Participation in MaRRS International Spelling Bee
      </p>
      <p>2006:
        <br>
        1st Position – Techno Art Competition
      </p>
      <p>2005:
        <br>
        Certificate of Merit – Proficiency in Mathematics
      </p>
      <p>2004:
        <br>
        2nd Position – Quiz Competition
        <br>
        2nd Position – Folk Dance Competition
        <br>
        Certificate of Merit – Proficiency in Number Work
      </p>
      <p>2003:
        <br>
        2nd Position in Fancy Dress
        <br>
        Certificate of Appreciation – English Rhymes
        <br>
        Certificate of Appreciation – Story Narration
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div>
<div id=pum-2119 class="pum pum-overlay pum-theme-332 pum-theme-default-theme popmake-overlay click_open" data-popmake={&quot;id&quot;:2119,&quot;slug&quot;:&quot;kevin-angdreas-2&quot;,&quot;theme_id&quot;:332,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;responsive_min_width&quot;:false,&quot;responsive_max_width&quot;:false,&quot;position_bottom&quot;:false,&quot;position_left&quot;:false,&quot;position_right&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:false,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:false,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:false,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:false,&quot;animation_speed&quot;:false,&quot;animation_origin&quot;:false,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:false},&quot;close&quot;:{&quot;text&quot;:false,&quot;button_delay&quot;:false,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}} role=dialog aria-hidden=true>
  <div id=popmake-2119 class="pum-container popmake theme-332 pum-responsive pum-responsive-medium responsive size-medium">
    <div class="pum-content popmake-content">
      <h2 style="text-align: center;">
        <strong>Kevin Angdreas
        </strong>
      </h2>
      <p>
        <img loading=lazy class="wp-image-1994 size-medium aligncenter" src=https://modelunitednation.org/wp-content/uploads/2017/08/Kevin1-300x300.png alt width=300 height=300 srcset="https://modelunitednation.org/wp-content/uploads/2017/08/Kevin1-300x300.png 300w, https://modelunitednation.org/wp-content/uploads/2017/08/Kevin1-400x400.png 400w, https://modelunitednation.org/wp-content/uploads/2017/08/Kevin1-100x100.png 100w, https://modelunitednation.org/wp-content/uploads/2017/08/Kevin1-510x510.png 510w, https://modelunitednation.org/wp-content/uploads/2017/08/Kevin1-150x150.png 150w, https://modelunitednation.org/wp-content/uploads/2017/08/Kevin1-768x768.png 768w, https://modelunitednation.org/wp-content/uploads/2017/08/Kevin1-1024x1024.png 1024w, https://modelunitednation.org/wp-content/uploads/2017/08/Kevin1-1080x1080.png 1080w" sizes="(max-width: 300px) 100vw, 300px">
      </p>
      <h2>
      </h2>
      <h2>
        <strong>PROFILE
        </strong>
      </h2>
      <p>Currently, I am in my second year, almost to my third year involve in Model UN. I have been to around 20 Model UN conferences. I have chaired for 5 times and the rest were delegates under my university, Asia Pacific University, Kuala Lumpur, Malaysia.
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>Model UN EXPERIENCE
        </strong>
      </h2>
      <p>Asia Pacific University Model UN 2015 &#8211; European Union
        <br>
        Tenby KL Model UN 2015 &#8211; Security Council &#8211; Best Delegate
        <br>
        Sunway University Model UN 2015 &#8211; General Assembly &#8211; Most Passionate Delegate
        <br>
        Methodist College KL Model UN 2015 &#8211; Security Council &#8211; Honorable Mention
        <br>
        Penang Model UN 2015- ASEAN
        <br>
        Fairview Model UN 2015 &#8211; European Council
        <br>
        Taylor’s University Model UN 2015 &#8211; European Union &#8211; Honorable Mention
        <br>
        Brickfield’s Asia College Model UN 2015 &#8211; ECOSOC
        <br>
        Malaysian National Crisis Simulation 2015 &#8211; Civil Rights Union Council &#8211; Chair
        <br>
        Nottingham Model UN 2016 &#8211; World Bank &#8211; Outstanding Delegate
        <br>
        Harvard WorldMUN 2016 &#8211; ECOFIN
        <br>
        Tenby KL Model UN &#8211; European Union &#8211; Chair
        <br>
        NISP Model UN 2016 &#8211; SPECPOL &#8211; Most Passionate
        <br>
        Asia Pacific University Model UN 2016 &#8211; European Union &#8211; Chair
        <br>
        Sunway University Model UN 2016 &#8211; ECOSOC &#8211; Chair
        <br>
        Malaysian National Model UN 2016 &#8211; Security Council
        <br>
        Taylor’s College Sri Hartamas Model UN 2016 &#8211; Security Council
        <br>
        Asia Pacific Model UN Conference 2016 &#8211; European Union &#8211; Most Diplomatic
        <br>
        Jakarta Model UN 2016 &#8211; Security Council
        <br>
        Selangor Model UN 2016 &#8211; ECOFIN &#8211; Best Delegate
        <br>
        Fairview Model UN 2016 &#8211; ASEAN
        <br>
        JOINMUN 2016 &#8211; ECOSOC &#8211; Honorable Mention
        <br>
        Jakarta Model UN 2017 &#8211; ECOFIN &#8211; Chair
      </p>
      <h2>
      </h2>
      <p>&nbsp;
      </p>
      <h2>
        <strong>EDUCATION
        </strong>
      </h2>
      <p>Asia Pacific University, Business in IT student, year 2016
      </p>
      <p>&nbsp;
      </p>
      <h2>
        <strong>SKILLS
        </strong>
      </h2>
      <p>I am specialized in Economic based committee. Beside that, I am an expert in European related council, due to awards that I have received. Furthermore, I am able to conduct humanitarian related councils with topics that I had done during previous Model UN conferences.
      </p>
    </div>
    <button type=button class="pum-close popmake-close" aria-label=Close>
      &#215; 
    </button>
  </div>
</div> 
<script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var c = document.body.className;
                c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
                document.body.className = c;</script> 
<noscript>
  <link
        id=woocommerce-currency-switcher rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/woocommerce-currency-switcher/css/front.css media=all type=text/css>
</noscript>
<noscript>
  <link
        id=premium-addons rel=stylesheet href=https://modelunitednation.org/wp-content/plugins/premium-addons-for-elementor/assets/frontend/min-css/premium-addons.min.css media=all type=text/css>
</noscript> 
<script id=eae-main-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var eae = {"ajaxurl":"https:\/\/modelunitednation.org\/wp-admin\/admin-ajax.php","current_url":"aHR0cHM6Ly9tb2RlbHVuaXRlZG5hdGlvbi5vcmcvYXlpbXVuLTIwMTkv","breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600}};
        var eae_editor = {"plugin_url":"https:\/\/modelunitednation.org\/wp-content\/plugins\/addon-elements-for-elementor-page-builder\/"};</script> 
<script id=leadin-script-loader-js-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var leadin_wordpress = {"userRole":"visitor","pageType":"page","leadinPluginVersion":"7.43.4"};</script> 
<script async defer id=hs-script-loader src='//js.hs-scripts.com/.js?integration=WordPress' id=leadin-script-loader-js-js type="8cfcd8d6780e2ddc6bdb6570-text/javascript">
</script> 
<script id=wc-add-to-cart-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/modelunitednation.org","is_cart":"","cart_redirect_after_add":"no"};</script> 
<script id=woocommerce-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};</script> 
<script id=wc-cart-fragments-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_e49ce740be2bd1fbc9edd1d72fbf9816","fragment_name":"wc_fragments_e49ce740be2bd1fbc9edd1d72fbf9816","request_timeout":"5000"};</script> 
<script src=https://connect.livechatinc.com/api/v1/script/d3af3e03-0f01-41a2-90dc-bcc313b10567/widget.js id=livechat-widget-js type="8cfcd8d6780e2ddc6bdb6570-text/javascript">
</script> 
<script id=popup-maker-site-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var pum_vars = {"version":"1.11.2","pm_dir_url":"https:\/\/modelunitednation.org\/wp-content\/plugins\/popup-maker\/","ajaxurl":"https:\/\/modelunitednation.org\/wp-admin\/admin-ajax.php","restapi":"https:\/\/modelunitednation.org\/wp-json\/pum\/v1","rest_nonce":null,"default_theme":"332","debug_mode":"","disable_tracking":"","home_url":"\/","message_position":"top","core_sub_forms_enabled":"1","popups":[]};
        var ajaxurl = "https:\/\/modelunitednation.org\/wp-admin\/admin-ajax.php";
        var pum_sub_vars = {"ajaxurl":"https:\/\/modelunitednation.org\/wp-admin\/admin-ajax.php","message_position":"top"};
        var pum_popups = {"pum-42130":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"triggers":[{"type":"click_open","settings":{"extra_selectors":".faossss"}}],"theme_id":"332","size":"medium","responsive_min_width":"0%","responsive_max_width":"100%","custom_width":"640px","custom_height":"380px","animation_type":"fade","animation_speed":"350","animation_origin":"center top","location":"center top","position_top":"100","position_bottom":"0","position_left":"0","position_right":"0","zindex":"1999999999","close_button_delay":"0","cookies":[],"theme_slug":"default-theme","id":42130,"slug":"fao"},"pum-50704":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":50704,"slug":"for-munpedia"},"pum-37061":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37061,"slug":"dr-dino-patti-djalal"},"pum-41100":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":41100,"slug":"testimonial-video-basak-nur-karasen"},"pum-38213":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":38213,"slug":"thxvote"},"pum-38015":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":38015,"slug":"reg_popup"},"pum-37477":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37477,"slug":"aung-ko-oo"},"pum-37475":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37475,"slug":"robin-jerome-banta"},"pum-37473":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37473,"slug":"amree-dueramae"},"pum-37469":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37469,"slug":"chornelius-fivtria-yuwana"},"pum-37423":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37423,"slug":"dominique"},"pum-37386":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37386,"slug":"safarov-jasur"},"pum-37063":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37063,"slug":"sipim-sornbanlang-b-a-m-p-s-ph-d"},"pum-37058":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37058,"slug":"ass-prof-dr-rosalia-sciortino-sumaryono"},"pum-37027":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37027,"slug":"taratan-intarachatorn"},"pum-37025":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":37025,"slug":"weerapot-bunlong"},"pum-33700":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33700,"slug":"chindy-fitridani"},"pum-36938":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36938,"slug":"selly-remiandayu"},"pum-36942":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36942,"slug":"john-braynel-mago-pural"},"pum-36940":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36940,"slug":"yen-my-vo"},"pum-36934":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36934,"slug":"hang-tola"},"pum-36930":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36930,"slug":"badamkhatan-tserenchimed"},"pum-33711":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33711,"slug":"vimal-raj-vivekanandah"},"pum-36870":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36870,"slug":"arslan-rana"},"pum-36874":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36874,"slug":"muoyleng-khov"},"pum-36872":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36872,"slug":"faidh-qurrotu-ainy"},"pum-36835":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36835,"slug":"muk-nattida-phuengpanit"},"pum-36833":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36833,"slug":"grace-eliana-wibisono"},"pum-36838":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36838,"slug":"sarfaraz-hussain"},"pum-36831":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36831,"slug":"mol-sokvanrith"},"pum-36829":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36829,"slug":"aruhvi-krishnasammy"},"pum-36827":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36827,"slug":"angga-gumilar"},"pum-36401":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36401,"slug":"mohammed-naim-uddin"},"pum-36397":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36397,"slug":"fahad-shahbaz"},"pum-36399":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":36399,"slug":"astrid-nadya-rizqita"},"pum-35502":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":35502,"slug":"nhung-nguyen"},"pum-35505":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":35505,"slug":"chahabuddeen-hayeebilang"},"pum-33717":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33717,"slug":"zhanning-ma"},"pum-33714":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33714,"slug":"yoshihiro-gurtiza"},"pum-33706":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33706,"slug":"snezhana-krot"},"pum-33704":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33704,"slug":"senkevich-dziyana"},"pum-33702":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33702,"slug":"rasulov-mukhammadjon"},"pum-33697":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33697,"slug":"jessica-paola-camelo-garcia"},"pum-33632":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33632,"slug":"danil-koshuev"},"pum-33630":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33630,"slug":"christian-dave-madula"},"pum-33627":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33627,"slug":"alipatova-mariia"},"pum-33616":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":33616,"slug":"aida-b-villegas"},"pum-28808":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":28808,"slug":"natalia-ayu-rizki-asti"},"pum-28771":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":28771,"slug":"md-shakhawat-hossain"},"pum-22909":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":22909,"slug":"raditya-rahim"},"pum-22966":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":22966,"slug":"syed-muhammad-salman-haider"},"pum-22958":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":22958,"slug":"udit-malik"},"pum-22955":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":22955,"slug":"nur-robiahtul-sofia"},"pum-22950":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":22950,"slug":"anggoro-galuh-pandhito"},"pum-22940":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":22940,"slug":"mohammad-huzaifa-fazal"},"pum-22969":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":22969,"slug":"john-caleb-roxas"},"pum-22982":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":22982,"slug":"lymeng-chi"},"pum-23083":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":23083,"slug":"musfirah-nadeem"},"pum-23175":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":23175,"slug":"arbaz-ahmed-yar-khan"},"pum-23193":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":23193,"slug":"hassaan-sudozai"},"pum-23185":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":23185,"slug":"abhishek-poddar"},"pum-23181":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":23181,"slug":"nur-faizah-mas-mohd-khalik"},"pum-23198":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":23198,"slug":"sayan-mukherjee"},"pum-22990":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":22990,"slug":"sakhir-ali-qureshi-2"},"pum-8522":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":8522,"slug":"sakhir-ali-qureshi"},"pum-8503":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":8503,"slug":"hassaan-nasim"},"pum-8281":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":8281,"slug":"muhamad-rizki-nugraha-darma-nagara"},"pum-7904":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":7904,"slug":"register"},"pum-3757":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":3757,"slug":"asean-regional-forum"},"pum-2168":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2168,"slug":"russell-marino-soh"},"pum-3759":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":3759,"slug":"unesco"},"pum-7724":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":7724,"slug":"press-corps"},"pum-3765":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":3765,"slug":"unhrc"},"pum-3763":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":3763,"slug":"unhcr"},"pum-351":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":351,"slug":"oic"},"pum-3761":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":3761,"slug":"imf"},"pum-2160":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2160,"slug":"mayang-krisnawardhani"},"pum-2363":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2363,"slug":"ang-mei-jun-sophie"},"pum-2265":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2265,"slug":"muhammad-rizal-saanun"},"pum-2259":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2259,"slug":"dominique-virgil-tuapetel"},"pum-2200":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2200,"slug":"rizkina-aliya"},"pum-2196":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2196,"slug":"varun-chopra"},"pum-2194":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2194,"slug":"nariswari-khairanisa-nurjaman"},"pum-2192":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2192,"slug":"patricia-mae-a-torres"},"pum-2144":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2144,"slug":"sean-chiong-ajihil"},"pum-2157":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2157,"slug":"miguel-leonardo-t-villegas"},"pum-2135":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2135,"slug":"saarthak-ravi"},"pum-2119":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"theme_slug":"default-theme","theme_id":332,"id":2119,"slug":"kevin-angdreas-2"}};</script> 
<script id=wc-price-slider_33-js-before type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var woocs_is_mobile = 1;
                var woocs_special_ajax_mode = 1;
                var woocs_drop_down_view = "flags";
                var woocs_current_currency = {"name":"USD","rate":1,"symbol":"&#36;","position":"left_space","is_etalon":1,"hide_cents":1,"hide_on_front":0,"rate_plus":0,"decimals":0,"description":"USA dollar","flag":"https:\/\/modelunitednation.org\/wp-content\/plugins\/woocommerce-currency-switcher\/img\/no_flag.png"};
                var woocs_default_currency = {"name":"USD","rate":1,"symbol":"&#36;","position":"left_space","is_etalon":1,"hide_cents":1,"hide_on_front":0,"rate_plus":0,"decimals":0,"description":"USA dollar","flag":"https:\/\/modelunitednation.org\/wp-content\/plugins\/woocommerce-currency-switcher\/img\/no_flag.png"};
                var woocs_array_of_get = '{}';
                woocs_array_no_cents = '["JPY","TWD"]';
                var woocs_ajaxurl = "https://modelunitednation.org/wp-admin/admin-ajax.php";
                var woocs_lang_loading = "loading";
                var woocs_shop_is_cached =0;</script> 
<script id=premium-addons-js-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var PremiumSettings = {"ajaxurl":"https:\/\/modelunitednation.org\/wp-admin\/admin-ajax.php"};</script> 
<script id=lae-frontend-scripts-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var lae_js_vars = {"custom_css":""};</script> 
<script id=bc9b8f2a3-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var localize = {"ajaxurl":"https:\/\/modelunitednation.org\/wp-admin\/admin-ajax.php","nonce":"d6046ac96c"};</script> 
<script id=elementor-frontend-js-before type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.8.1","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"https:\/\/modelunitednation.org\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":39363,"title":"AYIMUN%20%7C%20Asia%20Youth%20International%20Model%20United%20Nations","excerpt":"","featuredImage":false}};</script> 
<script id=jet-elements-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var jetElements = {"ajaxUrl":"https:\/\/modelunitednation.org\/wp-admin\/admin-ajax.php","isMobile":"true","templateApiUrl":"https:\/\/modelunitednation.org\/wp-json\/jet-elements-api\/v1\/elementor-template","devMode":"false","messages":{"invalidMail":"Please specify a valid e-mail"}};</script> 
<script id=jet-tricks-frontend-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var JetTricksSettings = {"elements_data":{"sections":{"20cd994":{"view_more":false,"particles":"false","particles_json":""},"cba6cd1":{"view_more":false,"particles":"false","particles_json":""},"6e344e6":{"view_more":false,"particles":"false","particles_json":""},"21c777f":{"view_more":false,"particles":"false","particles_json":""},"f91542d":{"view_more":false,"particles":"false","particles_json":""},"a96bdb3":{"view_more":false,"particles":"false","particles_json":""},"4a88e62":{"view_more":false,"particles":"false","particles_json":""},"d10e3e8":{"view_more":false,"particles":"false","particles_json":""},"5c4d913":{"view_more":false,"particles":"false","particles_json":""},"8e8a944":{"view_more":false,"particles":"false","particles_json":""},"76cbaf9":{"view_more":false,"particles":"false","particles_json":""},"ef3c9ee":{"view_more":false,"particles":"false","particles_json":""},"f763982":{"view_more":false,"particles":"false","particles_json":""},"f9c5e25":{"view_more":false,"particles":"false","particles_json":""},"1c4b4fc":{"view_more":false,"particles":"false","particles_json":""},"5c40759":{"view_more":false,"particles":"false","particles_json":""},"9be3755":{"view_more":false,"particles":"false","particles_json":""},"570eb80":{"view_more":false,"particles":"false","particles_json":""},"c164393":{"view_more":false,"particles":"false","particles_json":""},"1e7359d0":{"view_more":false,"particles":"false","particles_json":""},"34ae956f":{"view_more":false,"particles":"false","particles_json":""},"64ac19d":{"view_more":false,"particles":"false","particles_json":""},"7bb15ab":{"view_more":false,"particles":"false","particles_json":""},"e692654":{"view_more":false,"particles":"false","particles_json":""},"c1a6d7f":{"view_more":false,"particles":"false","particles_json":""},"4793e3e":{"view_more":false,"particles":"false","particles_json":""},"ee5c78c":{"view_more":false,"particles":"false","particles_json":""},"68e0e16":{"view_more":false,"particles":"false","particles_json":""},"b5fa74d":{"view_more":false,"particles":"false","particles_json":""},"3336c3e":{"view_more":false,"particles":"false","particles_json":""},"6384faa":{"view_more":false,"particles":"false","particles_json":""},"004502b":{"view_more":false,"particles":"false","particles_json":""},"e01d6e7":{"view_more":false,"particles":"false","particles_json":""},"bb0f011":{"view_more":false,"particles":"false","particles_json":""},"b9333ea":{"view_more":false,"particles":"false","particles_json":""},"cd9eb9c":{"view_more":false,"particles":"false","particles_json":""},"7175467":{"view_more":false,"particles":"false","particles_json":""},"4c973d7":{"view_more":false,"particles":"false","particles_json":""},"c83216a":{"view_more":false,"particles":"false","particles_json":""},"3e4f543":{"view_more":false,"particles":"false","particles_json":""},"7413b88":{"view_more":false,"particles":"false","particles_json":""},"417ea93":{"view_more":false,"particles":"false","particles_json":""},"85659db":{"view_more":false,"particles":"false","particles_json":""},"10999cb":{"view_more":false,"particles":"false","particles_json":""},"5284ce3":{"view_more":false,"particles":"false","particles_json":""},"80b8587":{"view_more":false,"particles":"false","particles_json":""},"79412e5":{"view_more":false,"particles":"false","particles_json":""},"410d6b0":{"view_more":false,"particles":"false","particles_json":""},"49eb14c":{"view_more":false,"particles":"false","particles_json":""},"3659002":{"view_more":false,"particles":"false","particles_json":""},"e49ae8f":{"view_more":false,"particles":"false","particles_json":""},"7d435d5":{"view_more":false,"particles":"false","particles_json":""},"ea4ae7e":{"view_more":false,"particles":"false","particles_json":""},"fc754dd":{"view_more":false,"particles":"false","particles_json":""},"7c908a8":{"view_more":false,"particles":"false","particles_json":""},"42be9ab":{"view_more":false,"particles":"false","particles_json":""},"5cc260a":{"view_more":false,"particles":"false","particles_json":""},"55e19cc":{"view_more":false,"particles":"false","particles_json":""},"0aa9097":{"view_more":false,"particles":"false","particles_json":""},"9d0d29b":{"view_more":false,"particles":"false","particles_json":""},"a129825":{"view_more":false,"particles":"false","particles_json":""},"db0beeb":{"view_more":false,"particles":"false","particles_json":""}},"columns":[],"widgets":{"4e37765":[],"8ccdea2":[],"342b905":[],"af50ad5":[],"25618b2":[],"2b9e14f":[],"3f3f822":[],"d66c525":[],"438da12":[],"737dd03":[],"529200c":[],"3a40708":[],"3e3c93c":[],"ee8b79c":[],"e371e2b":[],"dd92fa6":[],"00cd860":[],"6d36061":[],"b119c59":[],"1e08558":[],"1a3b38d":[],"5aa15a7":[],"fbe400f":[],"f328bc4":[],"6c93526":[],"79e06bb":[],"0560bff":[],"a55c524":[],"e941bda":[],"42a4733":[],"0d85d4c":[],"23f4a19":[],"3d61651":[],"c08764e":[],"bbfa6ea":[],"5d721c9":[],"e3935d8":[],"78a6bc0":[],"00d08b3":[],"387a1f7":[],"c87151e":[],"b1f4659":[],"885ad0e":[],"67474c1":[],"f8b2332":[],"342daef":[],"ef42aed":[],"5a8c64a":[],"1c39923":[],"8bcd770":[],"6ca3844":[],"22123349":[],"68c8887":[],"59c6936b":[],"1831f3b8":[],"7e1e655":[],"e493464":[],"ee712a6":[],"b2c455f":[],"bbb50ad":[],"4967f9b":[],"2f5bce4":[],"eb92f3d":[],"fbe33c0":[],"ce7d40a":[],"784cd54":[],"4dbae7d":[],"fd396b8":[],"a7644bd":[],"f552857":[],"f09024e":[],"67cadba":[],"dc48e52":[],"964ce30":[],"f1ed80b":[],"af5b62b":[],"3a65b51":[],"4fe9fec":[],"c60f9eb":[],"ce4422b":[],"f5485c4":[],"59ce316":[],"401afe3":[],"8d2cba7":[],"b729b08":[],"7ac49ec":[],"ce6df09":[],"1393409":[],"ce220e7":[],"e8093e0":[],"8b1d43e":[],"cb98e43":[],"c8a4d1f":[],"0b76f0b":[],"4296328":[],"c556cfd":[],"31bfcf5":[],"a148175":[],"8107cf6":[],"06e06a2":[],"0ea8e78":[],"2c32dbb":[],"877b4ad":[],"c30b938":[],"19b646b":[],"9565391":[],"054e272":[],"92afb45":[],"0e4350b":[],"13f4878":[],"c091421":[],"d04180e":[],"9539ef0":[],"9cdf1d4":[],"425674e":[],"0bb5dce":[],"648554a":[],"4cdb705":[],"70e6367":[],"c9c5439":[],"b383e61":[],"1066757":[],"a2ce035":[],"345e304":[],"046a250":[],"fbade79":[],"8da0b38":[],"333ad0e":[],"43c8604":[],"7c6eb3f":[],"6cd31e0":[],"1d58034":[],"4bb416b":[],"742b242":[],"dc9a9d0":[],"c783cec":[],"b0bcaf4":[],"5740765":[],"8dee08a":[],"bff83ab":[],"33074cf":[],"78d773a":[],"7a48bd6":[],"5257cae":[],"51288cc":[],"0a5f6a7":[],"d9cad60":[],"ab7d43f":[],"b9c71aa":[],"41bc815":[],"4db5bf6":[],"21a8abf":[],"6f4db32":[],"3b74fc6":[],"ccd5a3a":[],"a1295f7":[],"ed21123":[],"9ef502f":[],"5d50016":[],"5dd2043":[],"2521bf4":[],"6d68ca9":[],"f9869bf":[],"b72f3f6":[],"f844f7d":[],"263da37":[],"6eda910":[],"faea789":[],"5f3f4fb":[],"33af974":[],"44c1e50":[],"9e0e1a2":[],"3746bf3":[],"89da7d9":[],"65409f3":[],"84b6252":[],"0e5aa50":[],"48d60e5":[],"a5163d4":[],"ee28e2f":[],"f37b09e":[],"e83cbbe":[],"34ee5e6":[],"ddae30e":[],"8e7040d":[],"19f3b79":[],"3170a3c":[],"e7e1220":[],"93e30c2":[],"b76da9c":[],"008281b":[],"e1ddaf8":[],"27fb847":[],"1f66da2":[],"ca816dc":[],"db7e151":[],"56fdac9":[],"c0550a2":[],"2e513d7":[],"58ac976":[],"5a5271c":[],"876c1ca":[]}}};</script> 
<script id=jet-sticky-frontend-js-extra type="8cfcd8d6780e2ddc6bdb6570-text/javascript">var JetStickySettings = {"elements_data":{"sections":[],"columns":[]}};</script> 
<script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">jQuery(function($) { 
                            $( '.add_to_cart_button:not(.product_type_variable, .product_type_grouped)' ).click( function() {
                                ga( 'ec:addProduct', {'id': ($(this).data('product_sku')) ? ($(this).data('product_sku')) : ('#' + $(this).data('product_id')),'quantity': $(this).data('quantity')} );
                                ga( 'ec:setAction', 'add' );
                                ga( 'send', 'event', 'UX', 'click', 'add to cart' );
                            });
        });</script> 
<script type="8cfcd8d6780e2ddc6bdb6570-text/javascript">function lazyLoadThumb(e){var t='<img src=https://i.ytimg.com/vi/ID/hqdefault.jpg alt width=480 height=360>',a='<div class=play></div>';return t.replace("ID",e)+a}function lazyLoadYoutubeIframe(){var e=document.createElement("iframe"),t="ID?autoplay=1";t+=0===this.dataset.query.length?'':'&'+this.dataset.query;e.setAttribute("src",t.replace("ID",this.dataset.src)),e.setAttribute("frameborder","0"),e.setAttribute("allowfullscreen","1"),e.setAttribute("allow", "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"),this.parentNode.replaceChild(e,this)}document.addEventListener("DOMContentLoaded",function(){var e,t,a=document.getElementsByClassName("rll-youtube-player");for(t=0;t<a.length;t++)e=document.createElement("div"),e.setAttribute("data-id",a[t].dataset.id),e.setAttribute("data-query", a[t].dataset.query),e.setAttribute("data-src", a[t].dataset.src),e.innerHTML=lazyLoadThumb(a[t].dataset.id),e.onclick=lazyLoadYoutubeIframe,a[t].appendChild(e)});</script> 
<script defer src=https://modelunitednation.org/wp-content/cache/autoptimize/js/autoptimize_f38cb269842404f4e3bd37d8675ccc88.js type="8cfcd8d6780e2ddc6bdb6570-text/javascript">
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="8cfcd8d6780e2ddc6bdb6570-|49" defer="">
</script>
</body>
</html>
